// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js


var db = null;
var dbkey = "9(A3?6$y>44z+j";




angular.module('starter', ['ionic',  'starter.controllers', 'starter.services'])



.run(function($ionicPlatform, $cordovaSQLite, $ionicModal, synchService, $timeout, networkService) {




//  alert(".run()....");
window.localStorage['localPRReadings'] = "";
window.localStorage['localBPReadings'] = "";
window.localStorage['localErrors'] = "";

window.localStorage['secondHighBP'] = 0;
window.localStorage['secondLowBP'] = 0;




  $ionicPlatform.ready(function() {



      db = window.sqlitePlugin.openDatabase({name: 'my.db', location: 'default', key: dbkey }, function(db) {
                db.transaction(function(tx) {
                  //  alert("slqc.my.db SUCCESS... tx: " + JSON.stringify(tx));

                    tx.executeSql('CREATE TABLE IF NOT EXISTS test_table (id integer primary key, data text, data_num integer)');
                    tx.executeSql("CREATE TABLE IF NOT EXISTS bp_reading_raw (bpID integar primary key, createdAt text, userID text, base64Data text, stringData text, hexData text, bpmID text, phoneID text, readingOutcome text, readingOutcomeDetails text )");

                    tx.executeSql("CREATE TABLE IF NOT EXISTS bp_reading_processed (bppID integar primary key, createdAt text, userID text, chgSynched text, sys1 integar, dia1 integar, hr1 integar, sys2 integar, dia2 integar, hr2 integar, sys3 integar, dia3 integar, hr3 integar, finalSys integar, finalDia integar, finalHr integar, finalMap text, feedback text, outcome text, phoneID text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text, phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text, medicalAlertPresent text,  medicalAlertName text, medicalAlertDesc text, medicalAlertOther text )");
                    tx.executeSql("CREATE TABLE IF NOT EXISTS protein_reading (proteinID integar primary key, createdAt text, userID text, proteinReading text, chgSynched text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text, phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text, medicalAlertPresent text, medicalAlertName text, medicalAlertDesc text, medicalAlertOther text )");

                    tx.executeSql("CREATE TABLE IF NOT EXISTS device_error (errorID integar primary key, createdAt text, userID text, chgSynched integar, errorType text, errorName text, errorDesc text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text, phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text, medicalAlertPresent text, medicalAlertName text, medicalAlertDesc text, medicalAlertOther text )  ");

                    tx.executeSql("CREATE TABLE IF NOT EXISTS medical_alert (medAlertID integar primary key, createdAt text, desc text, bprchgSynched integar, machgSynched integar, userID text, localBPRID integar, chgBPRID integar, finalSys integar, finalDia integar, finalHr integar, symptomCount integar)  ");
                    tx.executeSql( "CREATE TABLE IF NOT EXISTS userData (id integar primary key, pin integar, userID text, phoneUUID text, bpmID text, loginDate text, token text)");






                },
                 function(err) {
                  alert('app.js: 248 : Open database ERROR: ' + JSON.stringify(err));
                });
    });  //end of openDB,....sqlc stuff




  //  checkLocalData.check();




              var networkConn = networkService.check();
              console.log("******networkConn == " + networkConn);

              if (networkConn == 1)
              {

                                  console.log("$ionicPlatform.ready... so calling synch 1 time!");

                                  ///THERE ARE ABOUT 40S BETWEEN EACH SYNCH WHICH minimizes the possiblity of overload etc...
                                  $timeout(function ()
                                  {
                                      var d = new Date();
                                      var time = d.toLocaleTimeString();
                                      console.log("** synchBP:: Time Now is: " + time);

                                      synchService.synchBP();
                                  }, 2000); //run this after 2s


                                  $timeout(function ()
                                  {
                                      var d = new Date();
                                      var time = d.toLocaleTimeString();
                                      console.log("** synchPR:: Time Now is: " + time);

                                      synchService.synchProteinReadings();
                                  }, 40000); //run this after 2s

                                  $timeout(function ()
                                  {
                                      var d = new Date();
                                      var time = d.toLocaleTimeString();
                                      console.log("** synchDE:: Time Now is: " + time);

                                      synchService.synchDeviceErrors();
                                  }, 80000); //run this after 2s
                }



            //=============== Synching Interval Timers ===========
            //BP Readings EVERY 60mins (ie 3.6M milliseconds)
            var synchBPVar = setInterval(bpSynchTimer, 3600000);
            function bpSynchTimer() {
                var d = new Date();
                var time = d.toLocaleTimeString();
                console.log("Time Now is: " + time);

                        if (networkConn == 1)
                        {
                          console.log("---- Doing TIMED BP Reading Synch");
                        synchService.synchBP();
                        }
                        else {
                          console.log("******networkConn == " + networkConn + "so CANT DO TIMED BP Reading Synch");
                        }

            }


            //Protein Readings EVERY 65mins (ie 3.9M milliseconds)
            var synchProteinVar = setInterval(proteinSynchTimer, 3900000);
            function proteinSynchTimer() {
                var d = new Date();
                var time = d.toLocaleTimeString();
                console.log("Time Now is: " + time);

                              if (networkConn == 1)
                              {
                                console.log("--- Doing TIMED Protein Reading Synch");
                              synchService.synchProteinReadings();
                              }
                              else {
                                console.log("******networkConn == " + networkConn + "so CANT DO TIMED Protein Reading Synch");
                              }
              }



            //Device Errors EVERY 70mins (ie 4.2M milliseconds)
            var synchErrorVar = setInterval(errorSynchTimer, 4200000);
            function errorSynchTimer() {
                var d = new Date();
                var time = d.toLocaleTimeString();
                console.log("Time Now is: " + time);


                            if (networkConn == 1)
                            {
                              console.log("---- Doing TIMED Device Errors Synch");
                              synchService.synchDeviceErrors();
                            }
                            else {
                              console.log("******networkConn == " + networkConn + " - so CANT DO TIMED Device Errors Reading Synch");
                            }



            }
           //=============== ///// Synching Interval Timers ===========





 window.localStorage['phoneMake'] = device.manufacturer;
 window.localStorage['phoneModel'] = device.model;
 window.localStorage['phoneOS'] = device.platform;
 window.localStorage['phoneOSVersion'] = device.version;
 window.localStorage['phoneUUID'] = device.uuid;


      cordova.getAppVersion(function(version) {
              var  appVersion = version;
              window.localStorage['appVersion'] = appVersion;
            });







    var isAndroid = ionic.Platform.isAndroid();

        if (isAndroid == true)
        {
                bluetoothSerial.isEnabled(
                          function() {
                          // alert("BTSERIAL is enabled. Listing");

                             bluetoothSerial.list(
                                 function(results) {
                                //alert(JSON.stringify(results));
                                   var addr = results[0].address;
                                  // alert("1. addr: " + addr)
                                   window.localStorage['mlAddr'] = addr;
                                   //return addr;

                                 },
                                 function(error) {
                                   //alert(JSON.stringify(error));
                                   return 0;
                                 }
                             );



                          },
                          function() {
                            //  alert("BTSERIAL is *not* enabled... calling enable bt serial");

                                  bluetoothSerial.enable( function()
                                  {

                                       bluetoothSerial.list(
                                           function(results) {
                                            // alert(JSON.stringify(results));
                                             var addr = results[0].address;
                                            // alert("addr: " + addr)
                                             window.localStorage['mlAddr'] = addr;
                                             //return addr;

                                           },
                                           function(error) {
                                             alert(JSON.stringify(error));
                                             return 0;
                                           }
                                       );


                                  },  function()
                                      {
                                          //  alert("The user did *not* enable Bluetooth");
                                      });







                          }
                      );

            }















    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }




 var isAndroid = ionic.Platform.isAndroid();
 //alert("isAndroid == " + isAndroid);

//start of sqlc stuff
//dbkey = "anotherdbkey"; //should not open db



















  }); //end platform.ready()








}) //end run()




.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.tabs.style('standard');








  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:
/*
//PIN
//Move into a PopUp box
.state('pin', {
  url: '/pin',
  views: {
    'pin': {
      templateUrl: 'templates/pincodeModal.html',
      controller: 'UnlockController'
    }
  }
}) */




  .state('tab.dash', {
    cache: false,
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

    .state('tab.btSS', {
      cache: false,
      url: '/btss',
      views: {
        'tab-dash': {
          templateUrl: 'templates/btSS1.html',
          controller: 'btSSCtrl'
        }
      }
    })

        .state('tab.btSSResult', {
          cache: false,
          url: '/btssresult',
          views: {
            'tab-dash': {
              templateUrl: 'templates/btSSResult.html',
              controller: 'btSSCtrl'
            }
          }
        })



    .state('tab.btSerialTest', {
      url: '/btSerialTest',
      views: {
        'tab-dash': {
          templateUrl: 'templates/btSerialTest.html',
          controller: 'btSerialTestCtrl'
        }
      }
    })

/*
    .state('tab.home', {
    parent: 'tab',
    url: '/HomeBle',
    controller: "HomeCtrl",
    templateUrl: "bleRD.html",
  })
  */

    .state('tab.home', {
      url: '/home',
      views: {
        'tab-dash': {
          templateUrl: 'templates/bleRD.html',
          controller: 'HomeCtrl'
        }
      }
    })

/*
        .state('tab.base64', {
          url: '/base64',
          views: {
            'tab-dash': {
              templateUrl: 'templates/base64.html',
              controller: 'myController'
            }
          }
        }) */


  .state('tab.appFeedback', {
    url: '/feedback',
    views: {
      'tab-dash': {
        templateUrl: 'templates/appFeedback.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.appFeedback2', {
    url: '/feedback2',
    views: {
      'tab-dash': {
        templateUrl: 'templates/appFeedback2.html',
        controller: 'DashCtrl'
      }
    }
  })



    .state('tab.errorWarningScreens', {
      url: '/errorWarningScreens',
      views: {
        'tab-dash': {
          templateUrl: 'templates/errorWarningScreens.html',
          controller: 'DashCtrl'
        }
      }
    })


    .state('tab.bpStepInit', {
      cache: false,
      url: '/stepInit',
      views: {
        'tab-dash': {
          templateUrl: 'templates/stepInit.html',
          controller: 'btSSCtrl'
        }
      }
    })

        .state('tab.bpStep0', {
          cache: false,
          url: '/step0',
          views: {
            'tab-dash': {
              templateUrl: 'templates/step0.html',
              controller: 'btSSCtrl'
            }
          }
        })

          .state('tab.bpStep1', {
            url: '/step1',
            views: {
              'tab-dash': {
                templateUrl: 'templates/step1.html',
                controller: 'btSSCtrl'
              }
            }
          })


              .state('tab.bpStep2', {
                url: '/step2',
                views: {
                  'tab-dash': {
                    templateUrl: 'templates/step2.html',
                    controller: 'btSSCtrl'
                  }
                }
              })

                  .state('tab.bpStep3', {
                    url: '/step3',
                    views: {
                      'tab-dash': {
                        templateUrl: 'templates/step3.html',
                        controller: 'btSSCtrl'
                      }
                    }
                  })




                  .state('tab.bpStep4', {
                    cache: false,
                    url: '/step4',
                    views: {
                      'tab-dash': {
                        templateUrl: 'templates/step4.html',
                        controller: 'btSSCtrl'
                      }
                    }
                  })




                  .state('tab.initReading', {
                        cache: false,
                    url: '/initReading',
                    views: {
                      'tab-dash': {
                        templateUrl: 'templates/initReading.html',
                        controller: 'DashCtrl'
                      }
                    }
                  })

                      //Protein/Urine Screens...

                        .state('tab.proteinStep0', {
                              cache: false,
                          url: '/proteinStep0',
                          views: {
                            'tab-dash': {
                              templateUrl: 'templates/proteinStep0.html',
                              controller: 'DashCtrl'
                            }
                          }
                        })


                    .state('tab.proteinStep1', {

                      url: '/proteinStep1',
                      views: {
                        'tab-dash': {
                          templateUrl: 'templates/proteinStep1.html',
                          controller: 'DashCtrl'
                        }
                      }
                    })


                        .state('tab.proteinStep2', {

                          url: '/proteinStep2',
                          views: {
                            'tab-dash': {
                              templateUrl: 'templates/proteinStep2.html',
                              controller: 'DashCtrl'
                            }
                          }
                        })

                                .state('tab.proteinStep2_2', {
                                    cache: false,
                                  url: '/proteinStep2_2',
                                  views: {
                                    'tab-dash': {
                                      templateUrl: 'templates/proteinStep2_2.html',
                                      controller: 'DashCtrl'
                                    }
                                  }
                                })


                            .state('tab.proteinStep3', {
                                  cache: false,
                              url: '/proteinStep3',
                              views: {
                                'tab-dash': {
                                  templateUrl: 'templates/proteinStep3.html',
                                  controller: 'DashCtrl'
                                }
                              }
                            })

                            .state('tab.proteinError', {
                              cache: false,
                              url: '/proteinError',
                              views: {
                                'tab-dash': {
                                  templateUrl: 'templates/proteinError.html',
                                  controller: 'DashCtrl'
                                }
                              }
                            })

                            .state('tab.proteinWarningMoreThanOneSymptom', {
                              cache: false,
                              url: '/proteinWarningMoreThanOneSymptom',
                              views: {
                                'tab-dash': {
                                  templateUrl: 'templates/proteinWarningMoreThanOneSymptom.html',
                                  controller: 'DashCtrl'
                                }
                              }
                            })


                            .state('tab.proteinWarningCombined', {
                              cache: false,
                              url: '/proteinWarningCombined',
                              views: {
                                'tab-dash': {
                                  templateUrl: 'templates/proteinWarningCombined.html',
                                  controller: 'DashCtrl'
                                }
                              }
                            })






                    .state('tab.tolven', {
                      url: '/tolven',
                      views: {
                        'tab-dash': {
                          templateUrl: 'templates/tolven.html',
                          controller: 'MainCtrl'
                        }
                      }
                    })




            .state('tab.bpError1', {
              url: '/bpError1',
              views: {
                'tab-dash': {
                  templateUrl: 'templates/bpError1.html',
                  controller: 'DashCtrl'
                }
              }
            })


            .state('tab.bpError2', {
              url: '/bpError2',
              views: {
                'tab-dash': {
                  templateUrl: 'templates/bpError2.html',
                  controller: 'DashCtrl'
                }
              }
            })


            .state('tab.bpError3', {
              url: '/bpError3',
              views: {
                'tab-dash': {
                  templateUrl: 'templates/bpError3.html',
                  controller: 'DashCtrl'
                }
              }
            })





            .state('tab.bpError5', {
              url: '/bpError5',
              views: {
                'tab-dash': {
                  templateUrl: 'templates/bpError5.html',
                  controller: 'DashCtrl'
                }
              }
            })

              .state('tab.bpmErrorLowBatt', {
                url: '/bpErrorLowBatt',
                views: {
                  'tab-dash': {
                    templateUrl: 'templates/bpErrorLowBatt.html',
                    controller: 'DashCtrl'
                  }
                }
              })


              .state('tab.bpErrorOther', {
                url: '/bpErrorOther',
                views: {
                  'tab-dash': {
                    templateUrl: 'templates/bpErrorOther.html',
                    controller: 'DashCtrl'
                  }
                }
              })

                .state('tab.btConnClosed', {
                  url: '/btConnClosed',
                  views: {
                    'tab-dash': {
                      templateUrl: 'templates/btConnClosed.html',
                      controller: 'DashCtrl'
                    }
                  }
                })



    .state('tab.bpErrorHI', {
      url: '/bpErrorHI',
      views: {
        'tab-dash': {
          templateUrl: 'templates/bpErrorHI.html',
          controller: 'DashCtrl'
        }
      }
    })

        .state('tab.bpErrorLO', {
          url: '/bpErrorLO',
          views: {
            'tab-dash': {
              templateUrl: 'templates/bpErrorLO.html',
              controller: 'DashCtrl'
            }
          }
        })


    .state('tab.bpWarningMoreThanOneSymptom', {
      cache: false,
      url: '/bpWarningMoreThanOneSymptom',
      views: {
        'tab-dash': {
          templateUrl: 'templates/bpWarningMoreThanOneSymptom.html',
          controller: 'btSSCtrl'
        }
      }
    })

    .state('tab.bpmErrorMoreThanOneSymptom', {
      cache: false,
      url: '/bpmErrorMoreThanOneSymptom',
      views: {
        'tab-dash': {
          templateUrl: 'templates/bpmErrorMoreThanOneSymptom.html',
          controller: 'btSSCtrl'
        }
      }
    })

            .state('tab.bpWarningCombined1', {
              cache: false,
              url: '/bpWarningCombined1',
              views: {
                'tab-dash': {
                  templateUrl: 'templates/bpWarningCombined1.html',
                  controller: 'btSSCtrl'
                }
              }
            })

              .state('tab.bpWarningCombined2', {
                url: '/bpWarningCombined2',
                views: {
                  'tab-dash': {
                    templateUrl: 'templates/bpWarningCombined2.html',
                    controller: 'btSSCtrl'
                  }
                }
              })





        .state('tab.bpWarning1', {
          cache: false,
          url: '/bpWarning1',
          views: {
            'tab-dash': {
              templateUrl: 'templates/bpWarning1.html',
              controller: 'btSSCtrl'
            }
          }
        })



    .state('tab.bpWarning2', {
      cache: false,
      url: '/bpWarning2',
      views: {
        'tab-dash': {
          templateUrl: 'templates/bpWarning2.html',
          controller: 'btSSCtrl'
        }
      }
    })


        .state('tab.bpWarningAFIB', {
          url: '/bpWarningAFIB',
          views: {
            'tab-dash': {
              templateUrl: 'templates/bpWarningAFIB.html',
              controller: 'DashCtrl'
            }
          }
        })

      .state('tab.bpWarningLow', {
        cache: false,
        url: '/bpWarningLow',
        views: {
          'tab-dash': {
            templateUrl: 'templates/bpWarningLow.html',
            controller: 'btSSCtrl'
          }
        }
      })

          .state('tab.bpWarningLow2', {
            cache: false,
            url: '/bpWarningLow2',
            views: {
              'tab-dash': {
                templateUrl: 'templates/bpWarningLow2.html',
                controller: 'btSSCtrl'
              }
            }
          })

          .state('tab.symptomWarning', {
            cache: false,
            url: '/symptomWarning',
            views: {
              'tab-dash': {
                templateUrl: 'templates/symptomWarning.html',
                controller: 'btSSCtrl'
              }
            }
          })





  .state('tab.log', {
       cache: false,
      url: '/log',
      views: {
        'tab-log': {
          templateUrl: 'templates/tab-log.html',
          controller: 'LogCtrl'
        }
      }
    })

    .state('tab.prLog', {
         cache: false,
        url: '/prLog',
        views: {
          'tab-prLog': {
            templateUrl: 'templates/tab-logPR.html',
            controller: 'LogCtrl'
          }
        }
      })


    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })


      .state('tab.help', {
        url: '/help',
        views: {
          'tab-help': {
            templateUrl: 'templates/tab-help.html',
            controller: 'HelpCtrl'
          }
        }
      })



  .state('tab.account', {
    cache: false,
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })


  //Log in stuff
  .state('signin', {
    cache: false,
  url: '/sign-in',
  templateUrl: 'templates/sign-in.html',
  controller: 'SignInCtrl'
})


.state('forgotpassword', {
  url: '/forgot-password',
  templateUrl: 'templates/forgotPassword.html'
});









  // if none of the above states are matched, use this as the fallback
  //$urlRouterProvider.otherwise('/tab/dash');
  $urlRouterProvider.otherwise('/tab/dash');
});
