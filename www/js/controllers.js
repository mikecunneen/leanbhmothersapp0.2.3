angular.module('starter.controllers', [ 'ngCookies', 'ngCordova', 'ngCordovaBluetoothLE', 'base64'])


.controller('MainCtrl', function($scope, $http) {



})



//anti tamper PIN Controller
.controller('UnlockController', function($scope, $location, $timeout, $state, $ionicModal) {


                $ionicModal.fromTemplateUrl('templates/pincodeModal.html', {
                scope: $scope
              }).then(function(modal) {
                $scope.modal = modal;
              });

                $scope.init = function() {

                   $scopeCorrectPasscode = 8585;
                   $scope.passcode = "";
                }

                $scope.add = function(value) {

                            if($scope.passcode.length < 4) {
                            $scope.passcode = $scope.passcode + value;
                            if($scope.passcode.length == 4) {
                                $timeout(function() {
                                    console.log("The four digit code was entered");
                                }, 500);

                                if($scope.passcode == $scopeCorrectPasscode)
                                {
                                console.log("Correct PIN :)");
                                //$state.go('tab.dash');

                                }



                            }
                        }


                }

                $scope.delete = function() {

                             if($scope.passcode.length > 0) {
                              $scope.passcode = $scope.passcode.substring(0, $scope.passcode.length - 1);
                          }

                }





})

//BLUETOOTH SIMPLE Stupid
.controller('btSSCtrl', function(storeDeviceError, networkService, chgServerApi, $base64, $state, $scope, $http, $timeout, $cordovaSQLite, $cordovaBluetoothLE, $ionicPopup, $ionicLoading)
{

  $scope.finishMedAlert = function()
  {
   console.log("med alert done...");
  //  $state.go("tab.dash");
$scope.resetBPSymptoms();

  }

  //symptom check box values...
  $scope.checkboxModel = {
     value1 : false,
     value2 : false,
     value3 : false,
     value4: false
   };


   window.localStorage['totalSymptomCount'] = parseInt(window.localStorage['headachesSymptom']) + parseInt(window.localStorage['visDSymptom']) + parseInt(window.localStorage['epiGSymptom']);
   console.log(" ");
   console.log("*****^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );
   console.log(" ");




console.log("second high bp: " + window.localStorage['secondHighBP']);
console.log("second low bp: " + window.localStorage['secondLowBP']);
console.log("lastProteinReading: " + window.localStorage['lastProteinReading']);




$scope.captureHeadaches = function(item)
{

console.log("Headaches?: " + $scope.checkboxModel.value1 + " ; item: " + JSON.stringify(item));

      if (item == true)
      {
        window.localStorage['headachesSymptom'] = 1;
        console.log("window.localStorage['headachesSymptom']: " + window.localStorage['headachesSymptom']);
        $scope.checkboxModel.value4 = false;


      }
      else {
        window.localStorage['headachesSymptom'] = 0;
        console.log("window.localStorage['headachesSymptom']: " + window.localStorage['headachesSymptom']);
      }

}





$scope.captureVisD = function(item1)
{

console.log("Vis D?: " + $scope.checkboxModel.value2);

          if (item1 == true)
          {
            window.localStorage['visDSymptom'] = 1;
            console.log("window.localStorage['visDSymptom']: " + window.localStorage['visDSymptom']);
            $scope.checkboxModel.value4 = false;

          }
          else {
            window.localStorage['visDSymptom'] = 0;
            console.log("window.localStorage['visDSymptom']: " + window.localStorage['visDSymptom']);
          }

}

$scope.captureEpiG = function(item2)
{

          if (item2 == true)
          {
            window.localStorage['epiGSymptom'] = 1;
            console.log("window.localStorage['epiGSymptom']: " + window.localStorage['epiGSymptom']);
              $scope.checkboxModel.value4 = false;

          }
          else {
            window.localStorage['epiGSymptom'] = 0;
            console.log("window.localStorage['epiGSymptom']: " + window.localStorage['epiGSymptom']);
          }


console.log("EpiG?: " +   window.localStorage['epiGSymptom'] );



}

$scope.captureNone = function(item3)
{
$scope.None = item3;
console.log("No Symptoms?: " + $scope.None);

  $scope.checkboxModel.value1 = false;
  $scope.checkboxModel.value2 = false;
  $scope.checkboxModel.value3 = false;



}

    //handle unselected symptom checkboxes
    if ( window.localStorage['headachesSymptom'] == null ||  window.localStorage['headachesSymptom'] == undefined )
    {
     window.localStorage['headachesSymptom'] = 0;
    }
    else if ( window.localStorage['visDSymptom'] == null ||  window.localStorage['visDSymptom'] == undefined )
    {
       window.localStorage['visDSymptom'] = 0;
    }
    else if ( window.localStorage['epiGSymptom'] == null ||  window.localStorage['epiGSymptom'] == undefined )
    {
       window.localStorage['epiGSymptom'] = 0;
    }

    var moreThan1Symptom = false;


     //init symptomCount
     $scope.calcSymptomCount = function()
     {



                if ( $scope.checkboxModel.value1 == false &  $scope.checkboxModel.value2 == false &  $scope.checkboxModel.value3 == false &  $scope.checkboxModel.value4 == false)
                {

                  $scope.showAlert = function() {
                        var alertPopup = $ionicPopup.alert({
                          title: 'Please Select an Option',
                          template: 'Please select an option from the Symptom Check list'
                        });
                      }
                      $scope.showAlert();


                }
                else {
                  console.log("A Selection was Made!  at least 1 of the symptoms / none of the above was selected! $scope.checkboxModel.value4: " + $scope.checkboxModel.value4 );

                            if ($scope.checkboxModel.value4 == false)
                            {

                                      if ( $scope.checkboxModel.value1 == false )
                                      {
                                        window.localStorage['headachesSymptom'] = 0;
                                      }
                                      else if ( $scope.checkboxModel.value2 == false )
                                      {
                                        window.localStorage['visDSymptom'] = 0;
                                      }
                                      else if ( $scope.checkboxModel.value3 == false )
                                      {
                                        window.localStorage['epiGSymptom'] = 0;
                                      }


                                      window.localStorage['totalSymptomCount'] = parseInt(window.localStorage['headachesSymptom']) + parseInt(window.localStorage['visDSymptom']) + parseInt(window.localStorage['epiGSymptom']);
                                      console.log("*****^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );

                                            $state.go("tab.bpStep1");





                            }
                            else {
                              window.localStorage['totalSymptomCount'] = 0;
                              window.localStorage['headachesSymptom'] = 0;
                              window.localStorage['visDSymptom'] = 0;
                              window.localStorage['epiGSymptom'] = 0;

                              console.log("^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );

                                    $state.go("tab.bpStep1");
                            }




                }




       var dtx = new Date();
       var utcDatex = dtx.toISOString();
       console.log("dtx: " + utcDatex );



    } //end calcSymptomCount()


    $scope.resetBPSymptoms = function()
    {
        console.log("*** resetting BP symptoms...");
         window.localStorage['headachesSymptom'] = 0;
         window.localStorage['visDSymptom'] = 0;
         window.localStorage['epiGSymptom'] = 0;
         window.localStorage['noSymptoms'] = null;
         window.localStorage['totalSymptomCount'] = 0;


    }




     var secondLowBP = 0;
  //   var secondHighBP = 0;

      var thisBPData = [];
      var byteArrayCount = null;
      byteArrayCount = 0;
      var parsePostCounter = 0;

      var isIOS = ionic.Platform.isIOS();
      var isAndroid = ionic.Platform.isAndroid();

      var bpmUUID = null;

      $scope.result = {};
      //alert("****START of btSSCtrl---");

         //THIS block RELIES on the controller being loaded EVERY time a tplate loads... otherwise it wont work!!
         $scope.result.finalSys = window.localStorage['fSys'];
         $scope.result.finalDia = window.localStorage['fDia'];
         $scope.result.finalHr =  window.localStorage['fHr'];
         $scope.result.finalMap = window.localStorage['fMap'];

         $scope.result.protein = window.localStorage['lastProteinReading'];




    //  if (isIOS == true)
      //{

      //  alert("isIOS=> " + isIOS);

                function closeConn(uuid)
                {
                   $scope.result.finalSys = "";
                   $scope.result.finalDia = "";
                   $scope.result.finalHr =  "";
                   $scope.result.finalMap = "";

                   $scope.checkboxModel.value1 = false;
                   $scope.checkboxModel.value2 = false;
                   $scope.checkboxModel.value3 = false;
                   $scope.checkboxModel.value4 = false;

                           //clear these vars BUT BUT BUT Only after the conn is CLOSED and ALL Data is Got!
                           thisBPData = [];
                           byteArrayCount = 0;
                           parsePostCounter = 0;
                           symptomCount = 0;



                           function resetSymptoms()
                           {
                              console.log("*** resetting symptoms...");
                                 window.localStorage['headachesSymptom'] = 0;
                                 window.localStorage['visDSymptom'] = 0;
                                 window.localStorage['epiGSymptom'] = 0;
                                 window.localStorage['noSymptoms'] = null;
                           }

                            $timeout(function() { resetSymptoms() },  10000 );



                //  var  bpmUUID_ = "5E9B7376-173F-FE68-264F-38169673DFD8";

                          //alert("Closing bleConn... bpmUUID => " + uuid);
                          var params = {address: uuid};
                            console.log("close: " + JSON.stringify(params));




                                $cordovaBluetoothLE.close(params).then(function(obj) {
                                  console.log("Close Success : " + JSON.stringify(obj));
                                  }, function(obj) {
                                    console.log("Close Error : " + JSON.stringify(obj));
                                  });


                }

                //$scope.loadingMessage = "Please Wait While Your BP Reading is Taken...";
                $scope.loadingMessage = {value1: "Connecting to BP Monitor"};
                //$scope.loadingMessage.value1 = "Connecting to BP Monitor";

              //CROSS PLATFORM
              $scope.initBTConn = function()
              {



                    $ionicLoading.show({
                          content: 'Loading',
                          animation: 'fade-in',
                          showBackdrop: true,
                          template: "<div class='loadingDiv'><div style='text-align:center;margin-left:auto;margin-right:auto;'><ion-spinner></ion-spinner><br/><br/><h4 style='text-align:center;'>Connecting to BP Monitor</h4><br /> (Ensure BP Monitor is Turned On)</div></div>",
                          maxWidth: 150,
                          showDelay: 0
                        });



                        // Set a timeout to clear loader, however you would actually call the $ionicLoading.hide(); method whenever everything is ready or loaded.
                      /*  $timeout(function () {
                          $ionicLoading.hide();

                        }, 20000); */





                        if (isIOS == true)
                        {




                                  $cordovaBluetoothLE.initialize({request:true}).then(null,
                                  function(obj) {
                                    //Handle errors
                                      console.log("Initialize Error : " + JSON.stringify(obj));

                                      $scope.showAlert = function() {

                                            $ionicLoading.hide();

                                            var alertPopup = $ionicPopup.alert({
                                              title: 'BP Monitor Connection Error!',
                                              template: 'App - BP Monitor has not connected Successfully. Please Restart BP Reading. Error: ' +  JSON.stringify(obj)
                                            });
                                          }
                                          $scope.showAlert();

                                  },
                                  function(obj) {
                                        //success
                                              scanBle();
                                  }
                                );
                    }
                    else if (isAndroid == true)
                    {
                    //  alert("starting android BTSerial... >> calling  connectBTSerial()");

                       connectBTSerial();

                    }
                    else {
                      alert("You are using the wrong Mobile Platform. Only iOS and Android are Supported.");
                    }

              } //end initBTConn






                          function scanBle()
                          {

                                      $cordovaBluetoothLE.startScan({serviceUuids:[]}).then(null,
                                      function(obj)
                                      {
                                        //Handle errors
                                        console.log("BLE Scan Error: " +  JSON.stringify(obj));

                                      },
                                      function(obj)
                                      {
                                                  if (obj.status == "scanResult")
                                                  {


                                                             if (obj.name == "Microlife" || obj.name == "microlife")
                                                              {

                                                                //Device found
                                                              console.log("******** DYNAMIC BLE Connection ******* Scan Result Present. obj.address: " + obj.address + " - full obj: " + JSON.stringify(obj) );
                                                                bpmUUID = obj.address;
                                                                window.localStorage['obj.address'] =  obj.address;

                                                                  connectBle(  bpmUUID  );

                                                              }
                                                              else {
                                                              //alert("No Microlife Device Found. Please ensure Microlife BP Monitor is ON and your Phone's Bluetooth is on");
                                                              }





                                                                    //STOP SCAN
                                                                    function stopScan()
                                                                    {
                                                                          console.log("Stop Scan");

                                                                          $cordovaBluetoothLE.stopScan().then(function(obj) {
                                                                            console.log("Stop Scan Success : " + JSON.stringify(obj));
                                                                          }, function(obj) {
                                                                              console.log("Stop Scan Error : " + JSON.stringify(obj));





                                                                          });
                                                                    }

                                                                    stopScan();


                                                  }
                                                    else if (obj.status == "scanStarted")
                                                      {
                                                        //Scan started
                                                     console.log("Scan Started");
                                                      }
                                                    }
                                                  );

                              } //end scanBLE



                                          function connectBle(bpmUUID)
                                          {

                                            //alert("starting connect ble....");
                                              $cordovaBluetoothLE.connect({address: bpmUUID }).then(null, function(obj)
                                              {


                                                        //Handle errors
                                                  //  alert("Connect Error! Message: " + JSON.stringify(obj));


                                                                      if (obj.message == "Connection failed" )
                                                                      {

                                                                        $scope.showAlert = function() {
                                                                          $ionicLoading.hide();

                                                                              var alertPopup = $ionicPopup.alert({
                                                                                title: 'BP Monitor Connection Error!',
                                                                                template: 'Please Cancel this BP Reading and Start a New BP Reading. <b>Error:</b> ' +  JSON.stringify(obj)
                                                                              });
                                                                            }
                                                                            $scope.showAlert();

                                                                       closeConn(bpmUUID);



                                                                          }







                                                    },
                                                    function(obj) {
                                                      if (obj.status == "connected")
                                                      {

                                                        //This works... :) //subscribe next...
                                                        console.log("connected. obj is: " + JSON.stringify(obj));

                                                        //may need to do a scan first as it gives a "service not found" error
                                                        //do a discover/ services (ios)... first..
                                                     console.log("call showServices() ");
                                                     showServices();

                                                      }
                                                      else if (obj.status == "connecting") {
                                                        //Device connecting
                                                        console.log("connecting....");

                                                      } else {

                                                          console.log("Conn. else... Should trigger on ios. window.localStorage['isAndroid'] is: " + window.localStorage['isAndroid']);



                                                                    console.log("%%%%%%%%disconnected... thisBPData[12] ==>  " + thisBPData[12] );

                                                                    //######## START OF PARSE, SAVE LOCAL, POST TO CHG & showResult logic... LINES: 276-515


                                                                  //    if(thisBPData.length > 27 & thisBPData[12] != 114 )
                                                                    //  {




                                                                  //   } //end connect else... ///IE PARSE, POST, gotoRESULT logic NOW!!!!
                                                                          //  console.log(" Calling showBPData().... just after disconnected....");
                                                                            //showBPData();
                                                                        //  console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                          //  this.disconnect();

                                                                 console.log("*****00000000000000000 CLOSING BT CONN --- could cause trouble due to clearing of thisBPData");
                                                                      closeConn(bpmUUID);
                                                                      console.log("***** FINISHED CLOSING BT CONN");

                                                                       console.log("***** GOING TO btConnClosed...");
                                                                    //   $state.go('tab.btConnClosed');

                                                                }






                                                      }
                                                    ); //end connect.then...




                                          } //end connect




                                    function showServices()
                                    {
                                      var params = {address: bpmUUID , serviceUuids: [ ] };
                                      $cordovaBluetoothLE.services(params).then(success, error);



                                                    function success(obj)
                                                    {
                                                //  alert("Services Success : " + JSON.stringify(obj));


                                                      if (obj.status == "services")
                                                      {
                                                        console.log("obj.status == Services");


                                                       var fff0Service =  obj.services[2];

                                                       getCharacteristics(fff0Service);


                                                      }
                                                      else
                                                      {
                                                        console.log("Unexpected Services Status");
                                                      }
                                                    }

                                                    function error(obj)
                                                    {
                                                        console.log("Services Error : " + JSON.stringify(obj));

                                                        $scope.showAlert = function() {

                                                          $ionicLoading.hide();
                                                              var alertPopup = $ionicPopup.alert({
                                                                title: 'BP Monitor Connection Error!',
                                                                template: 'App - BP Monitor has not connected Successfully. Please Restart BP Reading.  <b>Error:</b> ' +  JSON.stringify(obj)
                                                              });
                                                            }
                                                            $scope.showAlert();

                                                    }



                                    } //end this.showServices()



                                  function getCharacteristics(svc)
                                    {


                                    var params = {address: bpmUUID , service: svc};
                                    $cordovaBluetoothLE.characteristics(params).then(success, error);

                                                    function success(obj)
                                                    {
                                                  //   alert("Characteristics Success : " + JSON.stringify(obj));

                                                      if (obj.status == "characteristics")
                                                      {
                                                    //    alert("obj.status == characteristics");

                                                        var characteristics = obj.characteristics;

                                                              var fff1ch = 'FFF1';
                                                              var fff2ch = 'FFF2';

                                                          //    alert("About to subscribeBle... >> " + bpmUUID +  svc + fff1ch );
                                                              subscribeBle(bpmUUID, svc, fff1ch);



                                                      }
                                                      else
                                                      {
                                                        console.log("Unexpected Characteristics Status");
                                                      }
                                                    }

                                                    function error(obj)
                                                    {
                                                      $scope.showAlert = function() {
                                                        $ionicLoading.hide();

                                                            var alertPopup = $ionicPopup.alert({
                                                              title: 'BP Monitor Connection Error!',
                                                              template: 'App - BP Monitor has not connected Successfully. Please Restart BP Reading. <b>Error:</b> ' +  JSON.stringify(obj)
                                                            });
                                                          }
                                                          $scope.showAlert();
                                                    }




                                    } //end getCHs


                                    function readBLE(uuid, svc, ch)
                                    {


                                      /*  $cordovaBluetoothLE.read(params1).then(function() { //Read 1
                                                              return $cordovaBluetoothLE.read(params2); //Read 2
                                                            }).then(function() {
                                                              return $cordovaBluetoothLE.read(params3); //Read 3
                                                            }).catch(function(err) {
                                                              console.log(err); //Catch any errors
                                                            }); */

                                                            var params = {
                                                                          "address": uuid,
                                                                          "service": svc,
                                                                          "characteristic": ch
                                                                        }
                                                            $cordovaBluetoothLE.read(params).then(success, error);

                                                            function readSuccess(result) {

                                                                                    console.log("readSuccess():");
                                                                                    console.log(JSON.stringify(result));

                                                                                    if (result.status === "read") {

                                                                                        //reportValue(result.service, result.characteristic, window.atob(result.value));
                                                                                        console.log("READ VALUE: " + JSON.stringify(result.value));
                                                                                    }
                                                                                }

                                                            function error(result)
                                                            {

                                                              console.log("read error :( " + JSON.stringify(result) );


                                                            }




                                    }




                                                function subscribeBle(uuid, svc, ch)
                                                {

                                                    console.log("***** START OF SubBLE......");



                                                    var params = {address:uuid, service:svc, characteristic:ch};
                                                    $cordovaBluetoothLE.subscribe(params).then(null, error, success);


                                                      //success == subscribed successfully;
                                                      function success(obj)
                                                      {
                                                        console.log("subscribe success------------------------");


                                                        //console.log("SUB. SUCCESS :) [only once I think]"); //NOOOO.... every array!!!!!


                                                          //this runs for each array received from bpm
                                                          //c. 2 arrays per reading stored on device... >> so can be a LOT of arrays!
                                                          if (obj.status == "subscribedResult")
                                                          {


                                                            console.log("subResult: obj: " +  JSON.stringify(obj.value) );


                                                                      byteArrayCount = byteArrayCount + 1;
                                                                      var bytes = $cordovaBluetoothLE.encodedStringToBytes(obj.value);


                                                                                      var bytesLen = bytes.length;
                                                                                      for (var i = 0; i < bytesLen; i++)
                                                                                      {
                                                                                    //  console.log("bytes[" + i + "] is: " + bytes[i]);
                                                                                        thisBPData.push(bytes[i]);
                                                                                      }


                                                                      //this if just to ensure that it only executes once...
                                                                      if (parsePostCounter < 1)
                                                                      {
                                                                         parsePostCounter++;
                                                                          //we know that after 1s all the data should be on phone, so safe to move on
                                                                          //sys valid ONLY if no error...
                                                                          var sys = thisBPData.length - 8;
                                                                          console.log("*** calling checkBPData(thisBPData)... thisBPData[length-8th]: " + thisBPData[sys]);


                                                                          $timeout(function() { checkBPData(thisBPData)},  8000 );
                                                                      }


                                                              }
                                                            else if (obj.status == "subscribed")
                                                            {
                                                              /*
                                                               $scope.showAlert = function() {
                                                                     var alertPopup = $ionicPopup.alert({
                                                                       title: 'Connected!',
                                                                       template: 'Succesfully Subscribed to the BP Monitor. Your reading will be completed in about 3 mins.'
                                                                     });
                                                                   }
                                                                   $scope.showAlert();
                                                                   */
                                                                    $ionicLoading.hide();

                                                              $ionicLoading.show({
                                                                    content: 'Loading',
                                                                    animation: 'fade-in',
                                                                    showBackdrop: true,
                                                                    template: "<div class='loadingDiv'><div style='text-align:center;margin-left:auto;margin-right:auto;'><ion-spinner></ion-spinner><br/><br/><h4 style='text-align:center;'>Succesfully Subscribed to the BP Monitor. Your reading will be completed in about 3 mins.</h4></div></div>",
                                                                    maxWidth: 150,
                                                                    showDelay: 0
                                                                  });





                                                            //  alert("Succesfully Subscribed to the BP Monitor. Your reading will be completed c. 3 mins.");
                                                              //use THIS to CONFIRM to user that you are connected!
                                                          console.log("Subscribed to BPM; parsePostCounter: " + parsePostCounter);


                                                            }
                                                            else
                                                            {
                                                            console.log("Unexpected Subscribe Status.");
                                                            $scope.showAlert = function() {
                                                              $ionicLoading.hide();

                                                                  var alertPopup = $ionicPopup.alert({
                                                                    title: 'BP Monitor Connection Error!',
                                                                    template: 'Please Cancel this BP Reading and Start a New BP Reading.  <b>Error:</b> ' + JSON.stringify(obj)
                                                                  });
                                                                }
                                                                $scope.showAlert();

                                                            }



                                                          } //end subscribeSuccess()


                                                          // subscribe Error present
                                                          function error(obj)
                                                          {

                                                            $scope.showAlert = function() {
                                                              $ionicLoading.hide();

                                                                  var alertPopup = $ionicPopup.alert({
                                                                    title: 'BP Monitor Connection Error!',
                                                                    template: 'Please Cancel this BP Reading and Start a New BP Reading. <br /> <b>Error:</b> ' +  JSON.stringify(obj)
                                                                  });
                                                                }
                                                                $scope.showAlert();


                                                          //  console.log("Subscribe Error : " + JSON.stringify(obj));
                                                          console.log("Subscribe Error : " + JSON.stringify(obj));



                                                          }

                                                          console.log("**** near end of subscribeBle... return == true");


                                                    return true;





                                                    console.log("**** RIGHT end of subscribeBle");
                                                } //end subscribeBle() ios





                                                $scope.cancelBPReading = function()
                                                {

                                                  window.localStorage['totalSymptomCount'] = 0;
                                                  window.localStorage['headachesSymptom'] = 0;
                                                  window.localStorage['visDSymptom'] = 0;
                                                  window.localStorage['epiGSymptom'] = 0;

                                                      $scope.showAlert = function() {
                                                            var alertPopup = $ionicPopup.alert({
                                                              title: 'Are you sure you want to Cancel this BP Reading?',
                                                              template: '',
                                                              buttons: [
                                                                       { text: 'Dont Cancel' },
                                                                       {
                                                                         text: '<b>Cancel BP Reading</b>',
                                                                         type: 'button-energized',
                                                                         onTap: function(e) {
                                                                           // add your action
                                                                           console.log("********** CANCELLING BT Conn....");
                                                                          closeBTConn(window.localStorage['obj.address']);
                                                                           $timeout(function() { $state.go('tab.dash');  },  1000 );



                                                                         }
                                                                       }
                                                                     ]
                                                            });
                                                          }
                                                          $scope.showAlert();


                                                }




  $scope.submitBPData = function(sys, dia, hr)
  {
    console.log("BP Manual... Sys: " + sys + " ; Dia: " + dia +  " ; Hr: " + hr );


                        if (sys == undefined || dia == undefined || hr == undefined )
                        {
                            alert("Enter a Proper BP Reading you FOOL!");

                        }
                        else {



                        var thisBPData = [];


                        thisBPData[11] = sys;
                        thisBPData[12] = dia;
                        thisBPData[13] = hr;

                         thisBPData[18] = sys;
                         thisBPData[19] = dia;
                         thisBPData[20] = hr;

                          thisBPData[25] = sys;
                          thisBPData[26] = dia;
                          thisBPData[27] = hr;

                          thisBPData[30] = sys;
                          thisBPData[31] = dia;
                          thisBPData[32] = hr;

                          //thisBPData[33] = 6;
                          thisBPData[33] = 5;
                          thisBPData[34] = 4;
                          thisBPData[35] = 3;
                          thisBPData[36] = 2;
                          thisBPData[37] = 1;


                          console.log("-- thisBPData[].length: " + thisBPData.length);
                          console.log("-- thisBPData[]: " + JSON.stringify(thisBPData));

                          var xSys = thisBPData.length - 8;
                          var xDia = thisBPData.length - 7;
                          var xHR = thisBPData.length - 6;

                          var bpmSys = thisBPData[xSys];
                          var bpmDia =  thisBPData[xDia];
                          var bpmHR =  thisBPData[xHR];

                           console.log("--manual entry -- : bpmSys: " + bpmSys);
                           console.log("--manual entry -- : bpmDia: " + bpmDia);
                           console.log("--manual entry -- : bpmHr: " + bpmHR);


                           //NOW CALL thisBP Data...
                           checkBPData(thisBPData);

                         }




  }
  
  
  
//################## WRITE CMD to BPM to CLEAR MEMORY after receiving BP Data ############################       
function btsWrite() 
{
                    console.log("...btsWrite() - clear mem");
  
                     //NATIVE CODE: public static final byte CMD_HBP16_BP_READ[] = {(byte)0x12,(byte)0x16,0x18, 0x28};
                     //this is cmd:: 4D FF 02 03 51
                     //0x is just a prefix to indicate hex...: http://www.bluesock.org/~willg/dev/ascii.html
                  
                    var data = new Uint8Array(5);
                    data[0] = 0x4D;
                    data[1] = 0xFF;
                    data[2] = 0x02;
                    data[3] = 0x03;
                    data[4] = 0x51;
                  bluetoothSerial.write(data, success, failure);
                  function success()
                  {
                        console.log(" ^^^ bluetoothSerial.write.SUCCESS... >> Memory should be cleared...");  
                  }
                  function failure()
                  {

                        console.log("^^^ bluetoothSerial.write.FAILURE... >> Memory  not be cleared...");  
                  }
                  
        
  
} //btsWrite


//putting it here for testing only... 
function writeBLE()
{
  console.log("...writeBLE() - clear mem");

        var data = new Uint8Array(5);
        data[0] = 0x4D;
        data[1] = 0xFF;
        data[2] = 0x02;
        data[3] = 0x03;
        data[4] = 0x51;
        
          var encodedString =  $cordovaBluetoothLE.bytesToEncodedString(data);
          console.log("--1495 -- encoded string is: " + JSON.stringify(encodedString));
        
        var addrrbts = window.localStorage['mlAddr']; // >> for bts... 
          
          var addrrble = window.localStorage['obj.address'];
          console.log("(dynamic) address of bpm is: " + addrrble);
          console.log("(actual) address  of bpm is: 1AB99BA6-EB25-5A8F-D115-73A3CC68621A");
          console.log("(bts) address of bpm is (should be diff): " + addrrbts);
        // var params = {value: encodedString, service: "FFF0", characteristic: "FFF2" ,type: "noResponse", address:"1AB99BA6-EB25-5A8F-D115-73A3CC68621A"};
        
        var params = {value: encodedString, service: "FFF0", characteristic: "FFF2" ,type: "noResponse", address:addrrble};
        
        $cordovaBluetoothLE.write(params).then(writeSuccess, writeError);
        
        function writeSuccess()
        {
             console.log("***** ble write SUCCESS..."); 
             //alert("***** ble write SUCCESS..."); 
               
        }

        function writeError()
        {
             console.log("***** ble write error..."); 
             //alert("***** ble write error..."); 
             
        }
        
    
       // bluetoothle.write(writeSuccess, writeError, params);

} //writeBLE


//################## ///////WRITE CMD to BPM to CLEAR MEMORY after receiving BP Data ############################   






                //CHECK BP Data Function (both bts and ble) >> the thisBPData array is passed in
                function checkBPData(thisBPData)
                {
                      
                      
                        if (isIOS == true)
                        {
                             console.log("... IOS so writeBLE()...");
                              writeBLE();
                        }
                        else
                        {
                             
                        }

                        if (isAndroid == true)
                        {
                             console.log("... is Android so btsWrite()....");
                             btsWrite();
                        } else {
                             
                        }
                      
                      
                      
                      
                      var rawdata = JSON.stringify(thisBPData);
                      console.log("*1032**: rawData is: " +  rawdata);
                      
                      var b64encoded = $base64.encode(rawdata);
                      console.log("**1035**: rawData in b64: " + b64encoded);
                      rawdata = b64encoded;
                     // rawdata = "abcdefg";
                     
                     
                     
                     
                    //why is bpmuuid == null
                    closeBTConn(bpmUUID);

                  if (thisBPData[12] == 114)
                  { //never gets here...
                    console.log("bpm error! thisBPData[12] is (should be 114): " + thisBPData[12]);
                    handleBPMError(thisBPData);
                  }
                  else
                  {
                        
                        

                            $timeout(function () {
                              $ionicLoading.hide();
                            }, 1000);

                                    //parse the data...

                                        console.log("** NO BPM... ERRORS..... thisBPData[]: " + JSON.stringify(thisBPData));

                                           console.log("#### BPM RESULT************");

                                             var xSys = thisBPData.length - 8;
                                             var xDia = thisBPData.length - 7;
                                             var xHR = thisBPData.length - 6;

                                              var bpmSys = thisBPData[xSys];
                                              var bpmDia =  thisBPData[xDia];
                                              var bpmHR =  thisBPData[xHR];

                                             console.log("bpmSys: " + bpmSys);
                                             console.log("bpmDia: " + bpmDia);
                                             console.log("bpmHr: " + bpmHR);

                                            console.log("#### ///////BPM RESULT************");
                                            //These are the Intermediate values that the device uses to calculate a reading
                                             var sys1 =  thisBPData[11];
                                             var dia1 = thisBPData[12];
                                             var hr1 = thisBPData[13];

                                              var sys2 =  thisBPData[18];
                                              var dia2 = thisBPData[19];
                                              var hr2 = thisBPData[20];

                                               var sys3 =  thisBPData[25];
                                               var dia3 = thisBPData[26];
                                               var hr3 = thisBPData[27];



                                               console.log("####Printing Intermediate Values...");
                                               console.log("sys1: " + sys1);
                                               console.log("dia1: " + dia1);
                                               console.log("hr1: " + hr1);

                                                console.log("sys2: " + sys2);
                                                console.log("dia2: " + dia2);
                                                console.log("hr2: " + hr2);

                                                 console.log("sys3: " + sys3);
                                                 console.log("dia3: " + dia3);
                                                 console.log("hr3: " + hr3);


                                                  console.log("#### ////// Printing Intermediate Values...");

                                                 $scope.result.finalSys = bpmSys;
                                                 $scope.result.finalDia = bpmDia
                                                 $scope.result.finalHr = bpmHR;

                                                 //MAP Calc. = [(2 x diastolic)+systolic] / 3
                                                 $scope.result.finalMap = ( (2 * $scope.result.finalDia) + $scope.result.finalSys) / 3;
                                                 //round to 2 dec. places
                                                 $scope.result.finalMap = $scope.result.finalMap.toFixed(2);

                                                 //set values to string + round them...
                                                 $scope.result.finalSys = Math.round($scope.result.finalSys);
                                                 $scope.result.finalDia = Math.round($scope.result.finalDia);
                                                 $scope.result.finalHr = Math.round($scope.result.finalHr);

                                                   window.localStorage['fSys'] = $scope.result.finalSys;
                                                   window.localStorage['fDia'] = $scope.result.finalDia;
                                                   window.localStorage['fHr'] = $scope.result.finalHr;
                                                   window.localStorage['fMap'] = $scope.result.finalMap;

                                                  console.log("wls[fsys, fdia etc ] Set:: sys: " + window.localStorage['fSys']  + "dia: " + window.localStorage['fDia'] + "hr: " + window.localStorage['fHr']  );

                                                 console.log("********** $scope.Result is FULLY SET: " + JSON.stringify($scope.result));
                                                   //Save Reading to AppDb - tbl: bp_reading_processed
                                                /*  var dt = new Date();
                                                  var utcDate = dt.toISOString(); */

                                                  var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                                                  var utcDate = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);





                                                  //var userID =  window.localStorage['loggedIn_USERID']; >> Deprecated way of doing insert
                                                  var userID = window.localStorage['loggedIn_USERNAME'];

                                                  var phoneID = window.localStorage['phoneUUID'];
                                                  window.localStorage['rawDataDate'] = utcDate;

                                                  if ( window.localStorage['totalSymptomCount'] == 0)
                                                  {
                                                    window.localStorage['noSymptoms'] = true;
                                                  }
                                                  else {
                                                    window.localStorage['noSymptoms'] = false;
                                                  }





                                                  //********* HAndling medical alerts HERE - BEFORE the insert to local db...
                                                  //need to handle medical alerts before I  insert locally as need the data/vars...
                                                  console.log("------ totalSymptomCount is: " + window.localStorage['totalSymptomCount']);
                                                  var symptomCount = window.localStorage['totalSymptomCount'];

                                                  console.log("***** symptomCount: " + symptomCount);


                                                  //HANDLE the medical alerts...
                                                  var medicalAlertPresent = null;
                                                  var medicalAlertName = null;
                                                  var medicalAlertDesc = null;
                                                  var medicalAlertOther = null;

                                                  function resetSecondHighLow()
                                                  {

                                                           if (window.localStorage['secondHighBP'] == "1")
                                                           {
                                                             window.localStorage['secondHighBP'] = 0;
                                                           }

                                                           if (window.localStorage['secondLowBP'] == "1")
                                                           {
                                                             window.localStorage['secondLowBP'] = 0;
                                                           }
                                                           if (window.localStorage['lastProteinReading'] =="1")
                                                           {
                                                             window.localStorage['lastProteinReading'] = 0;
                                                           }

                                                  }



                                                //  if ($scope.result.finalSys > 90 &  $scope.result.finalSys < 159 &  $scope.result.finalDia > 40 &  $scope.result.finalDia < 99 & window.localStorage['lastProteinReading'] != "+1" & window.localStorage['lastProteinReading'] != "+2" & window.localStorage['lastProteinReading'] != "+3"  )
                                                  if ($scope.result.finalSys >= 90 &  $scope.result.finalSys <= 139 &  $scope.result.finalDia >= 40 &  $scope.result.finalDia <= 90 ) //normal = < borderline high...
                                                  {
                                                        //ie THIS READING is NOT BLH
                                                        window.localStorage['lastBLH'] = 0;



                                                                if (symptomCount > 1)
                                                                {
                                                                   $state.go('tab.bpWarningMoreThanOneSymptom');
                                                                   //this handleMedicalAlert() function takes name, timestamp, whether the reading (assoiated to it) was synched or not,
                                                                  // the localBPR id and the inserted BPR id if there is one
                                                                //   handleMedicalAlert("> 1 Symptom & NORMAL BP", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                                                medicalAlertPresent = true;
                                                                medicalAlertName = " >1 Symptom & NORMAL BP";
                                                                medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this NORMAL BP Reading. The User is instructed to call the Leanbh Clinician.";





                                                                }
                                                                else
                                                                {
                                                                  //NORMAL RESULT... THIS has already been stored locally and sent to chg
                                                                  console.log("******Normal Reading........");
                                                                   $state.go('tab.btSSResult');

                                                                   medicalAlertPresent = false;
                                                                   medicalAlertName = "NORMAL BP Reading";
                                                                   medicalAlertDesc = "Symptoms <= 1 Systolic is >90 & <139 and Diastolic is >40 & <90. BP Reading is NORMAL.";

                                                                   resetSecondHighLow();

                                                                }


                                                  } //borderline high on it's own...
                                                  //if ($scope.result.finalSys >=140 &  $scope.result.finalSys < 159 &  $scope.result.finalDia >= 90 &  $scope.result.finalDia <= 99 & window.localStorage['lastProteinReading'] != "+1" & window.localStorage['lastProteinReading'] != "+2" & window.localStorage['lastProteinReading'] != "+3")
                                                  if ($scope.result.finalSys >=140 &  $scope.result.finalSys <= 159 &  $scope.result.finalDia >= 90 &  $scope.result.finalDia <= 99 & symptomCount < 1 & window.localStorage['lastProteinReading'] != "+4" & window.localStorage['lastProteinReading'] != "+1" & window.localStorage['lastProteinReading'] != "+2" & window.localStorage['lastProteinReading'] != "+3")
                                                  {

                                                            window.localStorage['lastBLH'] = 1;
                                                            window.localStorage['lastSys'] = $scope.result.finalSys;
                                                            window.localStorage['lastDia'] = $scope.result.finalDia;
                                                            window.localStorage['lastMap'] = $scope.result.finalMap;
                                                            window.localStorage['lastHr'] =  $scope.result.finalHr;

                                                            console.log("window.localStorage['lastBLH']**** : " + window.localStorage['lastBLH']);

                                                                if (symptomCount > 1)
                                                                {
                                                                   $state.go('tab.bpWarningMoreThanOneSymptom');
                                                                   //this handleMedicalAlert() function takes name, timestamp, whether the reading (assoiated to it) was synched or not,
                                                                  // the localBPR id and the inserted BPR id if there is one
                                                                //   handleMedicalAlert("> 1 Symptom & NORMAL BP", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                                                medicalAlertPresent = true;
                                                                medicalAlertName = " >1 Symptom & NORMAL BP";
                                                                medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this NORMAL BP Reading. The User is instructed to call the Leanbh Clinician.";





                                                                }
                                                                else
                                                                {
                                                                  //NORMAL RESULT... THIS has already been stored locally and sent to chg
                                                                   $state.go('tab.btSSResult');
                                                                   console.log("BLH Reading & lastProteinReading !>= +1**");
                                                                   medicalAlertPresent = false;
                                                                   medicalAlertName = "Borderline High BP Reading";
                                                                   medicalAlertDesc = "Symptoms <= 1 Systolic is >90 & <159 and Diastolic is >40 & <99. BP Reading is NORMAL.";

                                                                   resetSecondHighLow();

                                                                }


                                                  }
                                                 //else if ( $scope.result.finalSys >=160 || $scope.result.finalDia >= 100)
                                                  else if (window.localStorage['secondHighBP'] == "0" & $scope.result.finalSys >=160 || window.localStorage['secondHighBP'] == "0" & $scope.result.finalDia >= 100 )
                                                  {

                                                    //ie THIS READING is NOT BLH
                                                    window.localStorage['lastBLH'] = 0;

                                                                  if (symptomCount > 1)
                                                                  {
                                                                    //set secondHighBP to 1 in order to be on lookout for 2nd high bp reading
                                                                    window.localStorage['secondHighBP'] = 1;
                                                                     $state.go('tab.bpWarningMoreThanOneSymptom');

                                                                     medicalAlertPresent = true;
                                                                     medicalAlertName = " >1 Symptom & 1st High BP Reading";
                                                                     medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this HIGH BP Reading (Sys >=160 or Dia >= 100). The User is instructed to call the Leanbh Clinician.";

                                                                    // handleMedicalAlert("> 1 Symptom & 1st HIGH BP Reading", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                                                  }
                                                                  else
                                                                  {
                                                                      //set secondHighBP to 1 in order to be on lookout for 2nd high bp reading
                                                                    console.log("secondHighBP: " + window.localStorage['secondHighBP']);
                                                                     window.localStorage['secondHighBP'] = 1;
                                                                     $state.go('tab.bpWarning1');
                                                                    // handleMedicalAlert("1st HIGH BP Reading & No Symptoms", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                                                    medicalAlertPresent = true;
                                                                    medicalAlertName = "1st High BP Reading & No Symptoms";
                                                                    medicalAlertDesc = "This BP Reading is High (Sys >=160 or Dia >= 100) & No Symptoms present. The User is instructed to sit & relax and take another BP Reading in 15mins.";


                                                                  }



                                                 }
                                                 else if ( window.localStorage['secondLowBP'] == "0" & $scope.result.finalSys < 90 || window.localStorage['secondLowBP'] == "0" & $scope.result.finalDia < 40)
                                                 {

                                                   //ie THIS READING is NOT BLH
                                                   window.localStorage['lastBLH'] = 0;

                                                            if (symptomCount > 1)
                                                            {
                                                                //set secondLowBP to 1 in order to be on lookout for 2nd low bp reading
                                                              window.localStorage['secondLowBP'] = 1;
                                                               $state.go('tab.bpWarningMoreThanOneSymptom');
                                                               //handleMedicalAlert("> 1 Symptom & 1st LOW BP Reading", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );

                                                               medicalAlertPresent = true;
                                                               medicalAlertName = ">1 Symptom & 1st LOW BP Reading";
                                                               medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this LOW BP Reading (Sys < 90 or Dia < 40). The User is instructed to call the Leanbh Clinician.";


                                                            }
                                                            else
                                                            {
                                                                  window.localStorage['secondLowBP'] = 1;

                                                                  $state.go('tab.bpWarningLow');
                                                                  // handleMedicalAlert("1st LOW BP Reading & No Symptoms", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );

                                                                  medicalAlertPresent = true;
                                                                  medicalAlertName = "1st Low BP Reading & No Symptoms";
                                                                  medicalAlertDesc = "This BP Reading is Low (Sys < 90 or Dia < 40) & No Symptoms present. The User is instructed to sit & relax and take another BP Reading in 15mins.";



                                                            }



                                                 }
                                                 //SECOND LOW BP Reading
                                                 else if ( window.localStorage['secondLowBP'] == "1" & $scope.result.finalSys < 90 || window.localStorage['secondLowBP'] == "1" &  $scope.result.finalDia < 40)
                                                  {

                                                     window.localStorage['lastBLH'] = 0;

                                                    $state.go('tab.bpWarningLow2');
                                                    //handleMedicalAlert("2nd Low BP Reading", dateTime, chgSynched, 0,  window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );

                                                    medicalAlertPresent = true;
                                                    medicalAlertName = "2nd Low BP Reading";
                                                    medicalAlertDesc = "This is the second Consecutive LOW BP Reading (Sys < 90 or Dia < 40) in a short time period. The User is instructed to call the Leanbh Clinician.";


                                                    //reset it now.
                                                    window.localStorage['secondLowBP'] = 0;
                                                  }
                                                  //SECOND High BP Reading
                                                   //else if ( $scope.result.finalSys >=160 ||  $scope.result.finalDia >= 100 & secondHighBP == 1)
                                                    else if (  window.localStorage['secondHighBP'] == "1" & $scope.result.finalSys >=160 ||  window.localStorage['secondHighBP'] == "1" & $scope.result.finalDia >= 100)
                                                    {

                                                       window.localStorage['lastBLH'] = 0;


                                                     $state.go('tab.bpWarning2');
                                                     //handleMedicalAlert("2nd High BP Reading", dateTime, chgSynched, 0, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );

                                                     medicalAlertPresent = true;
                                                     medicalAlertName = "2nd High BP Reading";
                                                     medicalAlertDesc = "This is the second Consecutive HIGH BP Reading (Sys >=160 or Dia >= 100) in a short time period. The User is instructed to call the Leanbh Clinician.";

                                                      window.localStorage['secondHighBP'] = 0;


                                                   }

                                                 //1 symptom & boderline HIGH
                                                else if ( symptomCount == 1 & $scope.result.finalSys >= 140 &  $scope.result.finalSys <= 159  ||  symptomCount == 1 &  $scope.result.finalDia >=90  &  $scope.result.finalDia <= 99  )
                                                 {

                                                   console.log("****1288: symptomCount");
                                                   //ie THIS READING IS BLH
                                                   window.localStorage['lastBLH'] = 1;

                                                            if (symptomCount > 1)
                                                            {

                                                               $state.go('tab.bpWarningMoreThanOneSymptom');

                                                               medicalAlertPresent = true;
                                                               medicalAlertName = ">1 Symptom & Borderline High BP Reading";
                                                               medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this Borderline High BP Reading (Sys: 140-159 or Dia: 90-99). The User is instructed to call the Leanbh Clinician.";

                                                            }
                                                            else
                                                            {

                                                               //alert("3. Borderline High BP WITH >= 1 Symptom");
                                                               console.log("****1298: symptomCount: " + symptomCount);
                                                              $state.go('tab.bpWarningCombined1');
                                                              medicalAlertPresent = true;
                                                              medicalAlertName = "1 Symptom & Borderline High BP Reading";
                                                              medicalAlertDesc = "There is 1 Preganancy Symptom associated with this Borderline High BP Reading (Sys: 140-159 or Dia: 90-99). The User is instructed to call the Leanbh Clinician.";




                                                            }



                                                 }
                                              else if ( window.localStorage['lastProteinReading'] == "+4" ||  window.localStorage['lastProteinReading'] == "+1" || window.localStorage['lastProteinReading'] == "+2" || window.localStorage['lastProteinReading'] == "+3" )
                                              //else if ( window.localStorage['lastProteinReading'] == "+1" || window.localStorage['lastProteinReading'] == "+2" || window.localStorage['lastProteinReading'] == "+3" &  $scope.result.finalSys >= 14 &  $scope.result.finalSys > 15  ||   $scope.result.finalDia >=9  &  $scope.result.finalDia > 9  )
                                                 {
                                                   //ie THIS READING IS BLH BUT BUT BUT we can reset it to 0 because we CAPTURING the BLH+Protein>=+1 HERE.... (ie they did protein first and bp second.)
                                                   window.localStorage['lastBLH'] = 0;

                                                            if (symptomCount > 1 & $scope.result.finalSys >= 140 &  $scope.result.finalSys <= 159  || symptomCount > 1 & $scope.result.finalDia >=90  &  $scope.result.finalDia <= 99 )
                                                            {

                                                               $state.go('tab.bpWarningMoreThanOneSymptom');

                                                               medicalAlertPresent = true;
                                                               medicalAlertName = ">1 Symptom & Borderline High BP Reading & Protein: " + window.localStorage['lastProteinReading'];
                                                               medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this Borderline High BP Reading (Sys: 140-159 or Dia: 90-99) & Protein is: " + window.localStorage['lastProteinReading']  + ". The User is instructed to call the Leanbh Clinician.";

                                                            }
                                                            else if (symptomCount <= 1 & $scope.result.finalSys >= 140 &  $scope.result.finalSys <= 159  || symptomCount <= 1 & $scope.result.finalDia >=90  &  $scope.result.finalDia <= 99 )
                                                            {
                                                              //$scope.result.protein = window.localStorage['lastProteinReading'];
                                                              console.log("window.localStorage['lastProteinReading'] is: " + window.localStorage['lastProteinReading'] );

                                                              $state.go('tab.bpWarningCombined2');
                                                              medicalAlertPresent = true;
                                                              medicalAlertName = "Borderline High BP Reading & Protein: " + window.localStorage['lastProteinReading'];
                                                              medicalAlertDesc = "Borderline High BP Reading (Sys: 140-159 or Dia: 90-99) & Protein is " + window.localStorage['lastProteinReading']  + ". The User is instructed to call the Leanbh Clinician.";

                                                              window.localStorage['lastProteinReading'] = 0;

                                                            }



                                                 }
                                                 else if ( window.localStorage['lastProteinReading'] != "+4" ||  window.localStorage['lastProteinReading'] != "+1" || window.localStorage['lastProteinReading'] != "+2" || window.localStorage['lastProteinReading'] != "+3" )
                                                 //else if ( window.localStorage['lastProteinReading'] == "+1" || window.localStorage['lastProteinReading'] == "+2" || window.localStorage['lastProteinReading'] == "+3" &  $scope.result.finalSys >= 14 &  $scope.result.finalSys > 15  ||   $scope.result.finalDia >=9  &  $scope.result.finalDia > 9  )
                                                    {
                                                      //ie THIS READING IS BLH BUT BUT BUT we can reset it to 0 because we CAPTURING the BLH+Protein>=+1 HERE.... (ie they did protein first and bp second.)


                                                               if (symptomCount > 1 & $scope.result.finalSys >= 140 &  $scope.result.finalSys <= 159  || symptomCount > 1 & $scope.result.finalDia >=90  &  $scope.result.finalDia <= 99 )
                                                               {
                                                                     window.localStorage['lastBLH'] = 1;
                                                                  $state.go('tab.bpWarningMoreThanOneSymptom');

                                                                  medicalAlertPresent = true;
                                                                  medicalAlertName = ">1 Symptom & Borderline High BP Reading: ";
                                                                  medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this Borderline High BP Reading (Sys: 140-159 or Dia: 90-99)";

                                                               }
                                                               else if (symptomCount <= 1 & $scope.result.finalSys >= 140 &  $scope.result.finalSys <= 159  || symptomCount <= 1 & $scope.result.finalDia >=90  &  $scope.result.finalDia <= 99 )
                                                               {

                                                                     window.localStorage['lastBLH'] = 1;
                                                                 console.log("BLH DIA but no syms & protein < +4...");

                                                                 $state.go('tab.btSSResult');
                                                                 medicalAlertPresent = true;
                                                                 medicalAlertName = "<= 1 Symptom Protein < +4 & Borderline High BP Reading";
                                                                 medicalAlertDesc = "There <= 1 (of 3) Preganancy Symptoms associated with this Borderline High BP Reading (Sys: 140-159 or Dia: 90-99) & Protein < +4";

                                                                 window.localStorage['lastProteinReading'] = 0;

                                                               }



                                                    }






                                                  //21 vars here too
                                                  function insertProcessedBPData(dateTime, rawdata, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, phID,
                                                                                headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS,
                                                                                appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther)
                                                  {

                                                    console.log("*** Inserting Processed BP Data. medicalAlertPresent: " + medicalAlertPresent + " ; medicalAlertName: " + medicalAlertName  + " ; medicalAlertDesc: " + medicalAlertDesc + " ; medicalAlertOther: " + medicalAlertOther);

                                                          var iFeedback = 0;
                                                          var iOutcome =0;

                                                        /* bp_reading_processed table fields:
                                                          (bppID integar primary key, createdAt text, userID text, chgSynched text,
                                                          sys1 integar, dia1 integar, hr1 integar, sys2 integar, dia2 integar, hr2 integar, sys3 integar, dia3 integar, hr3 integar, finalSys integar, finalDia integar, finalHr integar, finalMap text,
                                                          feedback text, outcome text, phoneID text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text,
                                                          phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text, medicalAlertPresent integar, medicalAlertName text, medicalAlertDesc text, medicalAlertOther text ) */

                                                           db.transaction(function(tx) {
                                                                    var query = "INSERT INTO bp_reading_processed (createdAt, rawdata, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, feedback, outcome, phoneID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                    tx.executeSql(query, [dateTime, rawdata, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, iFeedback, iOutcome, phID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther ], function(tx, res) {

                                                                                console.log("INSERTED bpReadingProcessed. The ID is:" + res.insertId);
                                                                                 window.localStorage['bprid'] = res.insertId;

                                                                       }); //end outer tx >> insert into

                                                           },
                                                            function(err) {
                                                             console.log('app db ERROR on Inserting BP Reading: ' + JSON.stringify(err));
                                                           })

                                                                        console.log("just checking network...");
                                                                         //Check to see that we have network conn.
                                                                           var networkConn = networkService.check();

                                                                           console.log("networkConn == " + networkConn);

                                                                             if (networkConn == 1)  //++
                                                                             {
                                                                              console.log("**Insert to AppDB Done! Netw. Conn Present so calling postBPData()**");
                                                                              postBPData();
                                                                            //  checkBPResult(dateTime, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, phID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms);


                                                                             }
                                                                             else {
                                                                              console.log("«««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");
                                                                               // $state.go('tab.bpStep4');
                                                                               //if ios...

                                                                               //checkBPResult(dateTime, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, phID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms);



                                                                             }

                                                     } //end insertProcessedBPData()



                                                             var initChgSynched = 0;
                                                             //21 vars here...
                                                             //these seem to cause issue: window.localStorage['headachesSymptom'], window.localStorage['visDSymptom'], window.localStorage['epiGSymptom'], window.localStorage['noSymptoms']
                                                             insertProcessedBPData(utcDate, rawdata, userID, initChgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, $scope.result.finalSys, $scope.result.finalDia, $scope.result.finalHr, $scope.result.finalMap, phoneID, window.localStorage['headachesSymptom'], window.localStorage['visDSymptom'], window.localStorage['epiGSymptom'], window.localStorage['noSymptoms'],
                                                             window.localStorage['phoneUUID'], window.localStorage['phoneMake'], window.localStorage['phoneModel'], window.localStorage['phoneOS'], window.localStorage['appVersion'],
                                                            medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther);


                                                             console.log("inserting bpReadingProcessed DONE!. Now Sending to CHG.....");





                                                            //THIS IS NOT BEING CALLED I THINK >> I think it is actually >
                                                            function postBPData()
                                                            {


                                                                             var token = window.localStorage["TOKEN"];
                                                                             var _iOutcome = "BP Reading taken Successfully";
                                                                             var _iFeedback = "None";
                                                                             var fSys = $scope.result.finalSys;
                                                                             var fDia = $scope.result.finalDia;
                                                                             var fHr = $scope.result.finalHr;
                                                                             var fMap = $scope.result.finalMap;
                                                                             var iBPMID = 777;







                                               //SEND in array vs all vars...
                                              var bprArray = [];
                                              bprArray[0] = {"token":window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME']};



                                              bprArray[1] = {"createdAt": utcDate, "rawdata": rawdata, "username": window.localStorage['loggedIn_USERNAME'], "finalSys":fSys, "finalDia":fDia, "finalHr":fHr, "finalMap":fMap, "feedback":_iFeedback, "outcome": _iOutcome,
                                                             "phoneID":  window.localStorage['phoneUUID'], "headachesSymptom": window.localStorage['headachesSymptom'], "visDSymptom": window.localStorage['visDSymptom'], "epiGSymptom": window.localStorage['epiGSymptom'], "noSymptoms": window.localStorage['noSymptoms'],
                                                              "phoneUUID":window.localStorage['phoneUUID'], "phoneMake": window.localStorage['phoneMake'], "phoneModel": window.localStorage['phoneModel'], "phoneOS": window.localStorage['phoneOS'], "appVersion":window.localStorage['appVersion'],
                                                              "medicalAlertPresent": medicalAlertPresent, "medicalAlertName": medicalAlertName, "medicalAlertDesc": medicalAlertDesc
                                                           };

                                                                    console.log("***** bprArray: " + bprArray);


                                                                          /*   $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: window.localStorage['rawDataDate'], bprCreatedAt: utcDate,
                                                                             chgUserID: window.localStorage['loggedIn_USERNAME'], systolic: fSys, diastolic: fDia, hr: fHr, map: fMap, bprFeedback: _iFeedback, bprOutcome: _iOutcome,
                                                                              bpmID: iBPMID, phoneID: phoneID,  headachesSymptom: window.localStorage['headachesSymptom'], visDSymptom: window.localStorage['visDSymptom'],
                                                                              epiGSymptom: window.localStorage['epiGSymptom'], noSymptoms: window.localStorage['noSymptoms'], phoneMake: window.localStorage['phoneMake'], phoneModel: window.localStorage['phoneModel'], phoneOS: window.localStorage['phoneOS'],
                                                                              phoneUUID: window.localStorage['phoneUUID'], appVersion: window.localStorage['appVersion'],
                                                                              medicalAlertPresent: medicalAlertPresent, medicalAlertName: medicalAlertName,
                                                                              medicalAlertDesc: medicalAlertDesc, medicalAlertOther: medicalAlertOther  } }).then(function successCallback(response)
                                                                              */

                                                                              $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReadings", data: bprArray }).then(function successCallback(response)
                                                                              {

                                                                                         //console.log("*******************Inside the $http post... response json: " + JSON.stringify(response));
                                                                                         //console.log("token is: " + token);

                                                                                              var synchedFlag = 0;
                                                                                              synchedFlag = parseInt(response.data.synched);

                                                                                               //  window.localStorage['insertedCHGBPRID'] = response.data.chgbprid;
                                                                                              /// window.localStorage['insertedCHGBPRID']  = 999; // for now...
                                                                                                   console.log("synchedFlag == " + synchedFlag);


                                                                                           if (synchedFlag == 1)
                                                                                           {
                                                                                                            var zbprid = parseInt(window.localStorage['bprid']);

                                                                                                             function updateBPR() {

                                                                                                                              //NEW Style sqlite Query....
                                                                                                                              db.transaction(function(tx) {
                                                                                                                             //var query = "INSERT INTO bp_reading_processed (createdAt, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, feedback, outcome, phoneID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                                                             var uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE rowid = ?";
                                                                                                                             tx.executeSql(uQuery, [1, zbprid], function(tx, res) {
                                                                                                                                       //console.log("UPDATED bpReadingProcessed. The rowid is:" + res.rowid);
                                                                                                                                      // console.log("******* bp_reading_processed Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));
                                                                                                                                        //  window.localStorage['bprid'] = res.id;

                                                                                                                                        //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                                                                        deleteBPR();

                                                                                                                                }); //end outer tx >> insert into

                                                                                                                              },
                                                                                                                               function(err) {
                                                                                                                                console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                                                                                                              })




                                                                                                                } //end updateBPR


                                                                                                               updateBPR();



                                                                                                               //now do DELETE
                                                                                                               function deleteBPR()
                                                                                                               {

                                                                                                                 console.log("****  start of deletePR()....");

                                                                                                                              db.transaction(function(tx) {

                                                                                                                               var uQuery = "DELETE FROM bp_reading_processed WHERE chgSynched=1 AND userID = ?";
                                                                                                                               tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                                                         console.log("**DELETED  bp reading(s). The rowid is:" + res.rows.id);
                                                                                                                                         console.log("******* bp Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                                                          //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                                                                  }); //end outer tx >> insert into

                                                                                                                                },
                                                                                                                                 function(err) {
                                                                                                                                  console.log('app db ERROR on  DELETING (Synching) bp Reading: ' + JSON.stringify(err));
                                                                                                                                })


                                                                                                                  } //end deleteBPR


                                                                                                  } //end if synchedFlag ==1




                                                                                }, function errorCallback(response) {

                                                                                              console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                                                                           });

                                                                } //end postBPData




                                        } //end else (not a BPM error)

                } //end checkBPData




             //uuid for ios only
             function closeBTConn(uuid)
             {

               var isIOS = ionic.Platform.isIOS();
               var isAndroid = ionic.Platform.isAndroid();

                       if (isIOS == true)
                       {
                       closeConn(uuid);
                       }
                       else if (isAndroid == true) {
                            disconnectBTSerial();
                       }

              }





                function handleBPMError(thisBPData)
                {

                  $timeout(function () {
                    $ionicLoading.hide();
                  }, 1000);


                  var isIOS = ionic.Platform.isIOS();
                  var isAndroid = ionic.Platform.isAndroid();

                  var symptomCount = window.localStorage['totalSymptomCount'];
                  var uuid = bpmUUID;
              //   alert("BPM Error... bpmUUID: " + uuid);

              /*   var dt = new Date();
                 var utcDate = dt.toISOString(); */

                 var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                 var utcDate = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);

                                      if ( thisBPData[13] == 1)
                                      {
                                      //  alert("BPM Error 1");


                                        readingOutcome = "BPM Error 1";
                                        readingOutcomeDetails = "BPM Error 1: Pulse Signals too weak.";
                                        bpmError1 = true;

                                        var errorType = "BP Monitor Error";
                                        var errorDesc = readingOutcomeDetails;
                                        storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);


                                         console.log("inserting raw bp data to app db (bpm error 1)");
                                        // insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                         console.log("inserting raw bp data DONE!");


                                         $state.go('tab.bpError1');
                                         bpmError1 = false;

                                         closeBTConn(uuid);

                                      }
                                      else if ( thisBPData[13] == 2)
                                      {
                                      //  alert("BPM Error 2");


                                        readingOutcome = "BPM Error 2";
                                        readingOutcomeDetails = "BPM Error 2: Error Signal";
                                        bpmError2 = true;

                                        var errorType = "BP Monitor Error";
                                        var errorDesc = readingOutcomeDetails;
                                        storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);

                                         console.log("inserting raw bp data to app db (bpm error 2)");
                                      //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                         console.log("inserting raw bp data DONE!");

                                           $state.go('tab.bpError2');
                                           bpmError2 = false;
                                        closeBTConn(uuid);

                                      }
                                      else if ( thisBPData[13] == 3)
                                      {
                                //  alert("BPM Error 3");

                                      readingOutcome = "BPM Error 3";
                                      readingOutcomeDetails = "BPM Error 3: No Pressure in the Cuff";
                                      bpmError3 = true;

                                      var errorType = "BP Monitor Error";
                                      var errorDesc = readingOutcomeDetails;
                                      storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);

                                       console.log("inserting raw bp data to app db (bpm error 3)");
                                  //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                       console.log("inserting raw bp data DONE!");

                                         $state.go('tab.bpError3');
                                         bpmError3 = false;
                                         closeBTConn(uuid);


                                      }
                                      else if (  thisBPData[13] == 5)
                                      {
                                      console.log("BPM Error 5");

                                        readingOutcome = "BPM Error 5";
                                        readingOutcomeDetails = "BPM Error 5: Abnormal Result";
                                        bpmError5 = true;

                                        var errorType = "BP Monitor Error";
                                        var errorDesc = readingOutcomeDetails;
                                        storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);


                                         console.log("inserting raw bp data to app db (bpm error 5)");
                                      //   insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                         console.log("inserting raw bp data DONE!");

                                           $state.go('tab.bpError5');
                                           bpmError5 = false;
                                          closeBTConn(uuid);

                                      }
                                      else if (  thisBPData[13] == 66)
                                      {
                                      console.log("BPM Error - Low Batteries");

                                        readingOutcome = "BPM Error - Low Batteries";
                                        readingOutcomeDetails = "BPM Error: Low Batteries";
                                        bpmErrorLowBatt = true;

                                        var errorType = "BP Monitor Error";
                                        var errorDesc = readingOutcomeDetails;
                                        storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);

                                         console.log("inserting raw bp data to app db (bpm error 4)");
                                      //  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                         console.log("inserting raw bp data DONE!");

                                         $state.go('tab.bpmErrorLowBatt');
                                         bpmError5 = false;
                                        closeBTConn(uuid);

                                      }
                                      else
                                      {
                                        console.log("Other BPM Error!");

                                           readingOutcome = "BPM Error - Other";
                                           readingOutcomeDetails = "BPM Error Other";
                                           bpmErrorOther = true;

                                           var errorType = "BP Monitor Error";
                                           var errorDesc = readingOutcomeDetails;
                                           storeDeviceError.store(readingOutcomeDetails, errorType, readingOutcome);


                                           console.log("inserting raw bp data to app db (bpm error other)");
                                      //     insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                           console.log("inserting raw bp data DONE!");

                                             $state.go('tab.bpErrorOther');
                                             bpmErrorOther = false;
                                             closeBTConn(uuid);


                                      }






                } //end handleBPMError()





              //**************************************Android / BT Serial*********************************************************

              //android specific bluetooth code... (BT Serial / SPP)
                  function enableBTSerial()
                  {

                                          bluetoothSerial.enable( function()
                                          {
                                            //user enabled BT
                                          connectBTSerial();

                                          },  function()
                                              {
                                                  console.log("The user did *not* enable Bluetooth");
                                              });

                  }

                  function isBTSerialEnabled()
                  {

                                            bluetoothSerial.isEnabled(
                                                      function() {
                                                      console.log("BTSERIAL is enabled");




                                                                // listPairedDevices();


                                                      },
                                                      function() {
                                                          console.log("BTSERIAL is *not* enabled... calling enable bt serial");
                                                          enableBTSerial();
                                                      }
                                                  );




                    }


                    function disconnectBTSerial()
                    {

                    //  alert("disconnectBTSerial...");

                      $scope.result.finalSys = "";
                     $scope.result.finalDia = "";
                     $scope.result.finalHr =  "";
                     $scope.result.finalMap = "";


                     $scope.checkboxModel.value1 = false;
                     $scope.checkboxModel.value2 = false;
                     $scope.checkboxModel.value3 = false;
                     $scope.checkboxModel.value4 = false;

                         thisBPData = [];
                         byteArrayCount = 0;
                         parsePostCounter = 0;
                         symptomCount = 0;

                         function resetSymptoms()
                         {
                            console.log("*** resetting symptoms...");
                               window.localStorage['headachesSymptom'] = 0;
                               window.localStorage['visDSymptom'] = 0;
                               window.localStorage['epiGSymptom'] = 0;
                               window.localStorage['noSymptoms'] = null;
                         }

                          $timeout(function() { resetSymptoms() },  10000 );


                          console.log("about to disconnectBTSerial...");
                          //THIS DOESNT wORK i thinkk..s
                          //bluetoothSerial.disconnect([success], [failure]);
                          bluetoothSerial.disconnect()


                    }

                    function listPairedBTSerialDevices()
                    {



                      bluetoothSerial.list(function(devices) {
                                  devices.forEach(function(device) {
                                      console.log("Paired Devices: " + device.id);
                                      console.log("device:: " + JSON.stringify(device));

                                        if (device.name == "Microlife" || device.name == "microlife")
                                        {
                                          //console.log(device.name + "'s address is: " + device.id);
                                          window.localStorage['mlAddr'] = device.id;
                                          //console.log("window.localStorage['mlAddr'] is: " + window.localStorage['mlAddr']);
                                        }
                                        else {
                                        //alert("No Microlife Device Found. Please ensure Microlife BP Monitor is Paired to your Phone and your Phone's Bluetooth is on. Worst case Upair All Microlife devices and Re-Pair the one you are using. The PIN is: 4103");
                                        }


                                  })

                                //  window.localStorage['mlAddr'] = 1111;

                              }, failure);

                              function failure()
                              {
                                console.log("bts list failure...");
                              }



                    } //end listPairedBTSerialDevices



                  function connectBTSerial()
                  {


                    //listPairedBTSerialDevices();

                        console.log("connecting to BTSerial...");

                                    isBTSerialEnabled();






                                      console.log("****** about to do BT Serial Connect!!");

                                      bluetoothSerial.list(function(devices) {
                                                  devices.forEach(function(device) {
                                                      console.log("Paired Devices: " + device.id);
                                                      console.log("device:: " + JSON.stringify(device));

                                                        if (device.name == "Microlife" || device.name == "microlife")
                                                        {
                                                          //console.log(device.name + "'s address is: " + device.id);
                                                          window.localStorage['mlAddr'] = device.id;

                                                          //this is SET in Platform.ready()
                                                        console.log("connecBTSerial: " + window.localStorage['mlAddr']);
                                                          var addrx = window.localStorage['mlAddr'];
                                                          bpmUUID = addrx;

                                                        //addrx = "8C:DE:52:C9:40:63"; >> hardcoded def works....
                                                          bluetoothSerial.connect(addrx, connectSuccess, connectFailure);

                                                        }
                                                        else {

                                                        //alert("No Microlife Device Found. Please ensure Microlife BP Monitor is Paired to your Phone and your Phone's Bluetooth is on. Worst case Upair All Microlife devices and Re-Pair the one you are using. The PIN is: 4103");
                                                      /*  $scope.showAlert = function() {
                                                              var alertPopup = $ionicPopup.alert({
                                                                title: 'No Microlife Device Paired with this Phone!',
                                                                template: 'No Microlife BP Monitor Paired with Phone. Unpair all Microlife devices and Re-Pair.'
                                                              });
                                                            }
                                                            $scope.showAlert(); */



                                                        }


                                                  })

                                                //  window.localStorage['mlAddr'] = 1111;

                                              }, failure);

                                              function failure()
                                              {
                                                console.log("bts list failure...");
                                                $scope.showAlert = function() {
                                                  $ionicLoading.hide();

                                                      var alertPopup = $ionicPopup.alert({
                                                        title: 'BP Monitor Connection Error!',
                                                        template: 'No Microlife BP Monitor Paired with Phone. Unpair all Microlife devices and Re-Pair. <b>Error:</b> ' +  JSON.stringify(obj)
                                                      });
                                                    }
                                                    $scope.showAlert();



                                              }




                                                function connectSuccess()
                                                {

                                                  $ionicLoading.hide();

                                            $ionicLoading.show({
                                                  content: 'Loading',
                                                  animation: 'fade-in',
                                                  showBackdrop: true,
                                                  template: "<div class='loadingDiv'><div style='text-align:center;margin-left:auto;margin-right:auto;'><ion-spinner></ion-spinner><br/><br/><h4 style='text-align:center;'>Succesfully Subscribed to the BP Monitor. Your reading will be completed in about 3 mins.</h4></div></div>",
                                                  maxWidth: 150,
                                                  showDelay: 0
                                                });



                                                /*     $scope.showAlert = function() {
                                                           var alertPopup = $ionicPopup.alert({
                                                             title: 'Connected!',
                                                             template: 'Succesfully Subscribed to the BP Monitor. Your reading will be completed in about 3 mins.'
                                                           });
                                                         }
                                                         $scope.showAlert(); */

                                                console.log("Connected via BT Serial. Now Calling subcribe()");
                                                  subscribeBTSerial();
                                                }

                                                function connectFailure()
                                                {
                                                 //alert("Connection Failure!");

                                                    thisBPData = [];
                                                    byteArrayCount = 0;

                                                      if ( thisBPData.length > 0)
                                                      {

                                                    /* $scope.showAlert = function() {
                                                            var alertPopup = $ionicPopup.alert({
                                                              title: 'BP Monitor Connection Failure & BP Data Available!',
                                                              template: 'Please Restart BP Reading.'
                                                            });
                                                          }
                                                          $scope.showAlert(); */


                                                      console.log("normal disconnectBTSerial!");
                                                        disconnectBTSerial();


                                                      }
                                                      else
                                                      {
                                                            console.log("Connection Failure + No Data from BPM. RESTARTING BP Reading!");
                                                        //  $state.go('tab.bpErrorOther');
                                                      //  alert("***** GOING TO btConnClosed...");
                                                      //   $state.go('tab.btConnClosed');

                                                      //alert("bts conn failure...");

                                                      $scope.showAlertCF = function() {

                                                          $ionicLoading.hide();

                                                          var alertPopup = $ionicPopup.alert({
                                                            title: 'BP Monitor Connection Failure!',
                                                            template: 'Please Cancel this BP Reading and Start a New BP Reading.'
                                                          });
                                                        }
                                                        $scope.showAlertCF();

                                                          disconnectBTSerial();


                                                      }


                                                }







                    } //end connectBTSerial




                                function subscribeBTSerial()
                                {
                                   //empty  thisBPData = []; WHEN Subscribe is called!!
                                  // thisBPData = [];
                                  console.log(" start of subscribeBTSerial thisBPData = ?? " +  JSON.stringify(thisBPData));

                                    console.log("start of this.subscribeBTSerial... ");
                                  //  var byteArrayCount = null;
                                  //  byteArrayCount = 0;

                                    //var counter1 = 0;
                                    window.localStorage['counter1'] = null;
                                    window.localStorage['counter1'] = 0;


                                                            bluetoothSerial.subscribeRawData(onRawData, subscribeFailed);

                                                                // this runs for every byteArray.... could run 5- * TIMES > depending on data in bpm
                                                                function onRawData(data)
                                                                {

                                                                console.log("raw data received!!! data: " + JSON.stringify(data));

                                                                    //this is a counter to see which array we are on... there will usually be many arrays and we are most interested in the first one
                                                                    byteArrayCount = byteArrayCount + 1;
                                                                    //alert("Just incremented byteArrayCount. It's now = " + byteArrayCount);



                                                                      var rawData = new Uint8Array(data);
                                                                        console.log("rawData (json) is: " + JSON.stringify(rawData) );
                                                                        var dataArrayJSON = JSON.stringify(rawData);


                                                                        for (var e = 0; e < rawData.length; e++)
                                                                        {
                                                                        thisBPData.push(rawData[e]);
                                                                        }



                                                                        //this if just to ensure that it only executes once...
                                                                        if (parsePostCounter < 1)
                                                                        {


                                                                           parsePostCounter++;
                                                                            //we know that after 1s all the data should be on phone, so safe to move on
                                                                            //sys valid ONLY if no error...
                                                                            var sys = thisBPData.length - 8;
                                                                        console.log("*** calling checkBPData(thisBPData)... thisBPData[length-8th]: " + thisBPData[sys] + " ; thisBPData[] is: " + JSON.stringify(thisBPData));

                                                                            //We timeout for 1000ms (1s) to ENSURE that ALL BP Data has come across from BP Monitor
                                                                            //Risk here is that IF for some reason data transfer takes longer than 1s, we dont get all the data loaded into thisBPData array
                                                                            // >>> WAIT to see if we run into issues in testing and if so > to 3000
                                                                            $timeout(function() { checkBPData(thisBPData)},  8000 );
                                                                        }









                                                                } //end onRawData()

                                                                function subscribeFailed()
                                                                {
                                                                  console.log("subscribe btSerial failed");

                                                                  $scope.showAlert = function() {
                                                                        var alertPopup = $ionicPopup.alert({
                                                                          title: 'BP Monitor Subscribe Failure Error!',
                                                                          template: 'Please Restart BP Reading.  <b>Error:</b> ' +  JSON.stringify(obj)
                                                                        });
                                                                      }
                                                                      $scope.showAlert();


                                                                }




                                  } //end subscribeBTSerial




















}) //end of btSS (bluetooth SImple Stupid) Controller



.controller('DashCtrl', function(storeDeviceError, $rootScope, encDcrypt, $ionicPopup, $cordovaProgress, synchAppCHGService, networkService, synchService, bleService, testLocalDBService, btSerialService, chgServerApi, $base64, $scope, $http, $state, $timeout, CipherService, Auth, $cordovaSQLite, $ionicModal, $cordovaBluetoothLE) {





//check the user is auth'd properly...
 Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );


/* var myVar = setInterval(myTimer, 4000);

 function myTimer() {
 var d = new Date();
 var time = d.toLocaleTimeString();
console.log("Time Now is: " + time);
 } */

 $scope.resetBPSymptoms = function()
 {
     console.log("*** resetting BP symptoms...");
      window.localStorage['headachesSymptom'] = 0;
      window.localStorage['visDSymptom'] = 0;
      window.localStorage['epiGSymptom'] = 0;
      window.localStorage['noSymptoms'] = null;
      window.localStorage['totalSymptomCount'] = 0;


 }

console.log("window.localStorage['lastBLH']: " + window.localStorage['lastBLH'] );


   $scope.bpDataSys = window.localStorage['finalSys'];
    $scope.bpDataDia = window.localStorage['finalDia'];
      $scope.bpDataHr =  window.localStorage['finalHr'];
      $scope.bpDataMap = window.localStorage['finalMap'];

var symptomCountProtein = null;


$scope.proteinEnabled = {value1: false, value2: false};
$scope.proteinEnabled.value1 = window.localStorage['proteinDisabled'];
$scope.proteinEnabled.value2 = window.localStorage['proteinErrorDisabled'];



window.localStorage['totalSymptomCount'] = parseInt(window.localStorage['headachesSymptom']) + parseInt(window.localStorage['visDSymptom']) + parseInt(window.localStorage['epiGSymptom']);
console.log(" ");
console.log("*****^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );
console.log(" ");

//Localdb data vars... for ui...
$scope.localData= {prReadings: "", bpReadings: "", deviceErrors: "", synchDisabled: false, totalToSynch: ""};





$scope.localData.prReadings = parseInt(window.localStorage['localPRReadings']);
$scope.localData.bpReadings = parseInt(window.localStorage['localBPReadings']);
$scope.localData.deviceErrors = parseInt(window.localStorage['localErrors']);

var x = isNaN($scope.localData.prReadings);
var y = isNaN($scope.localData.bpReadings);
var z = isNaN($scope.localData.deviceErrors);

if (x == true)
{
$scope.localData.prReadings = 0;
}

if (y == true)
{
$scope.localData.bpReadings = 0;
}

if (z == true)
{
  $scope.localData.deviceErrors = 0;
}



$scope.localData.totalToSynch = $scope.localData.prReadings + $scope.localData.bpReadings + $scope.localData.deviceErrors;
console.log("$scope.localData.totalToSynch:::: " + $scope.localData.totalToSynch);




$scope.localData.totalToSynch = parseInt($scope.localData.totalToSynch);
console.log("***$scope.totalToSynch: " + $scope.localData.totalToSynch);


if ($scope.localData.totalToSynch > 0)
{
$scope.localData.synchDisabled =  false;
}
else {
  $scope.localData.synchDisabled =  true;
}


console.log("$scope.localData is:: " + JSON.stringify($scope.localData));



//CHECKING HOW Many unsynched records there are...
$scope.checkLocalProteinData = function()
{
    //  var readingsToSynch = [];

    //  readingsToSynch =  checkLocalData.check();
    //  console.log("****  readingsToSynch[]: " + readingsToSynch );
  //checkLocalData.check();

  var localPRDataArray = [];

  var uQuery = "SELECT rowid, * FROM protein_reading WHERE userID = ? AND chgSynched = ?";

  var uQueryBP = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";


  var uQueryErr = "SELECT rowid, * FROM device_error WHERE userID = ? AND chgSynched = ?";


                              db.transaction(function(tx) {

                              //check protein_readings
                              tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                              {

                                    window.localStorage['localPRReadings'] = res.rows.length;
                                    console.log("There are: " + res.rows.length + " Protein Readings to synch!");


                                                  if(res.rows.length > 0)
                                                  {



                                                    } //if res.rows > ...
                                                    else {
                                                      console.log("No Protein Readings in AppDB to Synch.");
                                                    }




                                }); //end first select

                                    //check bpReadings
                                    //tx.executeSql(uQueryBP, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                tx.executeSql(uQueryBP, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                  //tx.executeSql("SELECT rowid, * FROM bp_reading_processed", function(tx, res)
                                    {
                                        console.log("--bpr tbl select...");

                                          window.localStorage['localBPReadings'] = res.rows.length;
                                          console.log("BPrs to synch: " + window.localStorage['localBPReadings']);

                                                        if(res.rows.length > 0)
                                                        {


                                                          } //if res.rows > ...
                                                          else {
                                                            console.log("No BP Readings in AppDB to Synch.");
                                                          }




                                      });//end 2nd select

                                  //check errors
                                  tx.executeSql(uQueryErr, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                  {


                                        //$scope.localData.prReadings = res.rows.length;
                                        window.localStorage['localErrors'] = res.rows.length;
                                        console.log("Errors to synch: " + window.localStorage['localErrors']);

                                                      if(res.rows.length > 0)
                                                      {



                                                        } //if res.rows > ...
                                                        else {
                                                          console.log("No ERRORS in AppDB to Synch.");
                                                        }




                                    });//end 2nd select






                        },
                         function(err) {
                          console.log('app db ERROR on checking localDB for unsynched Protein, BP Readings of Device Errors: ' + JSON.stringify(err));
                        }) //end transaction








} //end checkLocalProteinData()







 $timeout(function () {

$scope.checkLocalProteinData();

}, 1000);










 $scope.synchBPh = function()
 {




   var networkConn = networkService.check();
   console.log("networkConn == " + networkConn);

             if (networkConn == 1)
             {
                           $scope.localData.synchDisabled = true;

                                   ///THERE ARE ABOUT 40S BETWEEN EACH SYNCH WHICH minimizes the possiblity of overload etc...
                                   $timeout(function ()
                                   {
                                       var d = new Date();
                                       var time = d.toLocaleTimeString();
                                       console.log("** synchBP:: Time Now is: " + time);

                                       synchService.synchBP();
                                   }, 2000); //run this after 2s


                                   $timeout(function ()
                                   {
                                       var d = new Date();
                                       var time = d.toLocaleTimeString();
                                       console.log("** synchPR:: Time Now is: " + time);

                                       synchService.synchProteinReadings();
                                   }, 20000);

                                   $timeout(function ()
                                   {
                                       var d = new Date();
                                       var time = d.toLocaleTimeString();
                                       console.log("** synchDE:: Time Now is: " + time);

                                       synchService.synchDeviceErrors();
                                   }, 30000); //run this after 2s







              }
               else
               {

                           $scope.showAlert = function() {
                                 var alertPopup = $ionicPopup.alert({
                                   title: 'No Internet Connection',
                                   template: 'Cant Synch Data as No Internet Connection Available.'
                                 });
                               }
                               $scope.showAlert();

                   }

 } //end synchBPh





 /*
  $scope.synchReadings = function()
  {

    var networkConn = networkService.check();
    console.log("networkConn == " + networkConn);

              if (networkConn == 1)
              {

                                          $scope.localData.synchDisabled =  true;

                                          console.log("synching in 8s...");
                                        $timeout(function ()
                                        {
                                            var d = new Date();
                                            var time = d.toLocaleTimeString();
                                            console.log("** (from dash) synchPR:: Time Now is: " + time);

                                            synchService.synchProteinReadings();

                                                      $timeout(function () {  $scope.checkLocalProteinData(); }, 6000);



                                        }, 2000);

              }
              else {

              }


  } */










//=============Start of Urine Protein Reading Stuff =======================
//Symptoms
//symptom check box values...
$scope.checkboxModelPR = {
   value1 : false,
   value2 : false,
   value3 : false,
   value4: false
 };




$scope.captureHeadaches = function(item)
{

$scope.Headaches = item;
console.log("Headaches?: " + $scope.Headaches + "item: " + JSON.stringify(item));



      if (item == true)
      {
        window.localStorage['headachesSymptom'] = 1;
        window.localStorage['headachesSymptomText'] = "Yes";
        console.log("window.localStorage['headachesSymptom']: " + window.localStorage['headachesSymptom']);

        $scope.checkboxModelPR.value4 = false;

      }
      else {
        window.localStorage['headachesSymptom'] = 0;
        window.localStorage['headachesSymptomText'] = "N";
        console.log("window.localStorage['headachesSymptom']: " + window.localStorage['headachesSymptom']);
      }


}


$scope.captureVisD = function(item1)
{
$scope.VisD = item1;
console.log("Vis D?: " + $scope.VisD);

          if (item1 == true)
          {
            window.localStorage['visDSymptom'] = 1;
            console.log("window.localStorage['visDSymptom']: " + window.localStorage['visDSymptom']);

            $scope.checkboxModelPR.value4 = false;

          }
          else {
            window.localStorage['visDSymptom'] = 0;
            console.log("window.localStorage['visDSymptom']: " + window.localStorage['visDSymptom']);
          }

}

$scope.captureEpiG = function(item2)
{

          if (item2 == true)
          {
            window.localStorage['epiGSymptom'] = 1;
            console.log("window.localStorage['epiGSymptom']: " + window.localStorage['epiGSymptom']);

            $scope.checkboxModelPR.value4 = false;

          }
          else {
            window.localStorage['epiGSymptom'] = 0;
            console.log("window.localStorage['epiGSymptom']: " + window.localStorage['epiGSymptom']);
          }




$scope.EpiG = item2;
console.log("EpiG?: " + $scope.EpiG);



}

$scope.captureNone = function(item3)
{
$scope.None = item3;
console.log("No Symptoms?: " + $scope.None);

$scope.checkboxModelPR.value1 = false;
$scope.checkboxModelPR.value2 = false;
$scope.checkboxModelPR.value3 = false;



//item.checked = false;

}


    var moreThan1Symptom = false;





    //init symptomCount
    $scope.calcSymptomCount = function()
    {



               if ( $scope.checkboxModelPR.value1 == false &  $scope.checkboxModelPR.value2 == false &  $scope.checkboxModelPR.value3 == false &  $scope.checkboxModelPR.value4 == false)
               {

                 $scope.showAlert = function() {
                       var alertPopup = $ionicPopup.alert({
                         title: 'Please Select an Option',
                         template: 'Please select an option from the Symptom Check list'
                       });
                     }
                     $scope.showAlert();


               }
               else {
                 console.log("A Selection was Made!  at least 1 of the symptoms / none of the above was selected! $scope.checkboxModel.value4: " + $scope.checkboxModelPR.value4 );

                           if ($scope.checkboxModelPR.value4 == false)
                           {
                             window.localStorage['totalSymptomCount'] = parseInt(window.localStorage['headachesSymptom']) + parseInt(window.localStorage['visDSymptom']) + parseInt(window.localStorage['epiGSymptom']);
                             console.log(" ");
                             console.log("*****^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );
                             console.log(" ");

                                   $state.go("tab.proteinStep1");
                           }
                           else {
                             window.localStorage['totalSymptomCount'] = 0;
                             window.localStorage['headachesSymptom'] = 0;
                             window.localStorage['visDSymptom'] = 0;
                             window.localStorage['epiGSymptom'] = 0;

                             console.log(" ");
                             console.log("*****^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );
                             console.log(" ");

                                   $state.go("tab.proteinStep1");
                           }




               }






   } //end calcSymptomCount()


/*  OLD  $scope.calcSymptomCount()
   //init symptomCount
   $scope.calcSymptomCount = function()
   {
               //handle unselected symptom checkboxes
               if ( window.localStorage['headachesSymptom'] == null ||  window.localStorage['headachesSymptom'] == undefined )
               {
                window.localStorage['headachesSymptom'] = 0;
               }
               else if ( window.localStorage['visDSymptom'] == null ||  window.localStorage['visDSymptom'] == undefined )
               {
                  window.localStorage['visDSymptom'] = 0;
               }
               else if ( window.localStorage['epiGSymptom'] == null ||  window.localStorage['epiGSymptom'] == undefined )
               {
                  window.localStorage['epiGSymptom'] = 0;

               }



          if ($scope.None == true)
          {
            window.localStorage['noSymptoms'] = true;
            window.localStorage['totalSymptomCount'] = 0;
            symptomCountProtein = 0;

            console.log("^^^^total symptomCount is: " +   window.localStorage['totalSymptomCount'] );

          }
          else {
            window.localStorage['totalSymptomCount'] = parseInt(window.localStorage['headachesSymptom']) + parseInt(window.localStorage['visDSymptom']) + parseInt(window.localStorage['epiGSymptom']);
            symptomCountProtein = window.localStorage['totalSymptomCount'];
          }



  }
  */

 $scope.finishMedAlert = function()
 {
  console.log("med alert done...");

    $scope.resetBPSymptoms();

   //$state.go("tab.dash");


 }





/// end Symptoms


$scope.uaErrors = {
  "E1 Reference Pad Error Middle" : "E1 Reference Pad Error Middle",
  "E2 Wrong Strip" : "E2 Wrong Strip",
  "E3 Strip Measurement Error" : "E3 Strip Measurement Error",
  "E4 Calibration Error" : "E4 Calibration Error",
  "E5 Calibration Invalid" : "E5 Calibration Invalid",
  "E6 Chip Error" : "E6 Chip Error",
  "E7 Missing Tray" : "E7 Missing Tray",
  "E8 Tray Position Error" : "E8 Tray Position Error",
  "E9 Wrong Tray" : "E9 Wrong Tray",
  "E10 Light Barrier Error" : "E10 Light Barrier Error",
  "E11 Tray Step Error" : "E11 Tray Step Error",
  "E12 Optics Error" : "E12 Optics Error",
  "E14 Interface Error" : "E14 Interface Error",
  "E15 Reference Pad Error Bottom" : "E15 Reference Pad Error Bottom",
  "E16 Reference Pad Error Top" : "E16 Reference Pad Error Top",
  "Close Printer Cover" : "Close Printer Cover",
  "No Paper in Printer" : "No Paper in Printer",
  "No Printout" : "No Printout"
};

$scope.selectedProteinError = window.localStorage['selectedProteinError'];




$scope.ProteinOutcomes = {
    "" : "",
    "Neg" : "Neg",
    "+1" : "+1",
    "+2 ": "+2",
    "+3" : "+3",
    "+4" : "+4",
    "Urine Analyser Error" : "Urine Analyser Error"
};




$scope.data = {
   singleSelect: null
  };



  $scope.resetProteinSelect = function() {
    console.log("aaaa");
     $scope.data.singleSelect = null;
     console.log(" $scope.data.singleSelect: " +  $scope.data.singleSelect );
   };








$scope.captureProteinError = function(selectedProteinError)
{




             if (selectedProteinError == "" || selectedProteinError == null || selectedProteinError == undefined)
             {


              //alert("Please Ensure you have selected a valid outcome.");
              $scope.showAlert = function() {
                    var alertPopup = $ionicPopup.alert({
                      title: 'You did not select a valid Urine Analyser Error!',
                      template: 'Please Ensure you have selected a valid Urine Analyser Error from the Dropdown Menu'
                    });
                  }
                  $scope.showAlert();

             }
             else {

               window.localStorage['proteinErrorDisabled'] = true;

               window.localStorage['selectedProteinError'] = selectedProteinError;
               console.log("selectedProteinError: " + selectedProteinError);

                var errorType = "Urine Analyser Error";
                var errorDesc = "None";
                storeDeviceError.store(selectedProteinError, errorType, errorDesc);

               //$state.go("tab.dash");

               //call the store Protein Reading
               //storeProteinError(selectedProteinError);

               $scope.selectedProteinError = "";


             }


} //end captureProteinError








  //$scope.selectedProteinOutcome = window.localStorage['selectedProteinOutcome'];


  $scope.selectedProteinOutcome = { value1: "", sys: "", dia: "", map: "", hr: "" };
  $scope.selectedProteinOutcome.value1 = window.localStorage['selectedProteinOutcome'];
  $scope.selectedProteinOutcome.sys = window.localStorage['lastSys'];
  $scope.selectedProteinOutcome.dia = window.localStorage['lastDia'];
  $scope.selectedProteinOutcome.map = window.localStorage['lastMap'];
  $scope.selectedProteinOutcome.hr = window.localStorage['lastHr'];


$scope.captureProtein = function(selectedProteinOutcome)
{



        console.log("--- selectedProteinOutcome: " + selectedProteinOutcome);
        window.localStorage['selectedProteinOutcome'] = selectedProteinOutcome;


            console.log("--- $scope.selectedProteinOutcome:: " + $scope.selectedProteinOutcome.value1);

             if (selectedProteinOutcome == "" || selectedProteinOutcome == null || selectedProteinOutcome == undefined)
             {
              //alert("Please Ensure you have selected a valid outcome.");
              $scope.showAlert = function() {
                    var alertPopup = $ionicPopup.alert({
                      title: 'You did not select a valid outcome!',
                      template: 'Please Ensure you have selected a valid outcome from the Enter Protein Reading Value Dropdown Menu'
                    });
                  }
                  $scope.showAlert();

             }
             else {

                 window.localStorage['proteinDisabled'] = true;


            //   window.localStorage['selectedProteinOutcome'] = selectedProteinOutcome;
               console.log("selectedProteinOutcome: " + selectedProteinOutcome);


                        if (selectedProteinOutcome == "Urine Analyser Error")
                        {
                              $state.go("tab.proteinError");
                        }
                        else {
                            //$state.go("tab.proteinStep3");
                            //call the store Protein Reading
                            storeProteinReading(selectedProteinOutcome);
                          //  window.localStorage['lastProteinReading'] = selectedProteinOutcome;

                            $scope.selectedProteinOutcome = "";


                        }





             }


} //end captureProtein



                  //store it locally, then post to chg...

                  function storeProteinReading(selectedProteinOutcome)
                  {

                  /*
                  CREATE TABLE IF NOT EXISTS chg.protein_reading (
                    protein_reading_ID SERIAL PRIMARY KEY,
                    chgUserID int DEFAULT NULL,
                    monitoring_device_ID int DEFAULT NULL,
                    protein_reading varchar(200) DEFAULT NULL,
                    phone_meta_data_ID int DEFAULT NULL,
                    createdAt timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
                   );
                  */
                  var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                  var utcDate = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);


                  var userID =  window.localStorage['loggedIn_USERID'];
                  var phoneID = window.localStorage['phoneUUID'];
                  window.localStorage['rawDataDate'] = utcDate;

                  if ( window.localStorage['totalSymptomCount'] == 0)
                  {
                    window.localStorage['noSymptoms'] = true;
                  }
                  else {
                    window.localStorage['noSymptoms'] = false;
                  }



                  //********* HAndling medical alerts HERE - BEFORE the insert to local db...
                  //need to handle medical alerts before I  insert locally as need the data/vars...
                  console.log("------ totalSymptomCount is: " + window.localStorage['totalSymptomCount']);
                  var symptomCount = window.localStorage['totalSymptomCount'];

                  console.log("***** symptomCount: " + symptomCount);


                  //HANDLE the medical alerts...
                  var medicalAlertPresent = null;
                  var medicalAlertName = null;
                  var medicalAlertDesc = null;
                  var medicalAlertOther = null;

                    console.log("*******------ window.localStorage['lastBLH']: " + window.localStorage['lastBLH']);


                                 //Basic Medical alert for now... ie >1 symptom warning screen OR regualr result screen
                                if (symptomCount > 1 & window.localStorage['lastBLH'] != 1)
                                {
                                  console.log("^^^^^ symptomCount > 1 & window.localStorage['lastBLH'] != 1  ");
                                   $state.go('tab.proteinWarningMoreThanOneSymptom');
                                   //this handleMedicalAlert() function takes name, timestamp, whether the reading (assoiated to it) was synched or not,
                                  // the localBPR id and the inserted BPR id if there is one
                                //   handleMedicalAlert("> 1 Symptom & NORMAL BP", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                medicalAlertPresent = true;
                                medicalAlertName = ">1 Pregnancy Symptoms with this Protein Reading ";
                                medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this Protein Reading. The User is instructed to call the Leanbh Clinician. Protein Reading is: " + selectedProteinOutcome;;

                                 window.localStorage['lastBLH'] = 0;
                                 window.localStorage['lastProteinReading'] = selectedProteinOutcome;
                                }
                                else if (window.localStorage['lastBLH'] == 1 & selectedProteinOutcome == "+4" || window.localStorage['lastBLH'] == 1 & selectedProteinOutcome == "+1" || window.localStorage['lastBLH'] == 1 & selectedProteinOutcome == "+2" || window.localStorage['lastBLH'] == 1 & selectedProteinOutcome == "+3")
                                {
                                  window.localStorage['lastProteinReading'] = 0;
                                  window.localStorage['lastBLH'] = 0;


                                  console.log("^^^^^^^^^^^^^^^^^ window.localStorage['lastProteinReading']: " + window.localStorage['lastProteinReading'] );
                                  console.log("^^^ window.localStorage['lastBLH']: " + window.localStorage['lastBLH'] );

                                   $state.go('tab.proteinWarningCombined');
                                   //this handleMedicalAlert() function takes name, timestamp, whether the reading (assoiated to it) was synched or not,
                                  // the localBPR id and the inserted BPR id if there is one
                                //   handleMedicalAlert("> 1 Symptom & NORMAL BP", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                medicalAlertPresent = true;
                                medicalAlertName = "BP Borderline High & Protein >=+4";
                                medicalAlertDesc = "Protein Reading is >= +4 And Last BP Reading Was Borderline High. The User is instructed to call the Leanbh Clinician. Protein Reading is: " + selectedProteinOutcome;




                                $timeout(function () {


                                  //reset lastbp wls... (ie for BLH == 1)
                                  window.localStorage['lastSys'] = "";
                                  window.localStorage['lastDia'] = "";
                                  window.localStorage['lastMap'] = "";
                                  window.localStorage['lastHr'] = "";

                                  console.log("*** window.localStorage['lastSys'] , dia, map etc are = now! ");


                                }, 10000);



                                }
                                else
                                {
                                   console.log("::2968 ELSE...");
                                   window.localStorage['lastBLH'] = 0;

                                  //NORMAL RESULT... THIS has already been stored locally and sent to chg
                                   $state.go('tab.proteinStep3');

                                   medicalAlertPresent = false;
                                   medicalAlertName = "Protein Reading with <=1 Symptom";
                                   medicalAlertDesc = "Protein Reading is: " + selectedProteinOutcome;

                                   window.localStorage['lastProteinReading'] = selectedProteinOutcome;

                                }



                                // tx.executeSql("CREATE TABLE IF NOT EXISTS protein_reading (proteinID integar primary key, createdAt text, userID text,
                                // proteinReading text, chgSynched text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text,
                                // phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text,
                                //medicalAlertPresent text, medicalAlertName text, medicalAlertDesc text, medicalAlertOther text )");

                                function insertUrineData(dateTime, uid, chgSynched, proteinReading,
                                                              headachesSymptom, visDSymptom, epiGSymptom, noSymptoms,
                                                              phoneUUID, phoneMake, phoneModel, phoneOS, appVersion,
                                                              medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther)
                                {

                                  console.log("*** Inserting Urine Data. medicalAlertPresent: " + medicalAlertPresent + " ; medicalAlertName: " + medicalAlertName  + " ; medicalAlertDesc: " + medicalAlertDesc + " ; medicalAlertOther: " + medicalAlertOther);



                                         db.transaction(function(tx) {
                                                  var query = "INSERT INTO protein_reading (createdAt, userID, chgSynched, proteinReading, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                  tx.executeSql(query, [dateTime, uid, chgSynched, proteinReading, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther ], function(tx, res) {

                                                              console.log("INSERTED urineData. The ID is:" + res.insertId);
                                                               window.localStorage['proteinid'] = res.insertId;

                                                     }); //end outer tx >> insert into

                                         },
                                          function(err) {
                                           console.log('app db ERROR on Inserting Protein Reading: ' + JSON.stringify(err));
                                         })

                                                      console.log("just checking network...");
                                                       //Check to see that we have network conn.
                                                         var networkConn = networkService.check();

                                                         console.log("networkConn == " + networkConn);

                                                           if (networkConn == 1)
                                                           {
                                                            console.log("**Insert to AppDB Done! Netw. Conn Present so calling postProteinData()**");
                                                            postUrineData(selectedProteinOutcome, utcDate, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther);


                                                           }
                                                           else {
                                                            console.log("«««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");


                                                           }

                                   } //end insertProcessedBPData()



                                           var initChgSynched = 0;
                                           insertUrineData(utcDate, window.localStorage['loggedIn_USERNAME'], initChgSynched, selectedProteinOutcome, window.localStorage['headachesSymptom'], window.localStorage['visDSymptom'], window.localStorage['epiGSymptom'], window.localStorage['noSymptoms'],
                                           window.localStorage['phoneUUID'], window.localStorage['phoneMake'], window.localStorage['phoneModel'], window.localStorage['phoneOS'], window.localStorage['appVersion'],
                                            medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther);


                                           console.log("insertUrineData() DONE!. Now Sending to CHG.....");



                } //end of storeProteinReading




                function postUrineData(proteinReading, utcDate, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther )
                {


                                 var token = window.localStorage["TOKEN"];



                                   //SEND in array vs all vars...
                                  var urineArray = [];
                                  urineArray[0] = {"token":window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME']};




                                  urineArray[1] = {"createdAt": utcDate, "username": window.localStorage['loggedIn_USERNAME'], "proteinReading" : proteinReading,
                                                 "headachesSymptom": window.localStorage['headachesSymptom'], "visDSymptom": window.localStorage['visDSymptom'], "epiGSymptom": window.localStorage['epiGSymptom'], "noSymptoms": window.localStorage['noSymptoms'],
                                                  "phoneUUID":window.localStorage['phoneUUID'], "phoneMake": window.localStorage['phoneMake'], "phoneModel": window.localStorage['phoneModel'], "phoneOS": window.localStorage['phoneOS'], "appVersion":window.localStorage['appVersion'],
                                                  "medicalAlertPresent": medicalAlertPresent, "medicalAlertName": medicalAlertName, "medicalAlertDesc": medicalAlertDesc
                                               };

                                      console.log("***** urineArray: " +  urineArray);



                                                $http({  method: 'POST', url: chgServerApi.serverName + "/api/createProteinReadings", data: urineArray }).then(function successCallback(response)
                                                {



                                                                var synchedFlag = 0;
                                                                synchedFlag = parseInt(response.data.synched);

                                                                 //  window.localStorage['insertedCHGBPRID'] = response.data.chgbprid;
                                                                /// window.localStorage['insertedCHGBPRID']  = 999; // for now...
                                                                     console.log("synchedFlag == " + synchedFlag);


                                                             if (synchedFlag == 1)
                                                             {
                                                                              var proteinid = parseInt(window.localStorage['proteinid']);

                                                                               function updatePR() {

                                                                                                //NEW Style sqlite Query....
                                                                                                db.transaction(function(tx) {
                                                                                               //var query = "INSERT INTO bp_reading_processed (createdAt, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, feedback, outcome, phoneID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                               var uQuery = "UPDATE protein_reading SET chgSynched = ? WHERE rowid = ?";
                                                                                               tx.executeSql(uQuery, [1, proteinid], function(tx, res) {

                                                                                                      console.log("******* protein_reading Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));
                                                                                                      deletePR();

                                                                                                  }); //end outer tx >> insert into

                                                                                                },
                                                                                                 function(err) {
                                                                                                  console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                                                                                })




                                                                                  } //end updatePR


                                                                                 updatePR();


                                                                                 //now do DELETE
                                                                                 function deletePR()
                                                                                 {

                                                                                   console.log("****  start of deletePR()....");

                                                                                                db.transaction(function(tx) {

                                                                                                 var uQuery = "DELETE FROM protein_reading WHERE chgSynched=1 AND userID = ?";
                                                                                                 tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                           console.log("**DELETED  protein_reading(s). The rowid is:" + res.rows.id);
                                                                                                           console.log("******* protein_reading Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                            //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                                    }); //end outer tx >> insert into

                                                                                                  },
                                                                                                   function(err) {
                                                                                                    console.log('app db ERROR on  DELETING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                                  })


                                                                                    } //end deletePR


                                                                    } //end if synchedFlag ==1




                                                  }, function errorCallback(response) {

                                                                console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                                             });

                    } //end postUrineData












//resets the vars...
$scope.finishProteinReading = function()
{

  window.localStorage['proteinDisabled'] = false;
   window.localStorage['proteinErrorDisabled'] = false;

  $scope.resetProteinSelect();

  $scope.checkboxModelPR.value1 = false;
  $scope.checkboxModelPR.value2 = false;
  $scope.checkboxModelPR.value3 = false;
  $scope.checkboxModelPR.value4 = false;



console.log("... $scope.finishProteinReading()");
$scope.selectedProteinOutcome = "";
window.localStorage['selectedProteinOutcome'] = "";

window.localStorage['headachesSymptom'] = 0;
window.localStorage['visDSymptom'] = 0;
window.localStorage['epiGSymptom'] = 0;
window.localStorage['noSymptoms'] = false;

window.localStorage['totalSymptomCount'] = 0;






}




$scope.readSQLiteProteinData = function(username) {

  username = window.localStorage['loggedIn_USERNAME'];

                        db.transaction(function(tx) {
                        tx.executeSql("SELECT rowid, * FROM protein_reading where userID = ? ;", [username], function(tx, res) {
                        console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                        //console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");

                        console.log("res: " + JSON.stringify(res));

                        if(res.rows.length > 0) {
                     //  alert("#ROW 0 #: Time: " + res.rows.item(0).createdAt + "; bppID " + res.rows.item(0).rowid + "; PhoneID: " + res.rows.item(0).phoneID + "; Avgs::: Sys: " + res.rows.item(0).finalSys);
                       console.log("Now, here are all the Protein readings : " + JSON.stringify(res));

                         //alert(JSON.stringify(res));

                                            for(var i = 0; i < res.rows.length; i++)
                                            {


                                                console.log(JSON.stringify(res.rows.item(i)));


                                            }




                        } else {
                           console.log("No results found");
                        }



                      }); //end tx.executeSql
                    }); //end db.transaction

} //end readSQLiteProteinData()



//resets the chgSynched var in appdb to 0
$scope.proteinResetFlags = function(username)
{
  username = window.localStorage['loggedIn_USERNAME'];

    console.log("Resetting Flags now....");

            db.transaction(function(tx) {
             var uQuery = "UPDATE protein_reading SET chgSynched = ? WHERE userID = ?";
             tx.executeSql(uQuery, [0, username], function(tx, res) {


                       console.log("******* protein_reading Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                        //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                }); //end outer tx >> insert into

              },
               function(err) {
                console.log('app db ERROR on UPDATING (Synching) Protein Reading: ' + JSON.stringify(err));
              })


}

//Synching Logic for protein_readings
$scope.synchProteinReadings = function()
{
console.log("start of $scope.synchProteinReadings() ");

synchProteinData();

              function synchProteinData()
              {

                var synchDataArray = [];

                var uQuery = "SELECT rowid, * FROM protein_reading WHERE userID = ? AND chgSynched = ?";
                                      db.transaction(function(tx) {
                                      tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                      {
                                        //test lines to see if query is working...
                                        console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                        //console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                          if(res.rows.length > 0)
                                                          {

                                                                          //test
                                                                          //token for mollymattsson1@gmail.com / chguserid: 26
                                                                          //window.localStorage['TOKEN'] = null;

                                                                         synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                            for(var i = 0; i < res.rows.length; i++)
                                                                            {

                                                                              console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                               "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys);

                                                                               //push each row into the arrayF$httpF
                                                                               synchDataArray.push(res.rows.item(i));

                                                                              } //end loop here


                                                                              console.log("about to call postBPDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                              postProteinDataArray(synchDataArray);



                                                             console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                             console.log("synchDataArray.length: " + synchDataArray.length);
                                                             //console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                            // console.log("synchDataArray[0].createdAt: " + synchDataArray[0].createdAt + " ; synchDataArray[0].proteinReading: " + synchDataArray[0].proteinReading  + " ; synchDataArray[0].userID: " + synchDataArray[0].userID );
                                                            // console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) );





                                                            } //if res.rows > ...
                                                            else {
                                                              console.log("No Protein Readings in AppDB to Synch.");
                                                            }







                                        }); //end outer tx >> insert into

                                      },
                                       function(err) {
                                        console.log('app db ERROR on UPDATING (Synching) Protein Reading: ' + JSON.stringify(err));
                                      })






              } //end synchProteinData()



              function postProteinDataArray(synchDataArray )
              {
                    console.log("starting postProteinDataArray... array: " + JSON.stringify(synchDataArray));




                              //stuff that will always stay the same...
                               var token = window.localStorage["TOKEN"];


                               $http({  method: 'POST', url: chgServerApi.serverName + "/api/createProteinReadings", data: synchDataArray }).then(function successCallback(response)
                                {

                                           console.log("*******************Inside the Protein Readings SYNCH data array $http post... response json: " + JSON.stringify(response));
                                           //console.log("token is: " + token);

                                                var synchedFlag = 0;
                                                synchedFlag = parseInt(response.data.synched);
                                                console.log("synchedFlag == " + synchedFlag );


                                             if (synchedFlag == 1)
                                             {
                                                              //var zbprid = parseInt(window.localStorage['bprid']);
                                                            //  var zbprid = appBPRID;

                                                               function updatePR()
                                                               {

                                                                              db.transaction(function(tx) {
                                                                               var uQuery = "UPDATE protein_reading SET chgSynched = ? WHERE userID = ?";
                                                                               tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                         console.log("UPDATED protein_reading. The rowid is:" + res.rows.id);
                                                                                         console.log("******* protein_reading Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                         deletePR();

                                                                                          //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                  }); //end outer tx >> insert into

                                                                                },
                                                                                 function(err) {
                                                                                  console.log('app db ERROR on UPDATING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                })


                                                                  } //end updatePR

                                                                 updatePR();


                                                                 //now do DELETE
                                                                 function deletePR()
                                                                 {

                                                                   console.log("****  start of deletePR()....");

                                                                                db.transaction(function(tx) {

                                                                                 var uQuery = "DELETE FROM protein_reading WHERE chgSynched=1 AND userID = ?";
                                                                                 tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                           console.log("**DELETED  protein_reading(s). The rowid is:" + res.rows.id);
                                                                                           console.log("******* protein_reading Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                            //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                    }); //end outer tx >> insert into

                                                                                  },
                                                                                   function(err) {
                                                                                    console.log('app db ERROR on  DELETING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                  })


                                                                    } //end deletePR





                                                    } //end if synchedFlag ==1




                                  }, function errorCallback(response) {

                                                console.log("Error Sending Protein Reading Data to CHG: " + JSON.stringify(response));

                                             });

                  } //end postProteinDataArray






} ///// END of $scope.synchProteinReadings()...

//============= //// END OF Urine Protein Reading Stuff =======================

//================================= Synch Errors..... ================================

$scope.readSQLiteDeviceErrors = function(username) {

  username = window.localStorage['loggedIn_USERNAME'];

                        db.transaction(function(tx) {
                        tx.executeSql("SELECT rowid, * FROM device_error where userID = ? ;", [username], function(tx, res) {
                        console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                      //  console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");

                        console.log("res: " + JSON.stringify(res));

                        if(res.rows.length > 0) {
                     //  alert("#ROW 0 #: Time: " + res.rows.item(0).createdAt + "; bppID " + res.rows.item(0).rowid + "; PhoneID: " + res.rows.item(0).phoneID + "; Avgs::: Sys: " + res.rows.item(0).finalSys);
                       console.log("Now, here are all the Device Errors : " + JSON.stringify(res));

                         //alert(JSON.stringify(res));

                                            for(var i = 0; i < res.rows.length; i++)
                                            {


                                                console.log(JSON.stringify(res.rows.item(i)));


                                            }




                        } else {
                           console.log("No results found");
                        }



                      }); //end tx.executeSql
                    }); //end db.transaction

} //end readSQLiteDeviceErrors()



//resets the chgSynched var in appdb to 0
$scope.resetDeviceErrors = function(username)
{
  username = window.localStorage['loggedIn_USERNAME'];

    console.log("Resetting Flags now....");

            db.transaction(function(tx) {
             var uQuery = "UPDATE device_error SET chgSynched = ? WHERE userID = ?";
             tx.executeSql(uQuery, [0, username], function(tx, res) {


                       console.log("******* device_error Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                        //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                }); //end outer tx >> insert into

              },
               function(err) {
                console.log('app db ERROR on UPDATING (Synching) Device Error: ' + JSON.stringify(err));
              })


}

//Synching Logic for device_errors
$scope.synchDeviceErrors = function()
{
console.log("------start of $scope.synchDeviceErrors() ");

synchDeviceErrors();

              function synchDeviceErrors()
              {

                var synchDataArray = [];

                var uQuery = "SELECT rowid, * FROM device_error WHERE userID = ? AND chgSynched = ?";
                                      db.transaction(function(tx) {
                                      tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                      {
                                        //test lines to see if query is working...
                                        console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                        //console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                          if(res.rows.length > 0)
                                                          {

                                                                         synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                            for(var i = 0; i < res.rows.length; i++)
                                                                            {
                                                                            /*  console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                               "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys); */

                                                                               //push each row into the arrayF$httpF
                                                                               synchDataArray.push(res.rows.item(i));

                                                                              } //end loop here


                                                                              console.log("about to call postErrorDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                              postDeviceErrorsArray(synchDataArray);



                                                             console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                             console.log("synchDataArray.length: " + synchDataArray.length);

                                                          /*   console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                             console.log("synchDataArray[0].createdAt: " + synchDataArray[0].createdAt + " ; synchDataArray[0].proteinReading: " + synchDataArray[0].proteinReading  + " ; synchDataArray[0].userID: " + synchDataArray[0].userID );
                                                             console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) ); */





                                                            } //if res.rows > ...
                                                            else {
                                                              console.log("No Device Errors in AppDB to Synch.");
                                                            }







                                        }); //end outer tx >> insert into

                                      },
                                       function(err) {
                                        console.log('app db ERROR on UPDATING (Synching) Device Error(s): ' + JSON.stringify(err));
                                      })






              } //end synchDeviceErrors()



              function postDeviceErrorsArray(synchDataArray )
              {
                    console.log("starting postDeviceErrorsArray... array: " + JSON.stringify(synchDataArray));




                              //stuff that will always stay the same...
                               var token = window.localStorage["TOKEN"];


                               $http({  method: 'POST', url: chgServerApi.serverName + "/api/createDeviceErrors", data: synchDataArray }).then(function successCallback(response)
                                {

                                           console.log("*******************Inside the Protein Readings SYNCH data array $http post... response json: " + JSON.stringify(response));
                                           //console.log("token is: " + token);

                                                var synchedFlag = 0;
                                                synchedFlag = parseInt(response.data.synched);
                                                console.log("synchedFlag == " + synchedFlag );


                                             if (synchedFlag == 1)
                                             {
                                                              //var zbprid = parseInt(window.localStorage['bprid']);
                                                            //  var zbprid = appBPRID;

                                                               function updateDE()
                                                               {

                                                                              db.transaction(function(tx) {
                                                                               var uQuery = "UPDATE device_error SET chgSynched = ? WHERE userID = ?";
                                                                               tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                         console.log("UPDATED device_error. The rowid is:" + res.rows.id);
                                                                                         console.log("******* device_error Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                         deleteDE();

                                                                                          //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                  }); //end outer tx >> insert into

                                                                                },
                                                                                 function(err) {
                                                                                  console.log('app db ERROR on UPDATING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                })


                                                                  } //end updatePR

                                                                 updateDE();


                                                                 //now do DELETE
                                                                 function deleteDE()
                                                                 {

                                                                   console.log("****  start of deleteDE()....");

                                                                                db.transaction(function(tx) {

                                                                                 var uQuery = "DELETE FROM device_error WHERE chgSynched=1 AND userID = ?";
                                                                                 tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                           console.log("**DELETED  device_error(s). The rowid is:" + res.rows.id);
                                                                                           console.log("******* device_error Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                            //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                    }); //end outer tx >> insert into

                                                                                  },
                                                                                   function(err) {
                                                                                    console.log('app db ERROR on  DELETING (Synching) device error: ' + JSON.stringify(err));
                                                                                  })


                                                                    } //end deletePR





                                                    } //end if synchedFlag ==1




                                  }, function errorCallback(response) {

                                                console.log("Error Sending Device Error Data to CHG: " + JSON.stringify(response));

                                             });

                  } //end postDeviceErrorsArray






} ///// END of $scope.synchDeviceErrors()...








//================================= ///// END Synch Errors..... ================================



      //BP Readings reset flags...
      //resets the chgSynched var in appdb to 0
      $scope.resetFlags = function(chgUserID)
      {
        chgUserID = window.localStorage['loggedIn_USERID'];

          console.log("Resetting Flags now....");

                  db.transaction(function(tx) {
                   var uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE userID = ?";
                   tx.executeSql(uQuery, [0, 20], function(tx, res) {


                             console.log("******* bp_reading_processed Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                              //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                      }); //end outer tx >> insert into

                    },
                     function(err) {
                      console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                    })



      }





      //Synching BP Reading Logic!
      $scope.synch = function()
      {
      console.log("start of $scope.synchBPData()...");

      synchBPData();

                    function synchBPData()
                    {

                      var synchDataArray = [];

                      var uQuery = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                                            db.transaction(function(tx) {
                                            tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                            {
                                              //test lines to see if query is working...
                                              console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                              //console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                                if(res.rows.length > 0)
                                                                {

                                                                                //test
                                                                                //token for mollymattsson1@gmail.com / chguserid: 26
                                                                                //window.localStorage['TOKEN'] = null;

                                                                               synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                                  for(var i = 0; i < res.rows.length; i++)
                                                                                  {

                                                                                    console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                                     "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys);

                                                                                     //push each row into the arrayF$httpF
                                                                                     synchDataArray.push(res.rows.item(i));

                                                                                    } //end loop here


                                                                                    console.log("about to call postBPDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                                    postBPDataArray(synchDataArray);



                                                                   console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                                   console.log("synchDataArray.length: " + synchDataArray.length);
                                                                  // console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                                  // console.log("synchDataArray[0].sys1: " + synchDataArray[0].sys1 + " ; synchDataArray[0].dia1: " + synchDataArray[0].dia1  + " ; synchDataArray[0].hr1: " + synchDataArray[0].hr1 );
                                                                  // console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) );





                                                                  } //if res.rows > ...
                                                                  else {
                                                                    console.log("No BP Readings in AppDB to Synch.");
                                                                  }







                                              }); //end outer tx >> insert into

                                            },
                                             function(err) {
                                              console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                            })






                    } //end synchBPData()



                    function postBPDataArray(synchDataArray )
                    {
                          console.log("starting postBPDataArray... array: " + JSON.stringify(synchDataArray));




                                    //stuff that will always stay the same...
                                     var token = window.localStorage["TOKEN"];
                                     var _iOutcome = "";
                                     var _iFeedback = "";
                                     var iBPMID = 777;

                                     $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReadings", data: synchDataArray }).then(function successCallback(response)
                                      {

                                                 console.log("*******************Inside the SYNCH data array $http post... response json: " + JSON.stringify(response));
                                                 //console.log("token is: " + token);

                                                      var synchedFlag = 0;
                                                      synchedFlag = parseInt(response.data.synched);
                                                      console.log("synchedFlag == " + synchedFlag + " ; chgbprid: " + response.data.chgbprid );


                                                   if (synchedFlag == 1)
                                                   {
                                                                    //var zbprid = parseInt(window.localStorage['bprid']);
                                                                  //  var zbprid = appBPRID;

                                                                     function updateBPR()
                                                                     {

                                                                                    db.transaction(function(tx) {
                                                                                     var uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE userID = ?";
                                                                                     tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                               console.log("UPDATED bpReadingProcessed. The rowid is:" + res.rows.id);
                                                                                               console.log("******* bp_reading_processed Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                deleteBPR();
                                                                                                //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                        }); //end outer tx >> insert into

                                                                                      },
                                                                                       function(err) {
                                                                                        console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                                                                      })


                                                                        } //end updateBPR


                                                                       updateBPR();


                                                                       //now do DELETE
                                                                       function deleteBPR()
                                                                       {

                                                                         console.log("****  start of deletePR()....");

                                                                                      db.transaction(function(tx) {

                                                                                       var uQuery = "DELETE FROM bp_reading_processed WHERE chgSynched=1 AND userID = ?";
                                                                                       tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                 console.log("**DELETED  bp reading(s). The rowid is:" + res.rows.id);
                                                                                                 console.log("******* bp Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                  //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                          }); //end outer tx >> insert into

                                                                                        },
                                                                                         function(err) {
                                                                                          console.log('app db ERROR on  DELETING (Synching) bp Reading: ' + JSON.stringify(err));
                                                                                        })


                                                                          } //end deleteBPR


                                                          } //end if synchedFlag ==1




                                        }, function errorCallback(response) {

                                                      console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                                   });

                        } //end postBPDataArray



                        $scope.synchDeviceErrors();


      } ///// END of $scope.synch()...















$scope.testSQLC2 = function()
{

        /*    db.executeSql("pragma table_info (test_table);", [], function(res)
              {
        alert("app.js: PRAGMA res: " + JSON.stringify(res));
      }); */
          alert("testSQLC2().....");

                                                  db.transaction(function(tx) {

                                                  tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
                                                    alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                                    alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                                    });
                                            });

}




$scope.testSQLC = function()
{

//window.sqlitePlugin.echoTest(successCallback, errorCallback);
/*

          db.transaction(function(tx) {
              //alert("slqc.my.db SUCCESS... tx: " + JSON.stringify(tx));





                          //  tx.executeSQL(query, [dateTime, rawDataDate, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmid, phID]).then(function(res) {



                    var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                  //  tx.executeSql("INSERT INTO test_table (data, data_num) VALUES (?,?)", ["test", 100], function(tx, res) {
                    tx.executeSql(query, ["2016-04-05 19:30:23", "2016-04-05 19:30:23", 1601, 0, 139, 80, 99, 140, 81, 100, 141, 82, 101, 140, 81, 100, 99.99, 1, 1 ], function(tx, res) {


                        console.log("Sample Data Inserted into bp_reading_processed table :)");
                         alert("INSERTED bpReadingProcessed. The ID is:" + res.insertId);

                    //  alert("insertId: " + res.insertId + " -- probably 1");
                    //  alert("rowsAffected: " + res.rowsAffected + " -- should be 1");



                              /*  db.transaction(function(tx) {
                                tx.executeSql("select count(id) as cnt from test_table;", [], function(tx, res) {
                                //  alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                //  alert("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");
                                  });
                          });*/


/*


                  });



          }, function(err) {
            alert('Open database ERROR: ' + JSON.stringify(err));
          });

*/






            db.transaction(function(tx) {

                     var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                     tx.executeSql(query, ["2016-04-05 19:30:23", "2016-04-05 19:30:23", 1601, 0, 139, 80, 99, 140, 81, 100, 141, 82, 101, 140, 81, 100, 99.99, 1, 1 ], function(tx, res) {


                          alert("Inserted a row Successfully. res.insertId: " + res.insertId);


                                     db.transaction(function(tx) {
                                      tx.executeSql("select * from bp_reading_processed;", [], function(tx, res) {
                                       alert("res.rows.length: " + res.rows.length);
                                       alert("res.rows.item(0).cnt: " + res.rows.item(0).sys1 + " -- should be 139");
                                       alert("all rows (res): " + JSON.stringify(res));

                                     }); //end inner tx >> select *
                                }); //end inner d.TA
                        }); //end outer tx >> insert into

            },
             function(err) {
              alert('app db ERROR: ' + JSON.stringify(err));
            });













}






  $scope.setResultVars = function(sys, dia, hr, map)
  {

          /*    $scope.bpDataSys = sys;
              $scope.bpDataDia = dia;
              $scope.bpDataHr =  hr;
              $scope.bpDataMap = map; */
              console.log("''''''''''''''''' setResultVars().. sys:" + sys + "dia: " + dia);
                 window.localStorage['finalSys'] = sys;
                  window.localStorage['finalDia']
                    window.localStorage['finalHr']
                    window.localStorage['finalMap']







        //  alert("************** resultVars SET: " + "sys: " + $scope.bpDataSys + "; dia: " + $scope.bpDataDia + "; hr: " + $scope.bpDataHr);





 }




 //$scope.setResultVars();






  $scope.$on('eventFired', function(event, sys, dia, hr, map) {
        //$scope.someFunction();
        // alert("$emit received!! Setting Vars Now!");
         //$scope.setResultVars();

             //these vars are for the results screen
      /*      $scope.bpDataSys = window.localStorage['finalSys'];
            $scope.bpDataDia = window.localStorage['finalDia'];
            $scope.bpDataHr =  window.localStorage['finalHr'];
            $scope.bpDataMap = window.localStorage['finalMap']; */

              //dont think this will make a difference...

                    //alert has right values BUT the screen show the old values!!
                    //THIS alert fires 4-5 times when using $emit... > Whyyy

              alert("eventFired... Sys: " + sys + "; Dia: " + dia + "; Hr: " + hr);


              $scope.bpDataSys = sys;
              $scope.bpDataDia = dia;
              $scope.bpDataHr =  hr;
              $scope.bpDataMap = map;


              $scope.setResultVars(sys, dia, hr, map);


         //alert("************** resultVars SET: " + "sys: " + $scope.bpDataSys + "; dia: " + $scope.bpDataDia + "; hr: " + $scope.bpDataHr);



    })





 console.log("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);

var isAndroid = ionic.Platform.isAndroid();


//returns false on ios...
//alert("Dash ctrl... >> isAndroid:: " + isAndroid);

 //var currentPlatform = ionic.Platform.platform();
 //alert(" ionic.Platform.platform() ==>" +  currentPlatform);


          $scope.testPromises= function()
          {

            var defer = $q.defer();

            defer.promise
                .then(function() {
                  alert("I promised I would show up!");
                })
                .then(function() {
                   alert("me too");
                })
                .then(function(){
                  alert("and I!");
                })

              defer.resolve();

          }







                      $scope.initBPReading = function()
                      {



                          if (isAndroid == true)
                          {

                            btSerialService.connect();


                          }
                          else if (isAndroid == false)
                          {
                            //alert("isAndroid == false :) ... doing  test ble plugin call..");
                            bleService.init();


                          $scope.initX  = function()
                                {

                                alert(">>>>>>>>>>>>starting init ble....");

                                              $cordovaBluetoothLE.initialize({request:true}).then(null,
                                              function(obj) {
                                                //Handle errors
                                                  alert("Initialize Error : " + JSON.stringify(obj));

                                              },
                                              function(obj) {
                                                //Handle successes
                                                alert("Initialize Success : " + JSON.stringify(obj));

                                                  //it doesnt seem to call this method...
                                              //  scanBle();
                                              var bpmXid = "5E9B7376-173F-FE68-264F-38169673DFD8";
                                              $scope.connX(bpmXid);





                                              }
                                            );


                                }


                                $scope.connX = function(bpmUUID)
                                {

                                      //alert("starting connect ble....");
                                        $cordovaBluetoothLE.connect({address: bpmUUID }).then(null, function(obj)
                                        {


                                                  //Handle errors
                                              alert("Connect Error! Message: " + JSON.stringify(obj));


                                                                if (obj.message == "Connection failed" )
                                                                {

                                                                    //showBPData();
                                                                  //console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                  //  this.disconnect();
                                                                 console.log("***** CLOSING BT CONN");
                                                                    closeConn(bpmUUID);
                                                                  //  console.log("***** FINISHED CLOSING BT CONN");

                                                                }







                                              },
                                              function(obj) {
                                                if (obj.status == "connected")
                                                {

                                                  //This works... :) //subscribe next...
                                              alert("connected. obj is: " + JSON.stringify(obj));

                                                  //may need to do a scan first as it gives a "service not found" error
                                                  //do a discover/ services (ios)... first..
                                               alert("call showServices() ");
                                               $scope.servicesX(bpmUUID);

                                                }
                                                else if (obj.status == "connecting") {
                                                  //Device connecting
                                                  console.log("connecting....");

                                                } else {

                                                    console.log("Conn. else... Should trigger on ios. window.localStorage['isAndroid'] is: " + window.localStorage['isAndroid']);



                                                              console.log("disconnected...");
                                                                    //  console.log(" Calling showBPData().... just after disconnected....");
                                                                      //showBPData();
                                                                  //  console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                                                    //  this.disconnect();

                                                           console.log("***** CLOSING BT CONN");
                                                                //closeConn(bpmUUID);
                                                                console.log("***** FINISHED CLOSING BT CONN");

                                                          }






                                                }
                                              ); //end connect.then...




                                    } //end connX

                                    $scope.servicesX = function(bpmUUID)
                                    {

                                      alert("In servicesX");
                                        var params = {address: bpmUUID , serviceUuids: [ ] };
                                        $cordovaBluetoothLE.services(params).then(success, error);



                                                      function success(obj)
                                                      {
                                                    alert("Services Success : " + JSON.stringify(obj));
                                                      //it gets as far as here...

                                                        if (obj.status == "services")
                                                        {
                                                          console.log("obj.status == Services");

                                                        // var serviceUuids = obj.services;
                                                      //  alert("serviceUuids[1]: " + JSON.stringify(servicesUuids[1]));
                                                      alert("obj.services: " + JSON.stringify(obj.services[2]));

                                                         var fff0Service =  obj.services[2];
                                                        alert("fff0Service: " + fff0Service + " ; " );

                                                          //THIS WORKS!!!!! >> getting CN now on bpm :)!! yay!!!
                                                          // data show appear as an console.log when reading is complete
                                                          //WHY DOES IT GET CHS THO?
                                                          //it actually fails to subscribe with this line...
                                                          //subscribeBle(addr, fff0Service,   "fff1");

                                                          //seems to fall over here...
                                                          //skip it for now
                                                         //addService(addr, scope.fff0Service );
                                                         //console.log("svc not added. skipped this for now.");



                                                          for (var i = 0; i < serviceUuids.length; i++)
                                                          {
                                                            $cordovaBluetoothLE.addService(obj.address, serviceUuids[i]);
                                                            console.log("adding svc["+i+"]. serviceUuid:  " + serviceUuids[i]  );
                                                          }



                                                            //now call subscribe >> Noooo >> need to get characteristics first
                                                            //console.log("Getting characteristics now...");
                                                        //var fff0Service = "fff0";
                                                      //  getCharacteristics(fff0Service);
                                                    //  $scope.getCHx(fff0Service, bpmUUID);


                                                          //subscribeBle(bpmUUID, fff0Service, "fff1");




                                                        }
                                                        else
                                                        {
                                                        alert("Unexpected Services Status");
                                                        }
                                                      }

                                                      function error(obj)
                                                      {
                                                        alert("Services Error : " + JSON.stringify(obj));
                                                      }






                                    } //end servicesX


                                              $scope.getCHx = function(svc, bpmUUID)
                                                {
                                                  alert("++ Starting getCHS... ::" + bpmUUID + " . svc: " + svc);

                                                //  var svcXX = "FFF0";

                                                var params = {address: bpmUUID , service: svc}
                                                $cordovaBluetoothLE.characteristics(params).then(success, error);

                                                                function success(obj)
                                                                {
                                                                 alert("Characteristics Success : " + JSON.stringify(obj));

                                                                  if (obj.status == "characteristics")
                                                                  {
                                                                    alert("obj.status == characteristics");

                                                                    var characteristics = obj.characteristics;

                                                                          var fff1ch = 'FFF1';
                                                                          var fff2ch = 'FFF2';

                                                                          alert("About to subscribeBle... >> " + bpmUUID +  svc + fff1ch );
                                                                        //  subscribeBle(bpmUUID, svc, fff1ch);



                                                                  }
                                                                  else
                                                                  {
                                                                    alert("Unexpected Characteristics Status");
                                                                  }
                                                                }

                                                                function error(obj)
                                                                {
                                                                alert("Characteristics Error : " + JSON.stringify(obj));
                                                                }




                                                } //end getCHs


                                    //call init
                                  //  $scope.initX();












                            }
                            else
                            {
                                alert("Platform is NOT Android or iOS. Only iOS and Android are supported.");
                            }


                          } //end initBPReading()




                          $scope.doSynch = function(uid, flag)
                          {

                            /*
                            uid = window.localStorage['loggedIn_USERID'];
                            flag = 0;

                            var rowsToSynch = null;


                              var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                            //  var query = "SELECT rowid, createdAt, rawDataDate, userID, finalSys, finalDia, finalHr, finalMap, bpmID, phoneID FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                              $cordovaSQLite.execute(db, query, [uid, flag ]).then(function(res)
                              {
                                  console.log(" res JSON: " + JSON.stringify(res));
                                   rowsToSynch = res.rows.length;
                                   //console.log("4. rowsToSynch: " + rowsToSynch);

                                      if(res.rows.length > 0)
                                        {

                                        }


                                        for (var s = 0; s < rowsToSynch; s++)
                                        {

                                          //DEOSNT WORK... SENDS same row in 4 times ffs
                                          console.log(s + "Calling   synchAppCHGService.synch()");
                                          synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0);

                                        }



                                 });



                            //console.log("1. rowsToSynch: " + rowsToSynch); //== null (executes first)
                            //console.log("2. >>> end of function::: synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0)");
                            */


                            synchAppCHGService.synch(window.localStorage['loggedIn_USERID'], 0);





                          }








            $scope.cordovaHttp = function()
            {



                              cordovaHTTP.enableSSLPinning(true, function() {
                                        alert('success!');
                                }, function() {
                                alert('error :(');
                                });


                              /*
                                //ALSO REFUSES to work...
                    cordova.exec(
                              useAuth, // A callback function that deals with the JSON object from the CDVPluginResult instance
                              useAuthError, // An error handler
                              'CordovaHTTP', // What class to target messages to (method calls = message in ObjC)
                              'enableSSLPinning', // Which method to call
                              [ 'true' ] // These go in the CDVInvokedUrlCommand instance's.arguments property
                              );

                              */










            }













                                        $scope.closeBTConn = function()
                                        {


                                            //$scope.mlAddress3 = "5E9B7376-173F-FE68-264F-38169673DFD8";
                                              var params = {address: window.localStorage['mlAddress']};
                                            console.log("close: " + JSON.stringify(params));

                                            var iStr1 = "close bt conn: " + JSON.stringify(params);
                                               $scope.addToLog(iStr1, 5);


                                                    $cordovaBluetoothLE.close(params).then(function(obj) {
                                                      console.log("Close Success : " + JSON.stringify(obj));
                                                      }, function(obj) {
                                                        console.log("Close Error : " + JSON.stringify(obj));
                                                      });




                                        }












                         $scope.insertTestData = function(name, year ) {

                        /*    alert("starting insertTestData()");
                                var query = "INSERT INTO test (name, year) VALUES (?,?)";
                                $cordovaSQLite.execute(db, query, [name, year]).then(function(res) {
                                alert("INSERTED test data . The ID is:" + res.insertId);
                                window.localStorage['testDataid'] = res.insertId;
                                 //alert("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );


                                    //  alert("calling postBPData() from inside insert to appdb)");
                                    //  $scope.postBPData();
                                    //  alert("DONE calling postBPData()");



                                }, function (err) {
                                  alert(err);
                                });   */

                                testLocalDBService.insertTestData(name, year);



                            }




                              $scope.updateTestData = function(idd, year) {

                                //do test insert first...
                                //(db, "CREATE TABLE IF NOT EXISTS test (idd integar primary key, name text, year text)")

                            /*    alert("starting updateTestData()...");
                                        //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                        uQuery = "UPDATE test SET year=? WHERE rowid=1";
                                        alert("uQuery == " + uQuery);
                                     $cordovaSQLite.execute(db, uQuery, [year]).then(function(res) {

                                     alert(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                     }, function (err) {
                                       alert("update/synch error" + JSON.stringify(err));
                                     });
                                     */


                                     testLocalDBService.updateTestData(idd, year);




                                 }





                                 $scope.readTestData = function(idd)
                                 {
                                   //alert(1);

                                /*     var query = "SELECT rowid, * FROM test";
                                       $cordovaSQLite.execute(db, query).then(function(res) {
                                           if(res.rows.length > 0) {
                                             //  alert("SELECTED row0 -> " + res.rows.item(0).createdAt + " - " + res.rows.item(0).base64Data + " - " + res.rows.item(0).phoneID);
                                               alert("Now, here's all the data");


                                                               for(var i = 0; i < res.rows.length; i++) {


                                                                // alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; base64Data: " + res.rows.item(i).base64Data + "; hexData: " + res.rows.item(i).hexData + "; Outcome: " + res.rows.item(i).readingOutcome
                                                                //    + "; Outcome Details: " + res.rows.item(i).readingOutcomeDetails );

                                                                    alert( "TEST DATA Row: id: " + res.rows.item(i).rowid + "; Name: " + res.rows.item(i).name + "; Year: " + res.rows.item(i).year );


                                                                  }

                                                            }



                                 });  */


                                 testLocalDBService.readTestData(idd);




                               }




                    $scope.testSynch = function()
                    {

                      var arrx = [];
                      arrx  = SynchAppCHG.checkAppDB(1601);
                      //alert("arrx => " + arrx);
                      alert("  $scope.itemsToBeSynched => " + JSON.stringify( arrx ));


                    }










                      $scope.updateBPR_test = function(value, bprID) {

                        //do test insert first...
                        //(db, "CREATE TABLE IF NOT EXISTS test (idd integar primary key, name text, year text)")





                        alert("starting updateBPR_test()...");
                                //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE bppID = ?";
                                alert("uQuery == " + uQuery);
                             window.sqlitePlugin.execute(db, uQuery, [value, bprID]).then(function(res) {

                             alert(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                             }, function (err) {
                               alert("update/synch error" + JSON.stringify(err));
                             });
                         }













                      //   $scope.updateBPR();





                      //this fucntion reads in ALL of the b64 strings from the app db, converts them to byteArrays and prints out each array element >> which gives us the bp data
                      $scope.readSQLiteBPData = function(uid) {

                        //still no results found
                        /* alert("uid is: " + uid);
                        alert("userIDx: " + uid);
                          var userIDx = parseInt(uid);
                          */


                            //  var query = "SELECT rowid, * FROM bp_reading_raw WHERE userID  = ?";
                            var query = "SELECT rowid, * FROM bp_reading_raw WHERE userID  = ?";
                            window.sqlitePlugin.execute(db, query, [uid]).then(function(res) {
                                  if(res.rows.length > 0) {
                                    //  alert("SELECTED row0 -> " + res.rows.item(0).createdAt + " - " + res.rows.item(0).base64Data + " - " + res.rows.item(0).phoneID);
                                      alert("Now, here are all the readings for userID: " + uid);


                                                      for(var i = 0; i < res.rows.length; i++) {


                                                        alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; base64Data: " + res.rows.item(i).base64Data + "; hexData: " + res.rows.item(i).hexData + "; Outcome: " + res.rows.item(i).readingOutcome
                                                           + "; Outcome Details: " + res.rows.item(i).readingOutcomeDetails );


                                                            if(window.localStorage['isAndroid'] == "false")
                                                            {
                                                                        //only for ble... ie ios
                                                                        var ib64 = res.rows.item(i).base64Data;
                                                                            //convert each b64Value into a byteArray and print each element of each array


                                                                            var bytesZ = $cordovaBluetoothLE.encodedStringToBytes(ib64);
                                                                            console.log("******Starting byteArray.....");
                                                                            for (var j = 0; j < bytesZ.length; j++)
                                                                            {
                                                                              console.log("bytesZ[" + j + "] is: " + bytesZ[j]);

                                                                            }
                                                                            console.log("******Ending byteArray.....");

                                                              }
                                                              else {
                                                              //  alert("not ble");
                                                              }



                                                      } // end for loop






                                  }
                                  else {
                                    alert("No results found");
                                  }
                              }, function (err) {
                                  alert(err);
                              });
                          } //end readSQLiteBPData()




                           $scope.readSQLiteBPProcessedData = function(uid) {



                               db.transaction(function(tx) {
                               tx.executeSql("SELECT rowid, * FROM bp_reading_processed;", [], function(tx, res) {
                               console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                              // console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");

                               console.log("res: " + JSON.stringify(res));

                               if(res.rows.length > 0) {
                            //  alert("#ROW 0 #: Time: " + res.rows.item(0).createdAt + "; bppID " + res.rows.item(0).rowid + "; PhoneID: " + res.rows.item(0).phoneID + "; Avgs::: Sys: " + res.rows.item(0).finalSys);
                              console.log("Now, here are all the processed BP readings : " + JSON.stringify(res));

                                //alert(JSON.stringify(res));

                                                   for(var i = 0; i < res.rows.length; i++)
                                                   {


                                                       console.log(JSON.stringify(res.rows.item(i)));


                                                   }




                               } else {
                                  console.log("No results found");
                               }



                             }); //end tx.executeSql
                         }); //end db.transaction

                       } //end readSQLiteBPProcessedData()





                     $scope.disconnect = function()
                     {

                          var params = {address: window.localStorage['mlAddress']};

                        console.log("Attempting Disconnect to: " + params);

                            $cordovaBluetoothLE.disconnect(params).then(function(obj) {
                          console.log("Disconnect Success : " + JSON.stringify(obj));
                            }, function(obj) {
                            console.log("Disconnect Error : " + JSON.stringify(obj));
                            });





                     }





                        //RD's ng Read Version
                        //call this from a button... once subscribed
                        //$scope.subscribeBle($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        //calls
                      //  $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                      //  $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff2ch);

                        $scope.readRD = function(address, service, characteristic) {
                          var params = {address:address, service:service, characteristic:characteristic, timeout: 2000};

                          console.log("Read : " + JSON.stringify(params));

                          $cordovaBluetoothLE.read(params).then(function(obj) {
                            console.log("Read Success : " + JSON.stringify(obj));

                            var bytes = $cordovaBluetoothLE.encodedStringToBytes(obj.value);
                            //console.log("Read : " + bytes[0]);
                          }, function(obj) {
                            console.log("Read Error : " + JSON.stringify(obj));
                          });
                        };



                        //create the handler functions for the buttons - to call the readRD()

                        $scope.readfff0 = function()
                        {
                              $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        }


                        $scope.readfff1 = function()
                        {
                              $scope.readRD($scope.mlAddress, $scope.fff0Service,  $scope.fff1ch);
                        }









                    $scope.writeBle = function()
                    {

                        var string1 = "MÿQ";
                        var bytes =   $cordovaBluetoothLE.stringToBytes(string1);
                        var encodedString1 =   $cordovaBluetoothLE.bytesToEncodedString(bytes);

                        var newHexString = b64toHex(encodedString1);
                        console.log("New Hex String is: " + newHexString);
                        //console.log("New Hex String is: " + newHexString);

                        var params = {address:$scope.mlAddress, serviceUuid:$scope.fff0Service, characteristicUuid: $scope.fff1ch , value: newHexString};
                        console.log("Writing to : " + JSON.stringify(params));
                        $cordovaBluetoothLE.write(params).then(success, error);



                              function success(obj)
                              {

                                          console.log("Write Success!!!! WOW!!! - obj.value: " + JSON.stringify(obj.value));




                              }

                              function error(obj)
                              {

                                  console.log("write error :(" +  JSON.stringify(obj))


                              }


                    } //end write()







                          function hexStringToByteArrayNew(s) {
                            var len = s.length;
                            var data = [];

                           //WHAT is this line for??
                          //  for(var i=0; i<(len/2); data[i++]=0) {}

                            for (var i = 0; i < len; i += 2) {

                                var value = (parseInt(s.charAt(i), 16) << 4)  + parseInt(s.charAt(i+1), 16);

                                // "transforms" your integer to the value a Java "byte" would have:
                                if(value > 127) {
                                    value -= 256;
                                }

                                data[i / 2] = value;

                            }

                            return data;

                        };








                          $scope.parseHex = function()
                          {

                            var parsedHex = hexStringToByteArrayNew("4d516b003b000077433b3a7589a36f");
                            console.log("parsedHex is: " + parsedHex);

                          }








                    //my read function version
                  $scope.readBle = function(addr)
                    {
                  //trying to read now...
                  console.log("doing read now");
                  var params = {address: addr, serviceUuid: "fff0", characteristicUuid: "fff1"};
                  $cordovaBluetoothLE.read(params).then(success, error);

                            function success(obj)
                            {

                                          console.log("Read Success : " + JSON.stringify(obj));

                                          if (obj.status == "read")
                                          {
                                            /*var bytes = bluetoothle.encodedStringToBytes(obj.value);
                                            console.log("Read : " + bytes[0]);*/

                                            console.log("obj.status == Read");
                                          }
                                          else
                                          {
                                            console.log("Unexpected Read Status");
                                          }


                            }


                            //@1740 ON JAN21: KEEP getting read error: no connection address present
                            //@1743: got rid of ""s around the filed names
                            //@1748: Still get error... >> getCharacteristics works tho so keep trying that ... tack
                            function error(obj)
                            {
                              console.log("Read Error : " + JSON.stringify(obj));

                            }




                }  //end readBle()













// $scope.rating = 5;
//APP rating
 $scope.rating = {};
$scope.rating.numberSelection = 6;

$scope.rating.othernumberSelection = "";




//THe rest of DashCtrl etc from here...............

                    var userEncPassword = "stephen";



                         $scope.signOut = function()
                         {
                           console.log("&&&&&&&& ********* Signing out...");
                           window.localStorage['TOKEN'] = null;
                           window.localStorage['loggedIn_USERID'] = null;
                            window.localStorage['loggedIn_USERNAME'] = null;


                              $scope.user = null;
                              $state.go('signin');

                         }


                        console.log('Welcome to Home / Dashboard');
                        console.log("Token: " + window.localStorage['TOKEN'] );
                        console.log("UserID: " + window.localStorage['loggedIn_USERID'] );


                                  var token = window.localStorage['TOKEN'];







                                        $scope.testEnc = function()
                                        {

                                                //do login api call...
                                                //alert("server::: " + chgServerApi.serverName);
                                                $http({  method: 'GET', url: 'http://www.mikecunneen.com/enc.php?data=abc' }).then(function successCallback(response)
                                                {


                                                console.log("calling enc.php " + JSON.stringify(response));









                                                 }, function errorCallback(response) {
                                                              // called asynchronously if an error occurs
                                                              // or server returns response with an error status.

                                                             alert("enc.php Error. Please Check your Internet Connection");
                                                              //  alert(JSON.stringify(response));

                                                                  //console.log(response.data.message);




                                                            }); //end $http post









                                        }






















$scope.showUser = function()
{



 alert("user: " + window.localStorage["loggedIn_USERID"] + "; -- token: " + window.localStorage["TOKEN"]);

  alert("Phone Make: " + window.localStorage['phoneMake'] + " - phone model: " + window.localStorage['phoneModel'] + " - phone os: " +  window.localStorage['phoneOS'] +  " - phoneUUID: " +   window.localStorage['phoneUUID'] + " - app version: " + window.localStorage['appVersion']);

}



}) //end dash ctrl







.controller('SignInCtrl', function($scope, $state, $http, chgServerApi, networkService, Auth) {
  //window.localStorage["loggedIn_USERID"] = null;
  //window.localStorage['TOKEN'] = null;


  // Set the default value of inputType
  $scope.inputType = 'password';

  // Hide & show password function
  $scope.hideShowPassword = function(){



    if ($scope.inputType == 'password')
    {
      $scope.inputType = 'text';
      //user.password.focus();
    }
    else
    {
      $scope.inputType = 'password';
    //  user.password.focus();
    }



  };



   Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

 $scope.user = {};

//test for now...




                     $scope.signIn = function(user) {




                                  if (user.username == null || user.password == null || user.username == "" || user.password == "")
                                  {

                                  alert("You MUST enter a USERNAME and PASSWORD");

                                  }
                                  else
                                  {

                                  //alert("about to log in... >> server::: " + chgServerApi.serverName);


                                          //   var sUsername = user.username.toLowerCase(); //WONT work ...
                                             var sUsername = user.username
                                             var sPassword = user.password;


                                             //reset the form values
                                             //user.username = null;
                                            // user.password = null;


                                          //  var netConn = networkService.check();
                                            netConn = 1; //for web app testing
                                            if (netConn == 1)
                                            {

                                            //  alert("Internet Connection Present. Logging you in!");



                                                        //do login api call...
                                                        //alert("server::: " + chgServerApi.serverName);
                                                        $http({  method: 'POST', url: chgServerApi.serverName + '/api/login', data: { username: sUsername, password: sPassword } }).then(function successCallback(response)
                                                        {


                                                      //  console.log("*******in http SIGN IN... , resp: " + JSON.stringify(response));


                                                                      if (user.password == response.data.dbPassword)
                                                                      {
                                                                          window.localStorage['TOKEN'] = response.data.token;
                                                                          window.localStorage["loggedIn_USERID"] = response.data.UserID;
                                                                        //  alert("logged in... response.data.UserID: " + response.data.UserID);
                                                                          window.localStorage['loggedIn_USERNAME'] = response.data.Username;

                                                                          console.log("^^^^^Logged In. Token => " + window.localStorage['TOKEN']);
                                                                          console.log("^^^^^Logged In. userID (chgUserID) => " + window.localStorage["loggedIn_USERID"]);
                                                                          console.log("^^^^^Logged In. username (chg username) => " + window.localStorage["loggedIn_USERNAME"]);






                                                                          $state.go('tab.dash');



                                                                      }
                                                                      else if (response.data.loginFailed == true)
                                                                      {

                                                                          alert("Login Error: " + response.data.message);



                                                                      }
                                                                      else {

                                                                        alert("Unable to Log In. Please try again.");

                                                                      }










                                                         }, function errorCallback(response) {
                                                                      // called asynchronously if an error occurs
                                                                      // or server returns response with an error status.

                                                                     alert("Login Error. Please Check your Internet Connection");
                                                                      //  alert(JSON.stringify(response));

                                                                          //console.log(response.data.message);




                                                                    }); //end $http post


                                                  } //end if network conn == 1
                                                  else {
                                                    alert("Your Phone has not Internet Connection so you can't log in. Please check your Internet Connection and try again.");
                                                  }



                                  } //emd else




                     }





})



.controller('BPCtrl', function($scope, $state, $http, chgServerApi, BloodPressure) {




















})//end bpController


//Log / Health Record
.controller('LogCtrl', function($scope, Auth, $http, chgServerApi, networkService, $ionicPopup, synchService) {

//Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

var networkConn = networkService.check();
console.log("network is: " + networkConn);

$scope.readings = [];

$scope.AllLogData = [];
var logData = [];

var bprArray = [];
var prArray = [];




                  function getAllReadings()
                  {

                                  $http({  method: 'POST', url: chgServerApi.serverName + "/api/allReadings", data: {token: window.localStorage['TOKEN'], username: window.localStorage['loggedIn_USERNAME']} }).then(function successCallback(response)
                                  {

                                            //console.log("*******************Inside the $http post... response json: " + JSON.stringify(response));
                                             //console.log("token is: " + token);

                                             bprArray = response.data.bpResults;
                                             prArray = response.data.proteinResults;

                                             processReadings(bprArray, prArray);

                                               //$scope.getLogData(window.localStorage['loggedIn_USERNAME']);



                                    }, function errorCallback(response) {

                                                  console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                  });


                  } //end getAllReadings()


                  function processReadings(bprArray, prArray)
                  {

                    //console.log("processReadings()....");

                  /*  console.log(JSON.stringify(bprArray[0]));
                    console.log(" ");

                    console.log(JSON.stringify(prArray[0]));
                    console.log(" "); */

                    /*
                    bpr fields:
                    {"chgbprid":664,"createdatappraw":"2016-04-19T10:01:52.858Z","createdatapp":"2016-04-19T10:01:52.858Z","createdatchg":"2016-04-19T10:01:53.376Z",
                    "chguserid":20,"systolic":151,"diastolic":94,"heartrate":102,"map":"113.00",
                    "feedback":"No Feedback","outcome":"Successful Reading","monitoring_device_id":9,"phone_meta_data_id":101}

                    pr fields:
                    {"protein_reading_id":1,"chguserid":20,"monitoring_device_id":17,"protein_reading":" Nil","phone_meta_data_id":853,"createdat":"2016-05-10T14:02:04.561Z"}

                    */

                    //just do a collapsable tab on the log tab... >> 1 for BPR and 1 for PRs ... easier...

                  //  console.log("bprArray[0]: " + JSON.stringify(bprArray[0]));

                    for (x=0;x<bprArray.length;x++)
                    {
                      //console.log("initial createdAtApp: " + bprArray[x].createdatapp);

                      var n = bprArray[x].createdatapp;
                      var n2 = n.split("T");

                      var date = n2[0];
                      var time = n2[1];
                      var time2 = time.split(".");
                      var finalTime = time2[0];

                      var finalCreatedAtApp = date + " - " + finalTime;

                    bprArray[x].createdatapp = finalCreatedAtApp;

                      //console.log("final createdAtApp: " + bprArray[x].createdatapp);

                    }


                    for (x=0;x<prArray.length;x++)
                    {
                      //console.log("initial createdat: " + prArray[x].createdat);

                      var n = prArray[x].createdat;
                      var n2 = n.split("T");

                      var date = n2[0];
                      var time = n2[1];
                      var time2 = time.split(".");
                      var finalTime = time2[0];

                      var finalCreatedAtApp = date + " - " + finalTime;

                    prArray[x].createdat = finalCreatedAtApp;

                    //  console.log("final createdat: " + prArray[x].createdat);

                    }


                    $scope.readings = bprArray;

                    $scope.prReadings = prArray;




                  }









//$scope.AllLogData = logDataService.getData(1601);
//alert(JSON.stringify($scope.AllLogData));
//var logData = [];

          //fills up the $scope.logData array
          $scope.getLogData = function(uid)
          {


                            //New Style sqlite - with C >> to replace the other one...
                            db.transaction(function(tx) {


                                  //  alert("My Readings: slqc.my.db SUCCESS... tx: " + JSON.stringify(tx));

                                    var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = 0 ";
                                    //tx.executeSql(query, [1601], function(tx, res)
                                  //  var query = "SELECT * FROM bp_reading_processed;";
                                    tx.executeSql(query, [uid], function(tx, res)
                                    {
                                    //  alert("res.rows.length: " + res.rows.length + " -- should be 1");
                                    //  alert("res.rows.item(13).finalSys: " + res.rows.item(0).finalSys);

                                                            if(res.rows.length > 0)
                                                            {

                                                                //alert("Now, here's all the data");


                                                                                        for(var i = 0; i < res.rows.length; i++)
                                                                                        {


                                                                                        /*  logData.id = res.rows.item(i).rowid;
                                                                                          logData.createdAt = res.rows.item(i).createdAt;
                                                                                          logData.readingType = "Blood Pressure";
                                                                                          logData.systolic = res.rows.item(i).finalSys;
                                                                                          logData.diastolic = res.rows.item(i).finalDia;
                                                                                          logData.hr = res.rows.item(i).finalHr;
                                                                                          logData.map = res.rows.item(i).finalMap; */



                                                                                          //var logItem = res.rows.item(i).createdAt + " - Blood Pressure - " + "<br />" + "<b>Systolic:</b>: " + res.rows.item(i).finalSys + " - <b>Diastolic: 0</b>" + res.rows.item(i).finalDia + " - <b>HR:</b> " + res.rows.item(i).finalHr + " - MAP: " + res.rows.item(i).finalMap;
                                                                                        //  alert("res: " + JSON.stringify(res));
                                                                                            logData[i] = res.rows.item(i);
                                                                                           //logData.push(res.rows.item(i));



                                                                                         //logData.push({id: res.rows.item(i).rowid, createdAt: res.rows.item(i).createdAt, readingType: "Blood Pressure", systolic: res.rows.item(i).finalSys, diastolic: res.rows.item(i).finalDia, hr: res.rows.item(i).finalHr, map: res.rows.item(i).finalMap });

                                                                                        } //end loop

                                                                                        console.log("logData[]: " + JSON.stringify(logData));


                                                                      //  alert("end res.rows.length > 0");

                                                                } //end if res.rows.length >0







                                    }); //end tx.executeSql

                            }, //IF there's a   db.transaction error
                            function(err) {
                              console.log('Log database ERROR: ' + JSON.stringify(err));
                            });






                  //  alert("end of getLogData(). logData[] is: " + JSON.stringify(logData));

            } //end $scope.getLogData()








            $scope.doRefresh = function() {

              //$scope.getLogData(window.localStorage['loggedIn_USERID']);

              //$scope.readings = logData;

              if (networkConn == 1)
              {

                          getAllReadings();


            }
            else {
                          function showAlert() {
                                 var alertPopup = $ionicPopup.alert({
                                   title: 'No Log Data Available',
                                   template: 'There is no Internet Connection so Log Data is Unavailable'
                                 });
                               }
                               showAlert();
            }


              //ends the refresh
              $scope.$broadcast('scroll.refreshComplete');



                }








        //load log when ctrller loads
          $scope.doRefresh();










}) //end log (chats) ctrl

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})
/*
.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };


}) */



.controller('AccountCtrl', function($scope, $state, Auth, synchService, networkService, $ionicPopup, $timeout)
{

  Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );
//console.log(window.localStorage['loggedIn_USERID']);
$scope.accountID = window.localStorage['loggedIn_USERNAME'];
$scope.appVersion = window.localStorage['appVersion'];
$scope.phoneMake = window.localStorage['phoneMake'];
$scope.phoneModel = window.localStorage['phoneModel'];

$scope.phoneOS = window.localStorage['phoneOS'];
$scope.phoneOSVersion = window.localStorage['phoneOSVersion'];


                //sign out of app
                $scope.signOut = function()
                {

                  $scope.showAlert = function() {
                        var alertPopup = $ionicPopup.alert({
                          title: 'Are you sure you want to Log Out?',
                          template: '',
                          buttons: [
                                   { text: 'Cancel' },
                                   {
                                     text: '<b>Log Out</b>',
                                     type: 'button-positive',
                                     onTap: function(e) {
                                       // add your action


                                                         console.log("&&&&&&&& ********* Signing out...");
                                                         window.localStorage['TOKEN'] = null;
                                                         window.localStorage['loggedIn_USERID'] = null;
                                                          window.localStorage['loggedIn_USERNAME'] = null;


                                                            $scope.user = null;
                                                            $state.go('signin');


                                     }
                                   }
                                 ]
                        });
                      }
                      $scope.showAlert();





                }


                $scope.synchBP = function()
                {

                  var networkConn = networkService.check();
                  console.log("networkConn == " + networkConn);

                            if (networkConn == 1)
                            {


                                                  ///THERE ARE ABOUT 40S BETWEEN EACH SYNCH WHICH minimizes the possiblity of overload etc...
                                                  $timeout(function ()
                                                  {
                                                      var d = new Date();
                                                      var time = d.toLocaleTimeString();
                                                      console.log("** synchBP:: Time Now is: " + time);

                                                      synchService.synchBP();
                                                  }, 2000); //run this after 2s


                                                  $timeout(function ()
                                                  {
                                                      var d = new Date();
                                                      var time = d.toLocaleTimeString();
                                                      console.log("** synchPR:: Time Now is: " + time);

                                                      synchService.synchProteinReadings();
                                                  }, 40000); //run this after 2s

                                                  $timeout(function ()
                                                  {
                                                      var d = new Date();
                                                      var time = d.toLocaleTimeString();
                                                      console.log("** synchDE:: Time Now is: " + time);

                                                      synchService.synchDeviceErrors();
                                                  }, 80000); //run this after 2s







                             }
                              else
                              {

                                          $scope.showAlert = function() {
                                                var alertPopup = $ionicPopup.alert({
                                                  title: 'No Internet Connection',
                                                  template: 'Cant Synch Data as No Internet Connection Available.'
                                                });
                                              }
                                              $scope.showAlert();

                                  }

                } //end synchBP














})


.controller('HelpCtrl', function($scope, Auth)
{

  Auth.checkSession(window.localStorage["loggedIn_USERID"], window.localStorage["TOKEN"] );

//console.log(window.localStorage['loggedIn_USERID']);


//$scope.accountID = window.localStorage['loggedIn_USERID'];
//alert("$scope.accountType: " + $scope.accountType);

//


})



.controller('btSerialTestCtrl', function($base64, $scope, $cordovaBluetoothSerial, $cordovaBLE, $cordovaBluetoothLE) {





}) // end bt serial ctrl
