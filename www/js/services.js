angular.module('starter.services', ['ngResource', 'ngCordovaBluetoothLE'])


//global url for the CHG API...
// 1. Inject dep. into controller + 2. chgServerApi.serName

.factory('chgServerApi', function() {
  return {
      //SET SERVER / HOST NAME

        //serverName : 'http://leanbh.stephenlane.me:8080' //MySQL Server -- chgServer2.js
      //localhost for testing
      //pgSql server (chgServer5.js) running on :443
      //serverName : 'https://leanbh.stephenlane.me'
      //serverName : 'http://leanbh.stephenlane.me:3128'
    serverName: 'https://app.infantcentre.net'
      //serverName: 'https://app.infantcentre.net'

      //serverName: 'http://localhost:3128'


  };
})


//Inject this into each controller and call the checkSession(userID, token) at top of each contorller...
//NEEDS MORE TESTING TO ENSURE IT'S Robust!

.factory('Auth', function($state) {
  return {

        checkSession:function(userID, Token)
        {

                  //print loggedin user to the console... for testing..
                  console.log("AUTH.checkSession:: Logged In UserID: " + window.localStorage["loggedIn_USERID"] );
                  console.log("AUTH.checkSession:: Token is: " + window.localStorage["TOKEN"] );

                //null needs to be in ""s as wls converts it to string... (I think)!
                //  if (window.localStorage["loggedIn_USERID"] == "null")
                  if (window.localStorage["TOKEN"] == undefined || window.localStorage["loggedIn_USERID"] == undefined || window.localStorage["loggedIn_USERID"] == null || window.localStorage["TOKEN"] == null || window.localStorage["loggedIn_USERID"] == "null" || window.localStorage["TOKEN"] == "null" || window.localStorage["TOKEN"] == "" || window.localStorage["loggedIn_USERID"] == "" || window.localStorage["TOKEN"] == "undefined" || window.localStorage["loggedIn_USERID"] == "undefined")
                  {
                  console.log("User Not Logged In... >> force login...");
                  $state.go('signin');
                  //window.localStorage["loggedIn_USERID"] = 1601;
                  }
                  else {
                    console.log("logged in! window.localStorage[loggedIn_USERID]: " + window.localStorage["loggedIn_USERID"]);
                  }


        }



  }
})


.factory('encDcrypt', function(){

  //AES passphrase
  //Need to make this more secure
  //but it will do for now
var gPass = "stephen";


    return {

               enc:function(encData)
               {

                  console.log("encrypting: " + encData);

                  //var iv = "1234567891234567";

                  var encrypted = CryptoJS.AES.encrypt(encData, gPass);
      						//console.log("ENC = " + encrypted);
                  //console.log("ENC Plain Text = " + encrypted.toString());

                  return encrypted.toString();

                  //console.log(Passwd to Stringencrypted.toString);

      							//this.decrypt(encrypted);


               },

                decrypt:function(decryptData)
                {
                   //console.log("decrypting: " + decryptData);

                  //   var message = CryptoJS.enc.Hex.parse('00112233445566778899aabbccddeeff');


                //     console.log("msg: " + message);

              //    var iv = "2f568a7c28b6ec82a0bc3c407cace11a"

                  	var decrypted = CryptoJS.AES.decrypt(decryptData, gPass);

                    //console.log("DECRYPTED= " + decrypted);

                    var plaintext = decrypted.toString(CryptoJS.enc.Utf8);

                    //console.log("Plain Text DECRYPTED= " + plaintext);

                      return plaintext;



                },
                      //CryptoJS again...

                          stringifyParams: function (cipherParams) {
                              var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
                              if (cipherParams.iv) j.iv = cipherParams.iv.toString();
                              if (cipherParams.salt) j.s = cipherParams.salt.toString();
                              return JSON.stringify(j);
                          },
                          parseParams: function (jsonStr) {
                              var j = JSON.parse(jsonStr);
                              var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
                              if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
                              if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
                              return cipherParams;
                          }









           } //end return





}) //encDcrypt factory/service


//encryption service using Forge and Not CryptoJS
.service("CipherService", function() {

    /*
     * Encrypt a message with a passphrase or password
     *
     * @param    string message
     * @param    string password
     * @return   object
     */

    this.encrypt = function(message, password) {
        var salt = forge.random.getBytesSync(256);
        var key = forge.pkcs5.pbkdf2(password, salt, 40, 16 );
        var iv = forge.random.getBytesSync(16);
        var cipher = forge.cipher.createCipher('AES-CBC', key);
        cipher.start({iv: iv});
        cipher.update(forge.util.createBuffer(message));
        cipher.finish();
        var cipherText = forge.util.encode64(cipher.output.getBytes());
        return {cipher_text: cipherText, salt: forge.util.encode64(salt), iv: forge.util.encode64(iv)};
    }

    /*
     * Decrypt cipher text using a password or passphrase and a corresponding salt and iv
     *
     * @param    string (Base64) cipherText
     * @param    string password
     * @param    string (Base64) salt
     * @param    string (Base64) iv
     * @return   string
     */

     this.decrypt = function(cipherText, password, salt, iv) {
       //convert salt and iv to b64

        var key = forge.pkcs5.pbkdf2(password, forge.util.decode64(salt), 40, 16);

         console.log("******this.decrypt:: " + cipherText + " ; " + "password: " + password + "; salt: " + salt +  "iv: " + iv + "; key: " + key);
      //  var key = password;
        var decipher = forge.cipher.createDecipher('AES-CBC', key);
        decipher.start({iv: forge.util.decode64(iv)});
        decipher.update(forge.util.createBuffer(forge.util.decode64(cipherText)));
        decipher.finish();
        return decipher.output.toString();
    }










      this.ed = function()
      {



              //  var key = forge.random.getBytesSync(16);
                var iv = forge.random.getBytesSync(16);

                //alternatively, generate a password-based 16-byte key
                var salt = forge.random.getBytesSync(128);
                var key = forge.pkcs5.pbkdf2('password', salt, 8, 16);


                // encrypt some bytes using CBC mode
                // (other modes include: ECB, CFB, OFB, CTR, and GCM)

                var cipher = forge.cipher.createCipher('AES-CBC', key);
                cipher.start({iv: iv});
                cipher.update(forge.util.createBuffer("text to encrypt"));
                cipher.finish();
                var encrypted = cipher.output;
                // outputs encrypted hex
                console.log("***forge AES-CBC: (enc txt) " + encrypted.toHex());

                // decrypt some bytes using CBC mode
                // (other modes include: CFB, OFB, CTR, and GCM)
                var decipher = forge.cipher.createDecipher('AES-CBC', key);
                decipher.start({iv: iv});
                decipher.update(encrypted);
                decipher.finish();
                // outputs decrypted hex
                console.log("*** forge AES-CBC: (dec txt) " + decipher.output.toHex());





        }








/*
      this.decrypt = function(cipherText, password, iv) {
        var key = password;
        var decipher = forge.cipher.createDecipher('AES-CBC', key);
        decipher.start({iv: forge.util.decode64(iv)});
        decipher.update(forge.util.createBuffer(forge.util.decode64(cipherText)));
        decipher.finish();
        return decipher.output.toString();
    }
    */

/*
function decrypting($encdata){
$key = "stephen";
$parts1 = substr(base64_decode($encdata), 0, 16);
$parts2 = substr(base64_decode($encdata), 16, strlen($encdata));
//$decrypted = openssl_decrypt($parts[0], AES_256_CBC, $key, 0, $parts[1]);
$decrypted = openssl_decrypt($parts2, AES_256_CBC, $key, 0, $parts1);
return $decrypted;
}
*/


})




.service("networkService", function()
{

//returns 1 or 0
//networkService.check();
        this.check = function()
        {

          //alert("checking netw conn...")
                  var networkState = navigator.network.connection.type;

                  var states = {};
                  states[Connection.UNKNOWN]  = 'Unknown connection';
                  states[Connection.ETHERNET] = 'Ethernet connection';
                  states[Connection.WIFI]     = 'WiFi connection';
                  states[Connection.CELL_2G]  = 'Cell 2G connection';
                  states[Connection.CELL_3G]  = 'Cell 3G connection';
                  states[Connection.CELL_4G]  = 'Cell 4G connection';
                  states[Connection.NONE]     = 'No network connection';


                  if(navigator.connection.type == Connection.NONE)
                  {
                      //alert("No Connection... : " + states[networkState])
                      return 0;
                  }
                  else {
                    //alert('*** Connection type is: ' + states[networkState]);
                    //1 means there is a network conn
                    return 1;
                  }

        }


}) //end networkService

/*
.service("checkLocalData", function(networkService)
{


                    this.check = function()
                    {





                                  var localPRDataArray = [];
                                  var numReadingsArray = [];


                                  var uQuery = "SELECT rowid, * FROM protein_reading WHERE userID = ? AND chgSynched = ?";
                                  var uQueryBP = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                                  var uQueryErr = "SELECT rowid, * FROM device_error WHERE userID = ? AND chgSynched = ?";


                                                              db.transaction(function(tx) {

                                                              //check protein_readings
                                                              tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                              {

                                                                    window.localStorage['localPRReadings'] = res.rows.length;
                                                                    console.log("There are: " + res.rows.length + " Protein Readings to synch!");

                                                                            numReadingsArray.push(res.rows.length);
                                                                                  if(res.rows.length > 0)
                                                                                  {



                                                                                    } //if res.rows > ...
                                                                                    else {
                                                                                      console.log("No Protein Readings in AppDB to Synch.");
                                                                                    }




                                                                }); //end first select

                                                                    //check bpReadings
                                                                    tx.executeSql(uQueryBP, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                                    {

                                                                      console.log("loggedIn_USERNAME'");
                                                                          window.localStorage['localBPReadings'] = res.rows.length;
                                                                          console.log("BPrs to synch: " + window.localStorage['localBPReadings']);

                                                                                numReadingsArray.push(res.rows.length);
                                                                                        if(res.rows.length > 0)
                                                                                        {


                                                                                          } //if res.rows > ...
                                                                                          else {
                                                                                            console.log("No BP Readings in AppDB to Synch.");
                                                                                          }




                                                                      });//end 2nd select

                                                                  //check errors
                                                                  tx.executeSql(uQueryErr, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                                  {


                                                                        //$scope.localData.prReadings = res.rows.length;
                                                                        window.localStorage['localErrors'] = res.rows.length;
                                                                        console.log("Errors to synch: " + window.localStorage['localErrors']);

                                                                                  numReadingsArray.push(res.rows.length);

                                                                                  //now return the array... (1st element: PR rdgs, 2nd: BPrs, 3rd: device errors )
                                                                                  console.log("** numReadingsArray[]: " + numReadingsArray);
                                                                                  //return numReadingsArray;
                                                                                  //returnNums(numReadingsArray);


                                                                                      if(res.rows.length > 0)
                                                                                      {



                                                                                        } //if res.rows > ...
                                                                                        else {
                                                                                          console.log("No ERRORS in AppDB to Synch.");
                                                                                        }




                                                                    });//end 2nd select






                                                        },
                                                         function(err) {
                                                          console.log('app db ERROR on checking localDB for unsynched Protein, BP Readings of Device Errors: ' + JSON.stringify(err));
                                                        }) //end transaction








                    } //end this.check()






}) //end checkLocalData service
*/


.service("synchService", function(chgServerApi, $http, networkService)
{

            this.synchBP = function()
            {


              console.log("start of synchService.synchBP()  ...");

              synchBPData();

                            function synchBPData()
                            {

                              var synchDataArray = [];

                              var uQuery = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                                                    db.transaction(function(tx) {
                                                    tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                    {
                                                      //test lines to see if query is working...
                                                      console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                                    //  console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                                        if(res.rows.length > 0)
                                                                        {

                                                                                        //test
                                                                                        //token for mollymattsson1@gmail.com / chguserid: 26
                                                                                        //window.localStorage['TOKEN'] = null;

                                                                                       synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                                          for(var i = 0; i < res.rows.length; i++)
                                                                                          {

                                                                                            console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                                             "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys);

                                                                                             //push each row into the arrayF$httpF
                                                                                             synchDataArray.push(res.rows.item(i));

                                                                                            } //end loop here


                                                                                            console.log("about to call postBPDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                                            postBPDataArray(synchDataArray);



                                                                           console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                                           console.log("synchDataArray.length: " + synchDataArray.length);
                                                                           console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                                           console.log("synchDataArray[0].sys1: " + synchDataArray[0].sys1 + " ; synchDataArray[0].dia1: " + synchDataArray[0].dia1  + " ; synchDataArray[0].hr1: " + synchDataArray[0].hr1 );
                                                                           console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) );





                                                                          } //if res.rows > ...
                                                                          else {
                                                                            console.log("No BP Readings in AppDB to Synch.");
                                                                          }







                                                      }); //end outer tx >> insert into

                                                    },
                                                     function(err) {
                                                      console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                                    })






                            } //end synchBPData()



                            function postBPDataArray(synchDataArray )
                            {
                                  console.log("starting postBPDataArray... array: " + JSON.stringify(synchDataArray));




                                            //stuff that will always stay the same...
                                             var token = window.localStorage["TOKEN"];
                                             var _iOutcome = "";
                                             var _iFeedback = "";
                                             var iBPMID = 777;

                                             $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReadings", data: synchDataArray }).then(function successCallback(response)
                                              {

                                                         console.log("*******************Inside the SYNCH data array $http post... response json: " + JSON.stringify(response));
                                                         //console.log("token is: " + token);

                                                              var synchedFlag = 0;
                                                              synchedFlag = parseInt(response.data.synched);
                                                              console.log("synchedFlag == " + synchedFlag + " ; chgbprid: " + response.data.chgbprid );


                                                           if (synchedFlag == 1)
                                                           {
                                                                            //var zbprid = parseInt(window.localStorage['bprid']);
                                                                          //  var zbprid = appBPRID;

                                                                             function updateBPR()
                                                                             {

                                                                                            db.transaction(function(tx) {
                                                                                             var uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE userID = ?";
                                                                                             tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                       console.log("UPDATED bpReadingProcessed. The rowid is:" + res.rows.id);
                                                                                                       console.log("******* bp_reading_processed Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                        deleteBPR();
                                                                                                        //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                                }); //end outer tx >> insert into

                                                                                              },
                                                                                               function(err) {
                                                                                                console.log('app db ERROR on UPDATING (Synching) BP Reading: ' + JSON.stringify(err));
                                                                                              })


                                                                                } //end updateBPR


                                                                               updateBPR();


                                                                               //now do DELETE
                                                                               function deleteBPR()
                                                                               {

                                                                                 console.log("****  start of deletePR()....");

                                                                                              db.transaction(function(tx) {

                                                                                               var uQuery = "DELETE FROM bp_reading_processed WHERE chgSynched=1 AND userID = ?";
                                                                                               tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                         console.log("**DELETED  bp reading(s). The rowid is:" + res.rows.id);
                                                                                                         console.log("******* bp Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));


                                                                                                         window.localStorage['localBPReadings'] = 0;


                                                                                                    //    this.synchProteinReadings();

                                                                                                  }); //end outer tx >> insert into

                                                                                                },
                                                                                                 function(err) {
                                                                                                  console.log('app db ERROR on  DELETING (Synching) bp Reading: ' + JSON.stringify(err));
                                                                                                })


                                                                                  } //end deleteBPR


                                                                  } //end if synchedFlag ==1




                                                }, function errorCallback(response) {

                                                              console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                                           });

                                } //end postBPDataArray

                        } //end this.synchBP


                                this.synchProteinReadings = function()
                                {
                                  console.log("Synching Protein...");


                                  synchProteinData();

                                                function synchProteinData()
                                                {

                                                  var synchDataArray = [];

                                                  var uQuery = "SELECT rowid, * FROM protein_reading WHERE userID = ? AND chgSynched = ?";
                                                                        db.transaction(function(tx) {
                                                                        tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                                        {
                                                                          //test lines to see if query is working...
                                                                          console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                                                        //  console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                                                            if(res.rows.length > 0)
                                                                                            {

                                                                                                            //test
                                                                                                            //token for mollymattsson1@gmail.com / chguserid: 26
                                                                                                            //window.localStorage['TOKEN'] = null;

                                                                                                           synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                                                              for(var i = 0; i < res.rows.length; i++)
                                                                                                              {

                                                                                                                console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                                                                 "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys);

                                                                                                                 //push each row into the arrayF$httpF
                                                                                                                 synchDataArray.push(res.rows.item(i));

                                                                                                                } //end loop here


                                                                                                                console.log("about to call postBPDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                                                                postProteinDataArray(synchDataArray);



                                                                                               console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                                                               console.log("synchDataArray.length: " + synchDataArray.length);
                                                                                               console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                                                               console.log("synchDataArray[0].createdAt: " + synchDataArray[0].createdAt + " ; synchDataArray[0].proteinReading: " + synchDataArray[0].proteinReading  + " ; synchDataArray[0].userID: " + synchDataArray[0].userID );
                                                                                               console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) );





                                                                                              } //if res.rows > ...
                                                                                              else {
                                                                                                console.log("No Protein Readings in AppDB to Synch.");
                                                                                              }







                                                                          }); //end outer tx >> insert into

                                                                        },
                                                                         function(err) {
                                                                          console.log('app db ERROR on UPDATING (Synching) Protein Reading: ' + JSON.stringify(err));
                                                                        })






                                                } //end synchProteinData()



                                                function postProteinDataArray(synchDataArray )
                                                {
                                                      console.log("starting postProteinDataArray... array: " + JSON.stringify(synchDataArray));




                                                                //stuff that will always stay the same...
                                                                 var token = window.localStorage["TOKEN"];


                                                                 $http({  method: 'POST', url: chgServerApi.serverName + "/api/createProteinReadings", data: synchDataArray }).then(function successCallback(response)
                                                                  {

                                                                             console.log("*******************Inside the Protein Readings SYNCH data array $http post... response json: " + JSON.stringify(response));
                                                                             //console.log("token is: " + token);

                                                                                  var synchedFlag = 0;
                                                                                  synchedFlag = parseInt(response.data.synched);
                                                                                  console.log("synchedFlag == " + synchedFlag );


                                                                               if (synchedFlag == 1)
                                                                               {
                                                                                                //var zbprid = parseInt(window.localStorage['bprid']);
                                                                                              //  var zbprid = appBPRID;

                                                                                                 function updatePR()
                                                                                                 {

                                                                                                                db.transaction(function(tx) {
                                                                                                                 var uQuery = "UPDATE protein_reading SET chgSynched = ? WHERE userID = ?";
                                                                                                                 tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                                           console.log("UPDATED protein_reading. The rowid is:" + res.rows.id);
                                                                                                                           console.log("******* protein_reading Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                                           deletePR();


                                                                                                                    }); //end outer tx >> insert into

                                                                                                                  },
                                                                                                                   function(err) {
                                                                                                                    console.log('app db ERROR on UPDATING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                                                  })


                                                                                                    } //end updatePR

                                                                                                   updatePR();


                                                                                                   //now do DELETE
                                                                                                   function deletePR()
                                                                                                   {

                                                                                                     console.log("****  start of deletePR()....");

                                                                                                                  db.transaction(function(tx) {

                                                                                                                   var uQuery = "DELETE FROM protein_reading WHERE chgSynched=1 AND userID = ?";
                                                                                                                   tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                                             console.log("**DELETED  protein_reading(s). The rowid is:" + res.rows.id);
                                                                                                                             console.log("******* protein_reading Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));



                                                                                                                             window.localStorage['localPRReadings'] = 0;

                                                                                                                              //this.synchDeviceErrors();

                                                                                                                      }); //end outer tx >> insert into

                                                                                                                    },
                                                                                                                     function(err) {
                                                                                                                      console.log('app db ERROR on  DELETING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                                                    })


                                                                                                      } //end deletePR





                                                                                      } //end if synchedFlag ==1




                                                                    }, function errorCallback(response) {

                                                                                  console.log("Error Sending Protein Reading Data to CHG: " + JSON.stringify(response));

                                                                               });

                                                    } //end postProteinDataArray









                                } //end this.synchProteinReadings




                                this.synchDeviceErrors = function()
                                {
                                    console.log("Synching Device Errors...");


                                    synchDeviceErrors();

                                                  function synchDeviceErrors()
                                                  {

                                                    var synchDataArray = [];

                                                    var uQuery = "SELECT rowid, * FROM device_error WHERE userID = ? AND chgSynched = ?";
                                                                          db.transaction(function(tx) {
                                                                          tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME'], 0], function(tx, res)
                                                                          {
                                                                            //test lines to see if query is working...
                                                                            console.log("res.rows.length: " + res.rows.length + " -- should be 1");
                                                                          //  console.log("res.rows.item(0).cnt: " + res.rows.item(0).cnt + " -- should be 1");



                                                                                              if(res.rows.length > 0)
                                                                                              {

                                                                                                             synchDataArray.push({"token": window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME'] });

                                                                                                                for(var i = 0; i < res.rows.length; i++)
                                                                                                                {
                                                                                                                /*  console.log( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid +
                                                                                                                   "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt + "sys: " + res.rows.item(i).finalSys); */

                                                                                                                   //push each row into the arrayF$httpF
                                                                                                                   synchDataArray.push(res.rows.item(i));

                                                                                                                  } //end loop here


                                                                                                                  console.log("about to call postErrorDataArray() array: " +  JSON.stringify(synchDataArray));



                                                                                                                  postDeviceErrorsArray(synchDataArray);



                                                                                                 console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                                                                 console.log("synchDataArray.length: " + synchDataArray.length);

                                                                                              /*   console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                                                                 console.log("synchDataArray[0].createdAt: " + synchDataArray[0].createdAt + " ; synchDataArray[0].proteinReading: " + synchDataArray[0].proteinReading  + " ; synchDataArray[0].userID: " + synchDataArray[0].userID );
                                                                                                 console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) ); */





                                                                                                } //if res.rows > ...
                                                                                                else {
                                                                                                  console.log("No Device Errors in AppDB to Synch.");
                                                                                                }







                                                                            }); //end outer tx >> insert into

                                                                          },
                                                                           function(err) {
                                                                            console.log('app db ERROR on UPDATING (Synching) Device Error(s): ' + JSON.stringify(err));
                                                                          })






                                                  } //end synchDeviceErrors()



                                                  function postDeviceErrorsArray(synchDataArray )
                                                  {
                                                        console.log("starting postDeviceErrorsArray... array: " + JSON.stringify(synchDataArray));




                                                                  //stuff that will always stay the same...
                                                                   var token = window.localStorage["TOKEN"];


                                                                   $http({  method: 'POST', url: chgServerApi.serverName + "/api/createDeviceErrors", data: synchDataArray }).then(function successCallback(response)
                                                                    {

                                                                               console.log("*******************Inside the Protein Readings SYNCH data array $http post... response json: " + JSON.stringify(response));
                                                                               //console.log("token is: " + token);

                                                                                    var synchedFlag = 0;
                                                                                    synchedFlag = parseInt(response.data.synched);
                                                                                    console.log("synchedFlag == " + synchedFlag );


                                                                                 if (synchedFlag == 1)
                                                                                 {
                                                                                                  //var zbprid = parseInt(window.localStorage['bprid']);
                                                                                                //  var zbprid = appBPRID;

                                                                                                   function updateDE()
                                                                                                   {

                                                                                                                  db.transaction(function(tx) {
                                                                                                                   var uQuery = "UPDATE device_error SET chgSynched = ? WHERE userID = ?";
                                                                                                                   tx.executeSql(uQuery, [1, window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                                             console.log("UPDATED device_error. The rowid is:" + res.rows.id);
                                                                                                                             console.log("******* device_error Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                                             deleteDE();

                                                                                                                              //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later


                                                                                                                      }); //end outer tx >> insert into

                                                                                                                    },
                                                                                                                     function(err) {
                                                                                                                      console.log('app db ERROR on UPDATING (Synching) protein Reading: ' + JSON.stringify(err));
                                                                                                                    })


                                                                                                      } //end updatePR

                                                                                                     updateDE();


                                                                                                     //now do DELETE
                                                                                                     function deleteDE()
                                                                                                     {

                                                                                                       console.log("****  start of deleteDE()....");

                                                                                                                    db.transaction(function(tx) {

                                                                                                                     var uQuery = "DELETE FROM device_error WHERE chgSynched=1 AND userID = ?";
                                                                                                                     tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                                               console.log("**DELETED  device_error(s). The rowid is:" + res.rows.id);
                                                                                                                               console.log("******* device_error Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));


                                                                                                                               window.localStorage['localErrors'] = 0;

                                                                                                                        }); //end outer tx >> insert into

                                                                                                                      },
                                                                                                                       function(err) {
                                                                                                                        console.log('app db ERROR on  DELETING (Synching) device error: ' + JSON.stringify(err));
                                                                                                                      })


                                                                                                        } //end deletePR





                                                                                        } //end if synchedFlag ==1




                                                                      }, function errorCallback(response) {

                                                                                    console.log("Error Sending Device Error Data to CHG: " + JSON.stringify(response));

                                                                                 });

                                                      } //end postDeviceErrorsArray





                                } //end this.synchDeviceErrors











}) //end synchService







.service("storeDeviceError", function($state, $http, networkService, chgServerApi)
{


                      //Make this a generic error function... for both the Device and BPM errors

                      this.store = function(errorName, errorType, errorDesc)
                      {

                        console.log("Start of this.store()...");


                      var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
                      var utcDate = (new Date(Date.now() - tzoffset)).toISOString().slice(0,-1);


                      var userID =  window.localStorage['loggedIn_USERNAME'];
                      var phoneID = window.localStorage['phoneUUID'];
                      window.localStorage['rawDataDate'] = utcDate;

                      if ( window.localStorage['totalSymptomCount'] == 0)
                      {
                        window.localStorage['noSymptoms'] = true;
                      }
                      else {
                        window.localStorage['noSymptoms'] = false;
                      }


                      console.log("------ totalSymptomCount is: " + window.localStorage['totalSymptomCount']);
                      var symptomCount = window.localStorage['totalSymptomCount'];

                      console.log("***** symptomCount: " + symptomCount);


                      //HANDLE the medical alerts...
                      var medicalAlertPresent = null;
                      var medicalAlertName = null;
                      var medicalAlertDesc = null;
                      var medicalAlertOther = null;




                                        if (errorType == "Urine Analyser Error")
                                        {

                                                   //Basic Medical alert for now... ie >1 symptom warning screen OR regDevicelr result screen
                                                  if (symptomCount > 1)
                                                  {
                                                     //$state.go('tab.proteinWarningMoreThanOneSymptom');
                                                      $state.go('tab.dash');
                                                     //this handleMedicalAlert() function takes name, timestamp, whether the reading (assoiated to it) was synched or not,
                                                    // the localBPR id and the inserted BPR id if there is one
                                                  //   handleMedicalAlert("> 1 Symptom & NORMAL BP", 0, dateTime, chgSynched, window.localStorage['bprid'], window.localStorage['insertedCHGBPRID'], finalSys, finalDia, finalHr, symptomCount );
                                                  medicalAlertPresent = true;
                                                  medicalAlertName = " >1 Symptom & Protein Reading - Device Error";
                                                  medicalAlertDesc = "There is more than 1 (of 3) Preganancy Symptoms associated with this Protein Reading - which Resulted in Urine Analser Error. The User is instructed to call the Leanbh Clinician.";

                                                  }
                                                  else
                                                  {
                                                    //NORMAL RESULT... THIS has already been stored locally and sent to chg
                                                     $state.go('tab.dash');

                                                     medicalAlertPresent = false;
                                                     medicalAlertName = "Protein Reading with No Symptoms - Urine AnalyserError";
                                                     medicalAlertDesc = "Protein Reading is - Urine Analyser Error: " + errorName;

                                                  }


                                          }
                                        /*  else if (errorType == "BP Monitor Error")
                                          {



                                          } */






                                                  /*
                                        tx.executeSql("CREATE TABLE IF NOT EXISTS device_error (errorID integar primary key, createdAt text, userID text, chgSynched integar, errorType text, errorName text, errorDesc text, headachesSymptom text, visDSymptom text, epiGSymptom text, noSymptoms text, phoneUUID text, phoneMake text, phoneModel text, phoneOS text, appVersion text, medicalAlertPresent text, medicalAlertName text, medicalAlertDesc text, medicalAlertOther text )  ");
                                          */


                                    function insertDeviceErrorData(dateTime, uid, chgSynched, errorType, errorName, errorDesc,
                                                                  headachesSymptom, visDSymptom, epiGSymptom, noSymptoms,
                                                                  phoneUUID, phoneMake, phoneModel, phoneOS, appVersion,
                                                                  medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther)
                                    {

                                      console.log("*** Inserting Urine Data. medicalAlertPresent: " + medicalAlertPresent + " ; medicalAlertName: " + medicalAlertName  + " ; medicalAlertDesc: " + medicalAlertDesc + " ; medicalAlertOther: " + medicalAlertOther);



                                             db.transaction(function(tx) {
                                                      var query = "INSERT INTO device_error (createdAt, userID, chgSynched, errorType, errorName, errorDesc, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                      tx.executeSql(query, [dateTime, uid, chgSynched, errorType, errorName, errorDesc, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms, phoneUUID, phoneMake, phoneModel, phoneOS, appVersion, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther ], function(tx, res) {

                                                                  console.log("INSERTED Device Error. The ID is:" + res.insertId);
                                                                   window.localStorage['deviceErrorID'] = res.insertId;

                                                         }); //end outer tx >> insert into

                                             },
                                              function(err) {
                                               console.log('app db ERROR on Inserting Device Error: ' + JSON.stringify(err));
                                             })

                                                          console.log("just checking network...");
                                                           //Check to see that we have network conn.
                                                             var networkConn = networkService.check();

                                                             console.log("networkConn == " + networkConn);

                                                               if (networkConn == 1)
                                                               {
                                                                console.log("**Insert to AppDB Done! Netw. Conn Present so calling postDeviceError()**");
                                                                postDeviceError(errorType, errorName, errorDesc, utcDate, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther);


                                                               }
                                                               else {
                                                                console.log("«««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");


                                                               }

                                       } //end insertProcessedBPData()



                                               var initChgSynched = 0;

                                               insertDeviceErrorData(utcDate, window.localStorage['loggedIn_USERNAME'], initChgSynched, errorType, errorName, errorDesc , window.localStorage['headachesSymptom'], window.localStorage['visDSymptom'], window.localStorage['epiGSymptom'], window.localStorage['noSymptoms'],
                                               window.localStorage['phoneUUID'], window.localStorage['phoneMake'], window.localStorage['phoneModel'], window.localStorage['phoneOS'], window.localStorage['appVersion'],
                                                medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther);


                                               console.log("insertDeviceError() DONE!. Now Sending to CHG.....");



                      } //end of this.store








                      function postDeviceError(errorType, errorName, errorDesc, utcDate, medicalAlertPresent, medicalAlertName, medicalAlertDesc, medicalAlertOther )
                      {


                                     var token = window.localStorage["TOKEN"];



                                       //SEND in array vs all vars...
                                      var deviceErrorArray = [];
                                      deviceErrorArray[0] = {"token":window.localStorage['TOKEN'], "username":window.localStorage['loggedIn_USERNAME']};

                                      deviceErrorArray[1] = {"createdAt": utcDate, "username": window.localStorage['loggedIn_USERNAME'], "errorType":errorType, "errorName": errorName, "errorDesc": errorDesc,
                                                     "headachesSymptom": window.localStorage['headachesSymptom'], "visDSymptom": window.localStorage['visDSymptom'], "epiGSymptom": window.localStorage['epiGSymptom'], "noSymptoms": window.localStorage['noSymptoms'],
                                                      "phoneUUID":window.localStorage['phoneUUID'], "phoneMake": window.localStorage['phoneMake'], "phoneModel": window.localStorage['phoneModel'], "phoneOS": window.localStorage['phoneOS'], "appVersion":window.localStorage['appVersion'],
                                                      "medicalAlertPresent": medicalAlertPresent, "medicalAlertName": medicalAlertName, "medicalAlertDesc": medicalAlertDesc
                                                   };

                                          console.log("***** deviceErrorArray: " +  JSON.stringify(deviceErrorArray));







                                                    $http({  method: 'POST', url: chgServerApi.serverName + "/api/createDeviceErrors", data: deviceErrorArray }).then(function successCallback(response)
                                                    {



                                                                    var synchedFlag = 0;
                                                                    synchedFlag = parseInt(response.data.synched);

                                                                     //  window.localStorage['insertedCHGBPRID'] = response.data.chgbprid;
                                                                    /// window.localStorage['insertedCHGBPRID']  = 999; // for now...
                                                                         console.log("synchedFlag == " + synchedFlag);


                                                                 if (synchedFlag == 1)
                                                                 {
                                                                                  var deviceErrorID = parseInt(window.localStorage['deviceErrorID']);

                                                                                   function updateDE() {

                                                                                                    //NEW Style sqlite Query....
                                                                                                    db.transaction(function(tx) {
                                                                                                   //var query = "INSERT INTO bp_reading_processed (createdAt, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, feedback, outcome, phoneID, headachesSymptom, visDSymptom, epiGSymptom, noSymptoms) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                                   var uQuery = "UPDATE device_error SET chgSynched = ? WHERE rowid = ?";
                                                                                                   tx.executeSql(uQuery, [1, deviceErrorID], function(tx, res) {

                                                                                                          console.log("******* device_error Row Updated Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                          deleteDE();

                                                                                                      }); //end outer tx >> insert into

                                                                                                    },
                                                                                                     function(err) {
                                                                                                      console.log('app db ERROR on UPDATING (Synching) device_error Reading: ' + JSON.stringify(err));
                                                                                                    })




                                                                                      } //end updatePR


                                                                                     updateDE();



                                                                                     //now do DELETE
                                                                                     function deleteDE()
                                                                                     {

                                                                                       console.log("****  start of deleteDE()....");

                                                                                                    db.transaction(function(tx) {

                                                                                                     var uQuery = "DELETE FROM device_error WHERE chgSynched=1 AND userID = ?";
                                                                                                     tx.executeSql(uQuery, [window.localStorage['loggedIn_USERNAME']], function(tx, res) {
                                                                                                               console.log("**DELETED  device_error(s). The rowid is:" + res.rows.id);
                                                                                                               console.log("******* device_error Row DELETED Successfully for synch! res JSON: " + JSON.stringify(res));

                                                                                                                //LAST STEP IS TO DELETE THE synched==1 READINGS >> do later

                                                                                                        }); //end outer tx >> insert into

                                                                                                      },
                                                                                                       function(err) {
                                                                                                        console.log('app db ERROR on  DELETING (Synching) device error: ' + JSON.stringify(err));
                                                                                                      })


                                                                                        } //end deletePR




                                                                        } //end if synchedFlag ==1




                                                      }, function errorCallback(response) {

                                                                    console.log("Error Sending Data to CHG: " + JSON.stringify(response));

                                                                 });

                        } //end postDeviceErrorData



















})








//THIS does all the parseing / Post for bts and ble... >> better than having two identical functions in the bts and ble svcs
.service("parsePostService", function($rootScope, $state, $cordovaSQLite, $http, networkService, chgServerApi)
{


      this.process = function(thisBPData, counter1)
      {

        //this only appears once... ctr1 = 0, len => 21

      //alert("starting this.process()... window.localStorage['counter1']: " + window.localStorage['counter1'] + "; thisBPData.length => " + thisBPData.length);

                              //only want to do parse once.... (not up to 11 times) and what if only 1 or 2 arrays... could miss the data...
                              //if (thisBPData.length >= 28 & counter1 < 1 )
                              if (thisBPData.length >= 28 )
                              {

                                        //loop thru thisBPData[] array
                                        for (var v = 0; v < 28; v++)
                                        {

                                          console.log("***thisBPData["+v+"]: " + thisBPData[v]);

                                        }








                              //  alert("ONLY ONCE! thisBPData.length >= 28 && counter1 < 1");
                                //counter1 = counter1 + 1;
                                window.localStorage['counter1'] = window.localStorage['counter1'] + 1;

                               console.log("counter1: (should be 0)" + counter1);
                                //this is too small / not enough data in it...
                                //YOU SEE we're in onRawData() - which could run 1 or 10 times...
                                ///***** IF thisBPData.length >30
                             //alert("££££££ thisBPData[] json:: " + JSON.stringify(thisBPData));

                            //DO PARSING IN HERE!***************
                              //THESE SHOULDNT be Parse INT...
                               var sys1 =  thisBPData[11];
                               var dia1 = thisBPData[12];
                               var hr1 = thisBPData[13];

                                var sys2 =  thisBPData[18];
                                var dia2 = thisBPData[19];
                                var hr2 = thisBPData[20];

                                 var sys3 =  thisBPData[25];
                                 var dia3 = thisBPData[26];
                                 var hr3 = thisBPData[27];

                                 //CLEAR THE ARRAY TO STOP ISSUES ON RESTARTS....
                                //  thisBPData = [];


                               var finalSys = (  sys1 + sys2 + sys3  ) / 3;
                               var finalDia = (  dia1 + dia2 + dia3  ) / 3;
                               var finalHr =  (  hr1 + hr2 + hr3 ) / 3;


                               //@apr15.. >> ignore above... >> this.. is a better way..
                               var xSys = thisBPData.length - 8;
                               var xDia = thisBPData.length - 7;
                               var xHR = thisBPData.length - 6;

                                var bpmSys = thisBPData[xSys];
                                var bpmDia =  thisBPData[xDia];
                                var bpmHR =  thisBPData[xHR];

                                finalSys = bpmSys;
                                finalDia = bpmDia;
                                finalHR = bpmHR;
                                console.log("in parsePostService.process... using 8th-6th last of thisBPData...");




                               //MAP Calc. = [(2 x diastolic)+systolic] / 3
                               var finalMap = ((2 * finalDia) + finalSys) / 3;
                               //round to 2 dec. places
                               finalMap = finalMap.toFixed(2);


                               //counter of symptoms
                                window.localStorage['symptomCount'] = 0;
                                var symptomCount = parseInt(window.localStorage['symptomCount']);


                               finalSys = Math.round(parseFloat(finalSys));
                               finalDia = Math.round(parseFloat(finalDia));
                               finalHr = Math.round(parseFloat(finalHr));


                               window.localStorage['finalSys'] = finalSys;
                               window.localStorage['finalDia'] = finalDia;
                               window.localStorage['finalHr'] = finalHr;
                               window.localStorage['finalMap'] = finalMap;

                            console.log("''''''''''''''': setting finalSys: " + window.localStorage['finalSys'] + " ; finalDia: " + window.localStorage['finalDia'] + " ; finalHr: " + window.localStorage['finalHr']);
                               //still have OLD values :(
                               //alert("(parsePostService:: only ONCE): setting wls RESULT vars: finalSys: " + window.localStorage['finalSys'] + " ; finalDia: " + window.localStorage['finalDia'] + " ; finalHr: " + window.localStorage['finalHr']);


                               console.log("*****thisBPData.length ( > 100 Id say!!): " + this.BPData.length);

                               //****ADD SYMPTOMS LATER!
                               //check for high BP (MEDICAL Alerts)
                               //$state.go('tab.bpStep4');
                               //OK SYS: 90-159 // OK DIA: 40-99
                               //HIGH SYS: >=160 // HIGH Dia: >=100
                               //LOW SYS: <90 //LOW DIA: <40
                               if (finalSys > 90 & finalSys < 159 &  finalDia > 40 & finalDia < 99 & symptomCount < 2  )
                               {
                                //alert("3. BP is ok - NORMAL Result");
                                //OLD values here...
                                // $rootScope.$broadcast('eventFired', finalSys, finalDia, finalHr, finalMap);

                               //$state.go('tab.bpStep4');
                               $state.go('tab.btSSResult');

                               }
                               else if (finalSys >=160 || finalDia >= 100)
                               {
                                alert("3. Your BP is HIGH");
                                $state.go('tab.bpWarning1');
                              }
                              else if (finalSys < 90 || finalDia < 40)
                              {
                                alert("3. Your BP is LOW");
                                $state.go('tab.bpWarningLow');
                              }
                              else if (symptomCount > 1)
                              {
                                alert("3. You've >1 Pregnancy Symptoms'");
                                $state.go('tab.bpWarningLow');

                              }
                              else if ( symptomCount >= 1 & finalSys >= 140 & finalSys <= 159  ||  finalDia >=90  & finalDia <= 99  )
                              {
                               alert("3. Borderline High BP WITH >= 1 Symptom");
                               $state.go('tab.bpWarningCombined1');

                              }
                          /*   LEAVE THIS OUT for now... > 0.1 protein == trace!
                            else if ( urineProtein >= 0.1 & finalSys >= 140 & finalSys <= 159  ||  finalDia >=90  & finalDia <= 99  )
                              {
                               alert("Borderline High BP WITH >= 1 Symptom");
                               $state.go('tab.bpWarningCombined1');

                             }  */






                                                              //Save Reading to AppDb - tbl: bp_reading_processed
                                                             var dt = new Date();
                                                             var utcDate = dt.toUTCString();
                                                             var aUserID =  window.localStorage['loggedIn_USERID'];
                                                             var aBpmID = 1;
                                                             var aPhoneID =1 ;



                                                                         function insertProcessedBPData(dateTime, rawDataDate,  uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap, bpmid, phID )
                                                                         {
                                                                           console.log("*****------- inside insertProcessedBPData");
                                                                                var query = "INSERT INTO bp_reading_processed (createdAt, rawDataDate, userID, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmID, phoneID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                                                                //alert("query => " + query);
                                                                                $cordovaSQLite.execute(db, query, [dateTime, rawDataDate, uid, chgSynched, sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , bpmid, phID]).then(function(res) {
                                                                             console.log("INSERTED bpReadingProcessed. The ID is:" + res.insertId);
                                                                                window.localStorage['bprid'] = res.insertId;
                                                                                 console.log("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );


                                                                                    //  alert("calling postBPData() from inside insert to appdb)");

                                                                                    //Check to see that we have network conn.
                                                                                 var networkConn = networkService.check();

                                                                                     console.log("networkConn == " + networkConn);


                                                                                    if (networkConn == 1)
                                                                                    {
                                                                                     //alert("^^^^^^^^^^^^^^^ 99999999*************Insert to AppDB Done! Netw. Conn Present so calling postBPData()**");
                                                                                     console.log("^^^^^^^^^^^^^^^ 99999999*************Insert to AppDB Done! Netw. Conn Present so calling postBPData()**");
                                                                                     //alert("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);
                                                                                      // $state.go('tab.bpStep4');
                                                                                       //bleService.closeConn();
                                                                                      postBPData();

                                                                                    }
                                                                                    else {
                                                                                      console.log("888888888««««««««««««««Insert to AppDB Done!  No Network Connection so Data Stored Locally Only!");
                                                                                       //$state.go('tab.bpStep4');
                                                                                       //bleService.closeConn();
                                                                                    }





                                                                                }, function (err) {
                                                                                  alert(err);
                                                                                });
                                                                            }




                                                                                  //this appears but insertProcessedBPData() appears not to work... >>> try and paste code into bel svc... itself.
                                                                                   console.log("sssssssssss-********   inserting bpReadingProcessed to app db.... + THEN http post to chg");
                                                                                     console.log("loggedIn_USERID => " + window.localStorage['loggedIn_USERID']);
                                                                                    var initChgSynched = 0;
                                                                                    insertProcessedBPData(utcDate,  window.localStorage['rawDataDate']  , aUserID, initChgSynched , sys1, dia1, hr1, sys2, dia2, hr2, sys3, dia3, hr3, finalSys, finalDia, finalHr, finalMap , aBpmID, aPhoneID);
                                                                                   console.log("inserting bpReadingProcessed DONE!. Now Sending to CHG.....");









                                               //THIS IS NOT BEING CALLED I THINK >> I think it is actually >
                                               function postBPData()
                                               {



                                            //  alert("****************************************************************************************Start of postBPData().. $http... etc");
                                              console.log("****************************************************************************************Start of postBPData().. $http... etc");


                                                                var token = window.localStorage["TOKEN"];
                                                                var iOutcome = "Successful Reading";
                                                                var iFeedback = "No Feedback";

                                                                //alert("token is: " + token + "; about to start $http post...");



                                                                                                                                                                                      //fields from chgServer5.js
                                                                                                                                                                                      /*
                                                                                                                                                                                      token
                                                                                                                                                                                      bprCreatedAtRaw
                                                                                                                                                                                      bprCreatedAt.
                                                                                                                                                                                      chgUserID
                                                                                                                                                                                      systolic
                                                                                                                                                                                      diastolic
                                                                                                                                                                                      hr=66
                                                                                                                                                                                      &map
                                                                                                                                                                                      bprFeedback
                                                                                                                                                                                      bprOutcome
                                                                                                                                                                                      bpmID
                                                                                                                                                                                      phoneID
                                                                                                                                                                                      */




                                                                $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: window.localStorage['rawDataDate'], bprCreatedAt: utcDate, chgUserID: aUserID, systolic: finalSys, diastolic: finalDia, hr: finalHr, map: finalMap, bprFeedback: iFeedback, bprOutcome: iOutcome, bpmID: aBpmID, phoneID: aPhoneID   } }).then(function successCallback(response)
                                                                  {

                                                                            console.log("///*********************Inside the $http post... response json: " + JSON.stringify(response));
                                                                            console.log("token is: " + token);

                                                                                 var synchedFlag = 0;
                                                                                 synchedFlag = parseInt(response.data.synched);


                                                                                      console.log("synchedFlag == " + synchedFlag);


                                                                                 if (synchedFlag == 1)
                                                                                 {



                                                                                   var zbprid = parseInt(window.localStorage['bprid']);

                                                                                                function updateBPR() {
                                                                                                          //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                                                                                          uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE rowid = ?";
                                                                                                          //alert("uQuery == " + uQuery);
                                                                                                       $cordovaSQLite.execute(db, uQuery, [1, zbprid ]).then(function(res) {

                                                                                                  console.log(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                                                                                       }, function (err) {
                                                                                                         alert("update/synch error" + JSON.stringify(err));
                                                                                                       });
                                                                                                   }

                                                                                                  updateBPR();


                                                                                 }




                                                                   }, function errorCallback(response) {
                                                                                // called asynchronously if an error occurs
                                                                                // or server returns response with an error status.


                                                                                   //alert("Error: " + response);
                                                                                   alert("Error Sending Data to CHG: " + JSON.stringify(response));
                                                                                    //console.log(response.data.message);



                                                                              });

                                                   } //end postBPData








      } //end IF ... (  if (thisBPData.length >= 28 && counter1 < 1 ))



} //end this.process()

}) //end parsePostService









//bluetoothSerial SERVICE
.service("btSerialService", function($q, $state, $cordovaSQLite, $http, parsePostService, networkService, chgServerApi)
{
var readingOutcome = null;
var readingOutcomeDetails = null;
var bpmErrorPresent = null;
var bpmError1 = null;
var bpmError2 = null;
var bpmError3 = null;
var bpmError5 = null;
var bpmErrorOther = null;
var bpmErrorLowBatt = null;



var thisBPData = [];


    this.enabled= function()
    {

                            bluetoothSerial.enable( function()
                            {

                            this.connect();

                            },  function()
                                {
                                    //  alert("The user did *not* enable Bluetooth");
                                });

    }

    this.isBTEnabled = function()
    {

                              bluetoothSerial.isEnabled(
                                        function() {
                                        /// alert("BTSERIAL is enabled");




                                                  // listPairedDevices();


                                        },
                                        function() {
                                            //alert("BTSERIAL is *not* enabled... calling enable bt serial");
                                            this.enable();
                                        }
                                    );




      }


      function disconnect()
      {
            //alert("about to disconnect...");
            //THIS DOESNT wORK i thinkk..s
            bluetoothSerial.disconnect([success], [failure]);


      }





    this.connect = function()
    {



                      this.isBTEnabled();

                        //this is SET in Platform.ready()
                        var addrx = window.localStorage['mlAddr'];
                        bluetoothSerial.connect(addrx, connectSuccess, connectFailure);

                                  function connectSuccess()
                                  {
                                  //alert("Connected! Calling subcribe()");
                                    subscribeBTSerial();
                                  }

                                  function connectFailure()
                                  {
                                   //alert("Connection Failure!");

                                        if ( thisBPData.length > 0)
                                        {

                                        //  alert("normal disconnect!");
                                          disconnect();
                                           //$state.go('tab.btConnClosed');
                                              //use seperate service for that...
                                            //  alert("Calling ....  parseBPDataService.parse()");
                                            //  this.showBPData();
                                            //parseBPDataService.parse();

                                        }
                                        else
                                        {
                                            //alert("Connection Failure + No Data from BPM. RESTARTING BP Reading!");
                                          //  $state.go('tab.bpErrorOther');
                                        //  alert("***** GOING TO btConnClosed...");
                                           $state.go('tab.btConnClosed');

                                            disconnect();
                                        }


                                  }







      } //end this.Connect








            function subscribeBTSerial()
            {
               //empty  thisBPData = []; WHEN Subscribe is called!!
               thisBPData = [];
              // alert(" start of subscribeBTSerial thisBPData = []; " +  JSON.stringify(thisBPData));

                //alert("start of this.subscribeBTSerial... ");
                var byteArrayCount = null;
                byteArrayCount = 0;

                //var counter1 = 0;
                window.localStorage['counter1'] = null;
                window.localStorage['counter1'] = 0;


                                        bluetoothSerial.subscribeRawData(onRawData, subscribeFailed);

                                            // this runs for every byteArray.... could run 5-* TIMES > depending on data in bpm
                                            function onRawData(data)
                                            {

                                              //alert("raw data received!!! data: " + JSON.stringify(data));

                                                //this is a counter to see which array we are on... there will usually be many arrays and we are most interested in the first one
                                                byteArrayCount = byteArrayCount + 1;
                                                //alert("Just incremented byteArrayCount. It's now = " + byteArrayCount);



                                                  var rawData = new Uint8Array(data);
                                                  //alert("rawData (json) is: " + JSON.stringify(rawData) );
                                                    var dataArrayJSON = JSON.stringify(rawData);


                                                    for (var e = 0; e < rawData.length; e++)
                                                    {
                                                    thisBPData.push(rawData[e]);
                                                    }



                                                                             //Save RAW Data to AppDB >> this is v similar to code in the ble section... >> insert this value to b64 field: dataArrayJSON

                                                                             var dt = new Date();
                                                                             var utcDate = dt.toUTCString();

                                                                             //create a rawDataDate var to be posted into the processedData tbl... >> to help link the raw and the processed data
                                                                             window.localStorage['rawDataDate'] = utcDate;


                                                                             var aUserID = window.localStorage['loggedIn_USERID'];
                                                                             var aString = "No String Yet.";
                                                                             var aBpmID = window.localStorage['mlAddr'];
                                                                             var aPhoneID = "An IMEI Num here... - from CHG";


                                                                                          //INSERTING THE RAW DATA TO APP DB
                                                                                         function insertRawBPData(dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails ) {
                                                                                        //   alert("starting insertRawBPData()....");
                                                                                                var query = "INSERT INTO bp_reading_raw (createdAt, userID, base64Data, stringData, hexData, bpmID, phoneID, readingOutcome, readingOutcomeDetails) VALUES (?,?,?,?,?,?,?,?,?)";
                                                                                                $cordovaSQLite.execute(db, query, [dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails]).then(function(res) {
                                                                                                //alert("INSERTED bpReadingRaw ID:" + res.insertId);
                                                                                                }, function (err) {
                                                                                                alert(err);
                                                                                                });
                                                                                            }




                                                                                    //CHECK FOR BPM ERRORS!
                                                                                    if ( byteArrayCount == 1 && thisBPData[12] == 114 )
                                                                                    {
                                                                                    bpmErrorPresent = true;
                                                                                    console.log("BPM Error. Please Retake BP Reading.");
                                                                                    console.log("The BPM Error Code is..... ");

                                                                                                    if (  thisBPData[13] == 1)
                                                                                                    {
                                                                                                    //  alert("BPM Error 1");


                                                                                                      readingOutcome = "BPM Error 1";
                                                                                                      readingOutcomeDetails = "BPM Error 1: Signals too weak.";
                                                                                                      bpmError1 = true;


                                                                                                       console.log("inserting raw bp data to app db (bpm error 1)");
                                                                                                       insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                       console.log("inserting raw bp data DONE!");


                                                                                                       $state.go('tab.bpError1');
                                                                                                       bpmError1 = false;

                                                                                                       disconnect();

                                                                                                    }
                                                                                                    else if ( thisBPData[13] == 2)
                                                                                                    {
                                                                                                    //  alert("BPM Error 2");


                                                                                                      readingOutcome = "BPM Error 2";
                                                                                                      readingOutcomeDetails = "BPM Error 2: Error Signal";
                                                                                                      bpmError2 = true;


                                                                                                       console.log("inserting raw bp data to app db (bpm error 2)");
                                                                                                       insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                       console.log("inserting raw bp data DONE!");

                                                                                                         $state.go('tab.bpError2');
                                                                                                         bpmError2 = false;
                                                                                                         disconnect();

                                                                                                    }
                                                                                                    else if ( thisBPData[13] == 3)
                                                                                                    {
                                                                                              //  alert("BPM Error 3");

                                                                                                    readingOutcome = "BPM Error 3";
                                                                                                    readingOutcomeDetails = "BPM Error 3: No Pressure in the Cuff";
                                                                                                    bpmError3 = true;


                                                                                                     console.log("inserting raw bp data to app db (bpm error 3)");
                                                                                                  insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                     console.log("inserting raw bp data DONE!");

                                                                                                       $state.go('tab.bpError3');
                                                                                                       bpmError3 = false;
                                                                                                       disconnect();


                                                                                                    }
                                                                                                    else if (  thisBPData[13] == 5)
                                                                                                    {
                                                                                                    console.log("BPM Error 5");

                                                                                                      readingOutcome = "BPM Error 5";
                                                                                                      readingOutcomeDetails = "BPM Error 3: Abnormal Result";
                                                                                                      bpmError5 = true;


                                                                                                       console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                                       insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                       console.log("inserting raw bp data DONE!");

                                                                                                         $state.go('tab.bpError5');
                                                                                                         bpmError5 = false;
                                                                                                         disconnect();

                                                                                                    }
                                                                                                    else if (  thisBPData[13] == 66)
                                                                                                    {
                                                                                                    console.log("BPM Error - Low Batteries");

                                                                                                      readingOutcome = "BPM Error - Low Batteries";
                                                                                                      readingOutcomeDetails = "BPM Error: Low Batteries";
                                                                                                      bpmErrorLowBatt = true;


                                                                                                       console.log("inserting raw bp data to app db (bpm error 4)");
                                                                                                      insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                       console.log("inserting raw bp data DONE!");

                                                                                                       $state.go('tab.bpmErrorLowBatt');
                                                                                                       bpmError5 = false;
                                                                                                       disconnect();

                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                      console.log("Other BPM Error!");

                                                                                                         readingOutcome = "BPM Error - Other";
                                                                                                         readingOutcomeDetails = "BPM Error Other";
                                                                                                         bpmErrorOther = true;


                                                                                                         console.log("inserting raw bp data to app db (bpm error other)");
                                                                                                         insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                                         console.log("inserting raw bp data DONE!");

                                                                                                           $state.go('tab.bpErrorOther');
                                                                                                           bpmErrorOther = false;
                                                                                                           disconnect();


                                                                                                    }


                                                                                    }
                                                                                    //IF a SUCCESSFUL Reading - We need to caputure ALL the Arrays >> NO! 11 will cover it as bpData[27] is highest array element....
                                                                                    //So.... 11 arrays is still way more than enough....
                                                                                    else if ( byteArrayCount < 11 && thisBPData[12] != 114) {


                                                                                            bpmErrorPresent = false;
                                                                                            //set readingOutcome + readingOutcomeDetails
                                                                                            readingOutcome = "BP Reading Success";
                                                                                            readingOutcomeDetails = "No BPM Errors Present. Reading Completed Successfully";


                                                                                          //alert("inserting raw bp data to app db (no bpm errors)");
                                                                                            insertRawBPData(utcDate, aUserID, dataArrayJSON, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                                          //  alert("inserting raw bp data DONE! -- PARSING HAPPENS NOW...");

                                                                                          //CALL the parsePost svc to do all the work... >> 1 svc for both ble and bts
                                                                                          //THe CHECK NET conn should be a svc too...
                                                                                        //  alert("calling arsePostService.process(). counter is: " + window.localStorage['counter1']);
                                                                                        // alert("about to call parsePost.process(); this.bpDataArray is: " + JSON.stringify(thisBPData) );
                                                                                          parsePostService.process(thisBPData, window.localStorage['counter1']);
                                                                                          //EMPTY this.bpData[];


                                                                                          disconnect();

                                                                                          //  counter1 = counter1 + 1;

                                                                                          //REMOVE ALL BELOW THIS LINE INTO THE parsePostService







                                                                                    }  //end else if (ie no bpm errors [114 - 1,2,3,5 etc] so PARSE etc...)







                                                              } //end onRawData()

                                                              function subscribeFailed()
                                                              {
                                                                //alert("subscribe btSerial failed");
                                                              }




              } //end subscribeBTSerial










    }) //end btSerialService









.service("bleService", function($state, $cordovaSQLite, $cordovaBluetoothLE, $base64, parsePostService, networkService)
{
//ideally this should be the uuid from the chg
//for now we set it in scan()>>scan.result.

var readingOutcome = null;
var readingOutcomeDetails = null;
var bpmErrorPresent = null;
var bpmError1 = null;
var bpmError2 = null;
var bpmError3 = null;
var bpmError5 = null;
var bpmErrorOther = null;
var bpmErrorLowBatt = null;

var bpmUUID = null;
//bpmUUID = "5E9B7376-173F-FE68-264F-38169673DFD8";

var thisBPData = [];
  var byteArrayCount = null;
  byteArrayCount = 0;




      this.init = function()
      {

    //  alert(">>>>>>>>>>>>starting init ble....");

                    $cordovaBluetoothLE.initialize({request:true}).then(null,
                    function(obj) {
                      //Handle errors
                        console.log("Initialize Error : " + JSON.stringify(obj));

                    },
                    function(obj) {
                      //Handle successes
                    //  alert("Initialize Success : " + JSON.stringify(obj));

                        //it doesnt seem to call this method...
                      scanBle();
                    }
                  );


      }

      function scanBle()
      {
      // alert("starting scan ble....");

                  $cordovaBluetoothLE.startScan({serviceUuids:[]}).then(null,
                  function(obj)
                  {
                    //Handle errors
                    console.log("BLE Scan Error: " +  JSON.stringify(obj));

                  },
                  function(obj)
                  {
                              if (obj.status == "scanResult")
                              {
                                //Device found
                              //alert("Scan Result Present. obj.address: " + obj.address + " - full obj: " + JSON.stringify(obj) );
                                bpmUUID = obj.address;


                                      connectBle(  bpmUUID  );


                                                //STOP SCAN
                                                function stopScan()
                                                {
                                                      console.log("Stop Scan");

                                                      $cordovaBluetoothLE.stopScan().then(function(obj) {
                                                        console.log("Stop Scan Success : " + JSON.stringify(obj));
                                                      }, function(obj) {
                                                          console.log("Stop Scan Error : " + JSON.stringify(obj));
                                                      });
                                                }

                                                stopScan();


                              }
                                else if (obj.status == "scanStarted")
                                  {
                                    //Scan started
                                 console.log("Scan Started");
                                  }
                                }
                              );

          }




        function closeConn(uuid)
        {
        //  var  bpmUUID_ = "5E9B7376-173F-FE68-264F-38169673DFD8";

                     //clear these vars BUT BUT BUT Only after the conn is CLOSED and ALL Data is Got!
                     thisBPData = [];
                     byteArrayCount = 0;
                  console.log("vars CLEARED! thisBPData[] => " + JSON.stringify(thisBPData));


                  //alert("Closing bleConn... bpmUUID => " + uuid);
                  var params = {address: uuid};
                    console.log("close: " + JSON.stringify(params));




                        $cordovaBluetoothLE.close(params).then(function(obj) {
                          console.log("Close Success : " + JSON.stringify(obj));
                          }, function(obj) {
                            console.log("Close Error : " + JSON.stringify(obj));
                          });


        }


        this.disconnect = function()
        {
          var params = {address: bpmUUID};

        console.log("Attempting Disconnect to: " + params);

            $cordovaBluetoothLE.disconnect(params).then(function(obj) {
          console.log("Disconnect Success : " + JSON.stringify(obj));
            }, function(obj) {
            console.log("Disconnect Error : " + JSON.stringify(obj));
            });




        }


      function connectBle(bpmUUID)
        {

          //alert("starting connect ble....");
            $cordovaBluetoothLE.connect({address: bpmUUID }).then(null, function(obj)
            {


                      //Handle errors
                //  alert("Connect Error! Message: " + JSON.stringify(obj));


                                    if (obj.message == "Connection failed" )
                                    {

                                        //showBPData();
                                      //console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                      //  this.disconnect();
                                    alert("***** Connection Failed");
                                     closeConn(bpmUUID);
                                     $state.go('tab.bpErrorOther');



                                      //  closeConn(bpmUUID);
                                      //  console.log("***** FINISHED CLOSING BT CONN");

                                    }







                  },
                  function(obj) {
                    if (obj.status == "connected")
                    {

                      //This works... :) //subscribe next...
                      console.log("connected. obj is: " + JSON.stringify(obj));

                      //may need to do a scan first as it gives a "service not found" error
                      //do a discover/ services (ios)... first..
                   console.log("call showServices() ");
                   showServices();

                    }
                    else if (obj.status == "connecting") {
                      //Device connecting
                      console.log("connecting....");

                    } else {

                        console.log("Conn. else... Should trigger on ios. window.localStorage['isAndroid'] is: " + window.localStorage['isAndroid']);



                                  console.log("disconnected...");
                                        //  console.log(" Calling showBPData().... just after disconnected....");
                                          //showBPData();
                                      //  console.log("showBPData() called succcessfully. BP Result should be on the app.");


                                        //  this.disconnect();

                               console.log("***** CLOSING BT CONN");
                                    closeConn(bpmUUID);
                                    console.log("***** FINISHED CLOSING BT CONN");

                                     console.log("***** GOING TO btConnClosed...");
                                  //   $state.go('tab.btConnClosed');

                              }






                    }
                  ); //end connect.then...




        }


            //ios...
           function showServices()
            {
              var params = {address: bpmUUID , serviceUuids: [ ] };
              $cordovaBluetoothLE.services(params).then(success, error);



                            function success(obj)
                            {
                        //  alert("Services Success : " + JSON.stringify(obj));
                            //it gets as far as here...

                              if (obj.status == "services")
                              {
                                console.log("obj.status == Services");

                              // var serviceUuids = obj.services;
                            //  alert("serviceUuids[1]: " + JSON.stringify(servicesUuids[1]));
                          //  alert("obj.services: " + JSON.stringify(obj.services[2]));

                               var fff0Service =  obj.services[2];
                            //  alert("fff0Service: " + fff0Service + " ; " );

                                //THIS WORKS!!!!! >> getting CN now on bpm :)!! yay!!!
                                // data show appear as an console.log when reading is complete
                                //WHY DOES IT GET CHS THO?
                                //it actually fails to subscribe with this line...
                                //subscribeBle(addr, fff0Service,   "fff1");

                                //seems to fall over here...
                                //skip it for now
                               //addService(addr, scope.fff0Service );
                               //console.log("svc not added. skipped this for now.");


                                /*
                                for (var i = 0; i < serviceUuids.length; i++)
                                {
                                  $cordovaBluetoothLE.addService(obj.address, serviceUuids[i]);
                                  console.log("adding svc["+i+"]. serviceUuid:  " + serviceUuids[i]  );
                                }

                                */

                                  //now call subscribe >> Noooo >> need to get characteristics first
                                  //console.log("Getting characteristics now...");
                              //var fff0Service = "fff0";
                              getCharacteristics(fff0Service);


                                //subscribeBle(bpmUUID, fff0Service, "fff1");




                              }
                              else
                              {
                                console.log("Unexpected Services Status");
                              }
                            }

                            function error(obj)
                            {
                                console.log("Services Error : " + JSON.stringify(obj));
                            }



            } //end this.showServices()



          function getCharacteristics(svc)
            {
            //  alert("++ Starting getCHS... ::" + bpmUUID + " . svc: " + svc);

            var params = {address: bpmUUID , service: svc};
            $cordovaBluetoothLE.characteristics(params).then(success, error);

                            function success(obj)
                            {
                          //   alert("Characteristics Success : " + JSON.stringify(obj));

                              if (obj.status == "characteristics")
                              {
                            //    alert("obj.status == characteristics");

                                var characteristics = obj.characteristics;

                                      var fff1ch = 'FFF1';
                                      var fff2ch = 'FFF2';

                                  //    alert("About to subscribeBle... >> " + bpmUUID +  svc + fff1ch );
                                      subscribeBle(bpmUUID, svc, fff1ch);



                              }
                              else
                              {
                                console.log("Unexpected Characteristics Status");
                              }
                            }

                            function error(obj)
                            {
                              console.log("Characteristics Error : " + JSON.stringify(obj));
                            }




            } //end getCHs




          function b64toHex(str)
          {

                      function asc2hex(pStr)
                      {
                                    tempstr = '';
                                    for (a = 0; a < pStr.length; a = a + 1) {
                                        tempstr = tempstr + pStr.charCodeAt(a).toString(16);
                                    }
                                    return tempstr;
                       }

                        //first convert b64 to string
                        var decodedString = $base64.decode(str);
                        //then convert string(ascii) to hex
                        var hexString = asc2hex(decodedString);

                        return hexString;
            }






            function subscribeBle(uuid, svc, ch)
            {








                  window.localStorage['counter1'] = null;
                  window.localStorage['counter1'] = 0;

                var params = {address:uuid, service:svc, characteristic:ch};
                $cordovaBluetoothLE.subscribe(params).then(null, error, success);

                var decodedString = $base64.decode(obj.value);


                  function success(obj)
                  {


                    //NEED TO PLAN THIS CAREFULLY.... > ideally break off common components from bts svc into sepearate svc which both of these svcs can use (ie ble and bts)
                    //OR
                    //Paste in the code from the bts svc... >> quick and easy approacb
                    //==> Build seperate svc... well worth it!!
                      if (obj.status == "subscribedResult")
                      {


                                  byteArrayCount = byteArrayCount + 1;

                                  //convert the b64 data to hex >> no necessary though... except for storing it.
                                  var convertedString = b64toHex(obj.value);

                                  console.log("subscribedResult Present! obj.value is (b64): " + obj.value);
                                  console.log("subscribedResult Present! obj.value is (HEX): " + convertedString);

                                  var bytes = $cordovaBluetoothLE.encodedStringToBytes(obj.value);


                                                 //Rand Dusing: Base64 encoded string of bytes. Use bluetoothle.encodedStringToBytes(obj.value) to convert to a unit8Array.
                                                 // See characteristic's specification and example below on how to correctly parse this.

                                                  console.log("    ");
                                                  console.log("===== Start of Array # " + byteArrayCount + " =====");

                                                  var bytesLen = bytes.length;
                                                  for (var i = 0; i < bytesLen; i++)
                                                  {
                                                    //console.log("bytes[" + i + "] is: " + bytes[i]);
                                                    //push each array element into a single array
                                                    thisBPData.push(bytes[i]);
                                                  }

                                                  console.log("===== End of Array # " + byteArrayCount + " =====");
                                                  console.log("    ");


                                                   //Save RAW Data to AppDB
                                                   var dt = new Date();
                                                   var utcDate = dt.toUTCString();

                                                      //create a rawDataDate var to be posted into the processedData tbl... >> to help link the raw and the processed data
                                                      window.localStorage['rawDataDate'] = utcDate;


                                                   var aUserID = window.localStorage['loggedIn_USERID'];
                                                   var aString = "No String Yet."
                                                   var aBpmID = "MicroLife BPM ID Num... - from CHG";
                                                   var aPhoneID = "An IMEI Num here... - from CHG";


                                                                //INSERTING THE RAW DATA TO APP DB
                                                               function insertRawBPData(dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails ) {
                                                                      var query = "INSERT INTO bp_reading_raw (createdAt, userID, base64Data, stringData, hexData, bpmID, phoneID, readingOutcome, readingOutcomeDetails) VALUES (?,?,?,?,?,?,?,?,?)";
                                                                      $cordovaSQLite.execute(db, query, [dateTime, uid, b64, str, hex, bpmid, phID, readingOutcome, readingOutcomeDetails]).then(function(res) {
                                                                        console.log("INSERTED bpReadingRaw ID:" + res.insertId);
                                                                      }, function (err) {
                                                                        console.log(err);
                                                                      });
                                                                  }




                                                          //CHECK FOR BPM ERRORS!

                                                          //should only check first array for errors.
                                                          //eg 114 followed by 1 or 2 or 3 or 5...
                                                          if (byteArrayCount == 1 && bytes[12] == 114)
                                                          {
                                                          bpmErrorPresent = true;
                                                          console.log("BPM Error. Please Retake BP Reading.");
                                                          console.log("The BPM Error Code is..... ");

                                                                          if (bytes[13] == 1)
                                                                          {
                                                                            console.log("BPM Error 1");


                                                                            readingOutcome = "BPM Error 1";
                                                                            readingOutcomeDetails = "BPM Error 1: Signals too weak.";

                                                                              bpmError1 = true;


                                                                              $state.go('tab.bpError1');

                                                                             console.log("inserting raw bp data to app db (bpm error 1)");
                                                                             insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                             console.log("inserting raw bp data DONE!");



                                                                            closeConn(uuid);

                                                                          }
                                                                          else if (bytes[13] == 2)
                                                                          {
                                                                            console.log("BPM Error 2");


                                                                            readingOutcome = "BPM Error 2";
                                                                            readingOutcomeDetails = "BPM Error 2: Error Signal";
                                                                              bpmError2 = true;

                                                                              $state.go('tab.bpError2');

                                                                             console.log("inserting raw bp data to app db (bpm error 2)");
                                                                             insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                             console.log("inserting raw bp data DONE!");


                                                                            closeConn(uuid);

                                                                          }
                                                                          else if (bytes[13] == 3)
                                                                          {
                                                                        console.log("BPM Error 3");

                                                                          readingOutcome = "BPM Error 3";
                                                                          readingOutcomeDetails = "BPM Error 3: No Pressure in the Cuff";
                                                                            bpmError3 = true;

                                                                             $state.go('tab.bpError3');

                                                                           console.log("inserting raw bp data to app db (bpm error 3)");
                                                                           insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                           console.log("inserting raw bp data DONE!");



                                                                            closeConn(uuid);
                                                                          }
                                                                          else if (bytes[13] == 5)
                                                                          {
                                                                          console.log("BPM Error 5");

                                                                            readingOutcome = "BPM Error 5";
                                                                            readingOutcomeDetails = "BPM Error 3: Abnormal Result";
                                                                              bpmError5 = true;

                                                                               $state.go('tab.bpError5');

                                                                             console.log("inserting raw bp data to app db (bpm error 4)");
                                                                             insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                             console.log("inserting raw bp data DONE!");



                                                                            closeConn(uuid);

                                                                          }
                                                                          else if (  thisBPData[13] == 66)
                                                                            {
                                                                            console.log("BPM Error - Low Batteries");

                                                                              readingOutcome = "BPM Error - Low Batteries";
                                                                              readingOutcomeDetails = "BPM Error: Low Batteries";
                                                                              bpmErrorLowBatt = true;


                                                                               console.log("inserting raw bp data to app db (bpm error  low batt)");
                                                                              insertRawBPData(utcDate, aUserID, obj.value, aString, aString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                               console.log("inserting raw bp data DONE!");

                                                                               $state.go('tab.bpmErrorLowBatt');
                                                                               bpmErrorLowBatt = false;

                                                                            closeConn(uuid);

                                                                            }
                                                                          else
                                                                          {
                                                                            console.log("Other BPM Error!");

                                                                              readingOutcome = "BPM Error - Other";
                                                                              readingOutcomeDetails = "BPM Error Other";

                                                                                bpmErrorOther = true;

                                                                                   $state.go('tab.bpErrorOther');

                                                                               console.log("inserting raw bp data to app db (bpm error other)");
                                                                               insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                               console.log("inserting raw bp data DONE!");



                                                                              closeConn(uuid);


                                                                          }


                                                          }
                                                        //  else if (byteArrayCount == 1 && bytes[12] != 114) {
                                                          //As ble gets base64 strings we can store the b64 so don't need to submit any more than 1 row to raw tbl... (appdb)
                                                          //else if ( byteArrayCount < 11 && thisBPData[12] != 114) {
                                                          //TRY THIS - 4Apr...
                                                            else if ( thisBPData[12] != 114 & thisBPData.length >= 28 &  byteArrayCount < 11  ) {


                                                                  bpmErrorPresent = false;
                                                                  //set readingOutcome + readingOutcomeDetails
                                                                  readingOutcome = "BP Reading Success";
                                                                  readingOutcomeDetails = "No BPM Errors Present. Reading Completed Successfully";


                                                                   console.log("inserting raw bp data to app db (no bpm errors)");
                                                                   insertRawBPData(utcDate, aUserID, obj.value, aString, convertedString, aBpmID, aPhoneID, readingOutcome, readingOutcomeDetails);
                                                                   console.log("*******inserting raw bp data DONE! calling parsePostService");




                                                                   //alert("calling parsePostService.process()... this.BPData[]: ) " + JSON.stringify(thisBPData));
                                                                   parsePostService.process(thisBPData, window.localStorage['counter1']);
                                                                   //increment counter 1 as we only want to call parsePostService once...
                                                                  // counter1 = counter1 + 1;

                                                                        if (byteArrayCount == 3)
                                                                        {
                                                                           alert("closing conn....");
                                                                          closeConn(uuid);
                                                                        }


                                                                  //closeConn(uuid);


                                                          }  //end else if (ie no bpm errors [114 - 1,2,3,5 etc])







                        } //end if sub. result
                        else if (obj.status == "subscribed")
                        {
                      console.log("Subscribed to BPM");

                        }
                        else
                        {
                        //  console.log("Unexpected Subscribe Status.");

                        }



                      } //end subscribeSuccess()


                      // subscribe Error present
                      function error(obj)
                      {
                      //  console.log("Subscribe Error : " + JSON.stringify(obj));
                      console.log("Subscribe Error : " + JSON.stringify(obj));

                      var iStr5 = "Subscribe Error : " + JSON.stringify(obj);
                      addToLog(iStr5, 16);

                      }


                return false;






            } //end this.subscribe()




}) //end bleService

















.service("testLocalDBService", function($cordovaSQLite)
{


           this.insertTestData = function(name, year ) {

              alert("starting insertTestData()");
                  var query = "INSERT INTO test (name, year) VALUES (?,?)";
                  $cordovaSQLite.execute(db, query, [name, year]).then(function(res) {
                  alert("INSERTED test data . The ID is:" + res.insertId);
                  window.localStorage['testDataid'] = res.insertId;
                   //alert("-- in insert proc. data. method window.localStorage['appDB_bprID'] == " + window.localStorage['bprid'] );


                      //  alert("calling postBPData() from inside insert to appdb)");
                      //  postBPData();
                      //  alert("DONE calling postBPData()");



                  }, function (err) {
                    alert(err);
                  });
              }




                this.updateTestData = function(idd, year) {

                  //do test insert first...
                  //(db, "CREATE TABLE IF NOT EXISTS test (idd integar primary key, name text, year text)")

                  alert("starting updateTestData()...");
                          //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                          uQuery = "UPDATE test SET year=? WHERE rowid=1";
                          alert("uQuery == " + uQuery);
                       $cordovaSQLite.execute(db, uQuery, [year]).then(function(res) {

                       alert(" Row Updated Successfully! res JSON: " + JSON.stringify(res));


                       }, function (err) {
                         alert("update/synch error" + JSON.stringify(err));
                       });
                   }





                   this.readTestData = function(idd)
                   {
                     //alert(1);

                       var query = "SELECT rowid, * FROM test";
                         $cordovaSQLite.execute(db, query).then(function(res) {
                             if(res.rows.length > 0) {
                               //  alert("SELECTED row0 -> " + res.rows.item(0).createdAt + " - " + res.rows.item(0).base64Data + " - " + res.rows.item(0).phoneID);
                                 alert("Now, here's all the data");


                                                 for(var i = 0; i < res.rows.length; i++) {


                                                  // alert("bpID: " +  res.rows.item(i).rowid + "; CreatedAt Time: " + res.rows.item(i).createdAt + "; base64Data: " + res.rows.item(i).base64Data + "; hexData: " + res.rows.item(i).hexData + "; Outcome: " + res.rows.item(i).readingOutcome
                                                  //    + "; Outcome Details: " + res.rows.item(i).readingOutcomeDetails );

                                                      alert( "TEST DATA Row: id: " + res.rows.item(i).rowid + "; Name: " + res.rows.item(i).name + "; Year: " + res.rows.item(i).year );


                                                    }

                                              }



                   });




                 }



})


.service("logDataService", function($cordovaSQLite, $http, networkService)
{

        var logData = [];

            this.getData = function(uid)
            {

              //logData.createdAt =
              //logData.sys = 122;
              //logData.dia = 99;

              //logData[0] = "abc";
              //logData[1] = "def";


                            var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ?";
                            $cordovaSQLite.execute(db, query, [uid]).then(function(res)
                            {
                                 alert(" res JSON: " + JSON.stringify(res));


                                      if(res.rows.length > 0)
                                      {

                                          //alert("Now, here's all the data");


                                                                  for(var i = 0; i < res.rows.length; i++)
                                                                  {
                                                                  /*  logData.id = res.rows.item(i).rowid;
                                                                    logData.createdAt = res.rows.item(i).createdAt;
                                                                    logData.readingType = "Blood Pressure";
                                                                    logData.systolic = res.rows.item(i).finalSys;
                                                                    logData.diastolic = res.rows.item(i).finalDia;
                                                                    logData.hr = res.rows.item(i).finalHr;
                                                                    logData.map = res.rows.item(i).finalMap; */

                                                                    //var logItem = res.rows.item(i).createdAt + " - Blood Pressure - " + "<br />" + "<b>Systolic:</b>: " + res.rows.item(i).finalSys + " - <b>Diastolic: 0</b>" + res.rows.item(i).finalDia + " - <b>HR:</b> " + res.rows.item(i).finalHr + " - MAP: " + res.rows.item(i).finalMap;
                                                                    alert("res: " + JSON.stringify(res));
                                                                    logData.push(res.rows.item(i));



                                                                   //logData.push({id: res.rows.item(i).rowid, createdAt: res.rows.item(i).createdAt, readingType: "Blood Pressure", systolic: res.rows.item(i).finalSys, diastolic: res.rows.item(i).finalDia, hr: res.rows.item(i).finalHr, map: res.rows.item(i).finalMap });

                                                                  } //end loop




                                                  alert("end res.rows.length > 0");

                                          } //end if res.rows.length >0







                                },
                                function (err) {
                                  alert("Loading of Log Data Failed!" + JSON.stringify(err));
                                });


                                    //return the array of data
                    return logData;


            }


            this.showLogData = function()
            {
                //alert("showing logData JSON => " + JSON.stringify(logData));

            }




}) //end logDataService


.service("synchAppCHGService", function($cordovaSQLite, $http, networkService, chgServerApi, $timeout)
{

 var synchDataArray = [];

                this.synch = function(uid, flag)
                {

                  //var unsynchedArray = [];

              //  alert("starting this.sync()");

                var query = "SELECT rowid, * FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
              //  var query = "SELECT rowid, createdAt, rawDataDate, userID, finalSys, finalDia, finalHr, finalMap, bpmID, phoneID FROM bp_reading_processed WHERE userID = ? AND chgSynched = ?";
                $cordovaSQLite.execute(db, query, [uid, flag ]).then(function(res)
                {
                    // alert(" res JSON: " + JSON.stringify(res));


                          if(res.rows.length > 0)
                          {

                            //  alert("Now, here's all the data");


                                                      //for(var i = 0; i < res.rows.length; i++)
                                                      //only runs once >> to test if it's latency thing...
                                                      //for(var i = 0; i < 1; i++)
                                                      //DO 1 @ AT TIME FOR NOW!
                                                      for(var i = 0; i < res.rows.length; i++)
                                                      {


                                                          //alert( "Loop#" + i + " ; about to synch!!!unsynched data row: id: " + res.rows.item(i).rowid + "; uid: " + res.rows.item(i).userID + "; Year: " + res.rows.item(i).createdAt);

                                                            //push each row into the array
                                                         synchDataArray.push(res.rows.item(i));


                                                       var token = window.localStorage["TOKEN"];
                                                        //  alert("token....: " + window.localStorage["TOKEN"]);


                                                      /*   var iAppBprID = res.rows.item(i).rowid;
                                                          var iCreatedAt = res.rows.item(i).createdAt;
                                                          var iRawDataDate = res.rows.item(i).rawDataDate;
                                                          var iUserID = res.rows.item(i).userID;
                                                          var iSys = res.rows.item(i).finalSys;
                                                          var iDia = res.rows.item(i).finalDia;
                                                          var iHr = res.rows.item(i).finalHr;
                                                          var iMap = res.rows.item(i).finalMap;
                                                          var iFeedback = res.rows.item(i).feedback;
                                                          iFeedback = "testFeedback...";
                                                          var iOutcome = res.rows.item(i).outcome;
                                                          iOutcome = "test Outcome....";
                                                          var iBpmID = res.rows.item(i).bpmID;
                                                          var iPhoneID = res.rows.item(i).phoneID; */




                                                        } //end loop here

                                                       console.log("********************  synchDataArray is: " + JSON.stringify(synchDataArray));
                                                         console.log("synchDataArray.length: " + synchDataArray.length);
                                                         console.log("synchDataArray[0]: " + JSON.stringify(synchDataArray[0]) );
                                                         console.log("synchDataArray[1]: " + JSON.stringify(synchDataArray[1]) );




                                                          var networkConn = networkService.check();


                                                             if (networkConn == 1)
                                                             {
                                                              alert("Network Conn. Present.... Doing $http Synch POST");





                                                                                                //$http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: iRawDataDate, bprCreatedAt: iCreatedAt, chgUserID: iUserID, systolic: iSys, diastolic: iDia, hr: iHr, map: iMap, bprFeedback: iFeedback, bprOutcome: iOutcome, bpmID: iBpmID, phoneID: iPhoneID   } })
                                                                                                $http({  method: 'POST', url: chgServerApi.serverName + "/api/createSynchBPReadings", data: { token: window.localStorage["TOKEN"], synchData: synchDataArray } })
                                                                                                    .then(
                                                                                                    function successCallback(response)
                                                                                                    {


                                                                                                      console.log("**Starting Synch post...");

                                                                                                                  alert("2. Inside the $http post... response json: " + JSON.stringify(response));
                                                                                                                     var synchedFlag = 0;
                                                                                                                     synchedFlag = parseInt(response.data.synched);
                                                                                                                  //  alert("response.data.synched: " + response.data.synched);
                                                                                                                    alert("inserted rows:  " +  JSON.stringify(response.data.insertedRows));

                                                                                                                     //empty the array
                                                                                                                     synchDataArray = [];


                                                                                                                      //      alert("synchedFlag == " + synchedFlag);



                                                                                                                //  synchedFlag = 1;

                                                                                                                   if (synchedFlag == 1)
                                                                                                                     {


                                                                                                                      // var zbprid = iAppBprID;

                                                                                                                                    function updateBPR() {
                                                                                                                                              //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                                                                                                                              uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE rowid > 0";
                                                                                                                                              //alert("uQuery == " + uQuery);
                                                                                                                                           $cordovaSQLite.execute(db, uQuery, [1 ]).then(function(res) {

                                                                                                                                           alert("SYNCHING:: Row Updated Successfully! res JSON: " + JSON.stringify(res));


                                                                                                                                           }, function (err) {
                                                                                                                                             alert("update/synch error" + JSON.stringify(err));
                                                                                                                                           });
                                                                                                                                       }

                                                                                                                                      updateBPR();


                                                                                                                        }




                                                                                                       }, function errorCallback(response) {
                                                                                                                    // called asynchronously if an error occurs
                                                                                                                    // or server returns response with an error status.


                                                                                                                       //alert("Error: " + response);
                                                                                                                       alert("Error Sending Data to CHG: " + JSON.stringify(response));
                                                                                                                          //empty the array
                                                                                                                          synchDataArray = [];
                                                                                                                        //console.log(response.data.message);









                                                                                                          }); //end $http












                                                                   }
                                                                   else {
                                                                     alert("no network - unable to do $http POST synch.");
                                                                   }











                              }
                             else
                             {
                               alert("No Rows to Sync.");

                             }









                     }, function (err) {
                       alert("Synch Check Query on AppDB failed!" + JSON.stringify(err));
                     });

ch
                   } //end this.synch

                   this.resetFlags = function()
                   {

                       function updateFlags() {
                                 //var uQuery = "UPDATE bp_reading_processed SET chgSynched = 1 WHERE id = " + parseInt(window.localStorage['bprid']);
                                 var uQuery = "UPDATE bp_reading_processed SET chgSynched = ? WHERE userID = ?";
                                 //uQuery = "UPDATE bp_reading_processed SET userID = ? WHERE userID = ?";
                                 //alert("uQuery == " + uQuery);
                              $cordovaSQLite.execute(db, uQuery, [0, 1601]).then(function(res) {

                              alert("Changing Synch Flag:: Row Updated Successfully! res JSON: " + JSON.stringify(res));

                              }, function (err) {
                                alert("update/synch error" + JSON.stringify(err));
                              });
                          }

                         updateFlags();







                   }


                   this.testSynch = function()
                   {

                        //Need to do ONLY ONE $http POST and send all the data together >> in an array... then insert it into db that way...





                                        var token = window.localStorage["TOKEN"];
                                        var iCreatedAt = "2016-03-23 10:19:57.894";
                                        var iRawDataDate = "2016-03-23 10:19:57.894";
                                        var iUserID = 2;
                                        var iSys = 144;
                                        var iDia = 74;
                                        var iHr = 88;
                                        var iMap = 80;
                                        var iFeedback = "testFeedback";
                                        var iOutcome = "testOutcome";
                                        var iBpmID = 1;
                                        var iPhoneID = 1;







                                          $http({  method: 'POST', url: chgServerApi.serverName + "/api/createBPReading", data: { token: token, bprCreatedAtRaw: iRawDataDate, bprCreatedAt: iCreatedAt, chgUserID: iUserID, systolic: iSys, diastolic: iDia, hr: iHr, map: iMap, bprFeedback: iFeedback, bprOutcome: iOutcome, bpmID: iBpmID, phoneID: iPhoneID   } }).then(function successCallback(response)
                                          {







                                                        console.log("Inside the $http post... response json: " + JSON.stringify(response));
                                                        //alert("Response.synched: " + response.data.synched);




                                             }, function errorCallback(response) {
                                                          // called asynchronously if an error occurs
                                                          // or server returns response with an error status.


                                                             //alert("Error: " + response);
                                                             console.log("Error Sending Data to CHG: " + JSON.stringify(response));
                                                              //console.log(response.data.message);



                                                }); //end $http




                   }







}) //end synchAppCHGService










.factory('BloodPressure', function($state) {
  return {

        getSystolic:function()
        {
          //Interface code for bluetooth here!

          //returns a dummy var for now
          return 122;

        },
          getDiastolic:function()
          {
            //Interface code for bluetooth here!

            //returns a dummy var for now
            return 75;

          },

            getHeartRate:function()
            {
              //Interface code for bluetooth here!

              //returns a dummy var for now
              return 75;

            }



  };
})

















.factory('Post', function($resource) {
  return $resource('/api/post/:id');
});
